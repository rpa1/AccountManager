#### 介绍
个人记账系统 （V2.0 版本，原记账软件的底层深度重构；）

1. 用于个人记账理财，账目分析统计。
2. 简单的操作流程、 **尽量** 友好的操作界面、严谨的账目操作、账实一致的操作理念。
3. 独立运行环境，本地数据库 sqlite 或 sqlserver，可支持 mysql 和 oracle。
4. releases 包地址： [下载地址](https://gitee.com/zhcun/AccountManager/releases/2.0)
5. 默认用户名为：admin 密码：123

#### 程序环境
- .Net 4.0 
- SQLite3 | SQL Server 2008R2+
- VS2019 + WinForm
- 目前版本为CS架构，依赖：ZhCun.DbCore.Net40、ZhCun.Utils.Net40、ZhCun.Win
- 目前 Release 包 默认为SQLite 数据库可直接运行，可通过配置修改为SQLServer，本项目只包含记账软件的相关业务功能，界面控件及样式由 ZhCun.Win 实现

#### 文件说明

![程序文件说明](https://images.gitee.com/uploads/images/2020/0530/002646_deefac94_382749.png "屏幕截图.png")

#### 配置说明

```
<?xml version="1.0" encoding="utf-8"?>
<configuration>
  <appSettings>
    <add key="Caption" value="个人记账软件" />
    <add key="RunOnleOne" value="true" />
    <add key="FormIcon" value="app3.ico" />
    <add key="UserRoleId" value="01" />    
    <!--支持的数据库类型：sqlite、sqlserver、mysql、oracle-->
    <add key="DbType" value="sqlite" />
    <add key="Sys.DbType" value="sqlite" />
    <!--启用该选项可对连接字符串进行加密处理，加密处理详见：ZhCun.DynamicPassword 项目-->
    <!--<add key="DynamicEncryptyType" value="ZhCun.DynamicPassword.StringEncrypty,ZhCun.DynamicPassword" />-->
  </appSettings>
  <connectionStrings>
    <add name="Core" connectionString="Data Source=AccountBook.db;" />
    <add name="Sys.ConnStr" connectionString="Data Source=AccountBook.db;" />
    <!--<add name="Core" connectionString="Xn1K6nJtn2nML7WGIhdU0IPNfDv/xPgBYDsheEurfH408v12DYSb0KSkh6MEQntYzHuklPhzvvs=" />    
    <add name="Sys.ConnStr" connectionString="wZ4tY+KlrZBqPcR3GyNSgAjljhJH+X++E8iGFQRD5hlT1cXMXPUwp7PBfibBcejrLrP/84rf8Kw=" />-->
  </connectionStrings>
</configuration>
```
#### 软件截图
- 记账主界面
![记账](https://images.gitee.com/uploads/images/2020/0530/001121_3e73e935_382749.jpeg "02记账.jpg")
- 明细查询
![明细查询](https://images.gitee.com/uploads/images/2020/0530/001147_24f20d22_382749.jpeg "05综合明细.jpg")
- 类型统计
![类型统计](https://images.gitee.com/uploads/images/2020/0530/001211_448c9180_382749.jpeg "06类型统计.jpg")

