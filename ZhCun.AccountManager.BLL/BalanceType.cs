﻿/**************************************************************************
创建时间:	2020/5/20 16:46:08    
作	  者:	张存
邮 	  箱:	zhangcunliang@126.com

Copyright (c) zhcun.cn

描	述：
记	录：
***************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZhCun.AccountManager.BLL
{
    public enum BalanceType : int
    {       
        支出 = 1001,
        冲账支出 = 1002,
        对账支出 = 1003,
        转账支出 = 1004,
        收入 = 2001,
        冲账收入 = 2002,
        对账收入 = 2003,
        转账收入 = 2004           
    }
}
