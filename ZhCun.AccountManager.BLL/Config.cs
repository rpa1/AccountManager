﻿/**************************************************************************
创建时间:	2020/5/15
作	  者:	张存
邮 	  箱:	zhangcunliang@126.com

Copyright (c) zhcun.cn

描	述：
记	录：
***************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZhCun.Utils;

namespace ZhCun.AccountManager.BLL
{
    /// <summary>
    /// 本地的配置项
    /// </summary>
    public static class Config
    {
        static Config()
        {
            Init();
        }

        public static void Init()
        {
            Settings = ConfigHelper.GetAppSettings<AppSettings>();
        }
        /// <summary>
        /// 本地 appSetings
        /// </summary>
        public static AppSettings Settings { private set; get; }
    }

    /// <summary>
    /// 本地appSetings 
    /// </summary>
    public class AppSettings
    {
        /// <summary>
        /// 是否只允许运行一个实例
        /// </summary>
        public bool RunOnleOne { set; get; }
        /// <summary>
        /// 图标文件目录
        /// </summary>
        public string IconFolder { set; get; } = "Icon";
        /// <summary>
        /// 数据库类型
        /// </summary>
        public string DbType { set; get; } = "sqlserver";
        /// <summary>
        /// 普通用户的角色Id，默认为 01
        /// </summary>
        public string UserRoleId { set; get; } = "01";
        /// <summary>
        /// 管理员用户Id，默认：00
        /// </summary>
        public string ManageUserId { set; get; } = "00";
        /// <summary>
        /// 新增用户的默认密码
        /// </summary>
        public string DefaultUserPwd { set; get; } = "123";
    }
}
