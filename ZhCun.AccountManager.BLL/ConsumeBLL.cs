﻿/**************************************************************************
创建时间:	2020/5/20 15:31:52    
作	  者:	张存
邮 	  箱:	zhangcunliang@126.com

Copyright (c) zhcun.cn

描	述：记账操作,本项目核心操作
记	录：
***************************************************************************/
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using ZhCun.AccountManager.BLL.VM;
using ZhCun.AccountManager.DBModel;
using ZhCun.DbCore;
using ZhCun.Utils;
using ZhCun.Utils.Helpers;

namespace ZhCun.AccountManager.BLL
{
    public class ConsumeBLL : BLLBase
    {
        static ConsumeBLL()
        {
            DateList = new List<DateRange>();
            DateList.Add(new DateRange { DateType = DateRangeType.本周 });
            DateList.Add(new DateRange { DateType = DateRangeType.本月 });
            DateList.Add(new DateRange { DateType = DateRangeType.本年 });
            DateList.Add(new DateRange { DateType = DateRangeType.一周内 });
            DateList.Add(new DateRange { DateType = DateRangeType.一月内 });
            DateList.Add(new DateRange { DateType = DateRangeType.半年内 });
        }

        static readonly List<DateRange> DateList;

        public List<DateRange> SearchDateList
        {
            get { return DateList; }
        }

        public DataTable GetData(ArgConsumeSearch arg)
        {
            var query = DB.CreateQuery<TConsume>();
            query.PageNo = arg.PageNo;
            query.PageSize = arg.PageSize;
            if (arg.DateType == 0)
            {
                query.WhereAnd(s => s.AddTime >= arg.DataRange.DateValue.Value);
                query.OrderBy(s => s.AddTime, EmQueryOrder.Desc);
            }
            else
            {
                query.WhereAnd(s => s.ConsumeDate >= arg.DataRange.DateValue.Value);
                query.OrderBy(s => s.ConsumeDate, EmQueryOrder.Desc);
            }
            if (!arg.SearchVal.IsEmpty())
            {
                string likeStr = $"%{arg.SearchVal}%";
                query.WhereAnd(s =>
                    s.WEx_Like(s.ConsumeName, likeStr) ||
                    s.WEx_Like(s.ConsumeTypeName, likeStr));
            }
            var rData = DB.Query<TConsume>(query, ADJoinSql).ToDataTable();
            arg.OutRowCount = query.Total;

            query.WhereAnd(s => s.WEx_In(s.BalanceTypeId, 1001, 1003, 2001, 2003));
            //收支差额
            arg.AmountSum = DB.QuerySum<TConsume>(s => s.Amount, query, ADJoinSql).ToObject<decimal>();
            //支出总额
            query.WhereAnd(s => s.BalanceTypeId < 2000);            
            arg.AmountSumOut = DB.QuerySum<TConsume>(s => s.Amount, query, ADJoinSql).ToObject<decimal>();
            arg.AmountSumIn = arg.AmountSum - arg.AmountSumOut;

            return rData;
        }

        public DataTable GetAccountData(string serVal)
        {
            var query = DB.CreateQuery<TAccount>();
            if (!serVal.IsEmpty())
            {
                string likeStr = $"%{serVal}%";
                query.WhereAnd(s =>
                    s.WEx_Like(s.AccountName, likeStr) ||
                    s.WEx_Like(s.AccountNameCode, likeStr));
            }

            query.OrderBy(s => s.LastTime, EmQueryOrder.Desc)
                 .OrderBy(s => s.UseCount, EmQueryOrder.Desc);

            var dt = DB.Query<TAccount>(query).ToDataTable();
            return dt;
        }

        public DataTable GetConsumeTypeData(string serVal, BalanceType bType)
        {
            var query = DB.CreateQuery<TConsumeType>();
            query.WhereAnd(s => s.IsType == true);
            if (bType == BalanceType.支出)
            {
                query.WhereAnd(s => s.IsBalanceOut == true);
            }
            if (bType == BalanceType.收入)
            {
                query.WhereAnd(s => s.IsBalanceIn == true);
            }
            if (!serVal.IsEmpty())
            {
                string likeStr = $"%{serVal}%";
                query.WhereAnd(s =>
                    s.WEx_Like(s.TypeName, likeStr) ||
                    s.WEx_Like(s.TypeNamePY, likeStr));
            }
            query.OrderBy(s => s.LastTime, EmQueryOrder.Desc)
                 .OrderBy(s => s.UseCount, EmQueryOrder.Desc);

            var dt = DB.Query<TConsumeType>(query).ToDataTable();
            return dt;
        }
        /// <summary>
        /// 保存流水
        /// </summary>
        public ApiResult<string> SaveConsume(TConsume model)
        {
            if (model.AccountId.IsEmpty() || model.AccountName.IsEmpty())
            {
                return RetErr(nameof(model.AccountId), "账户不能为空");
            }

            if (model.ConsumeTypeName.IsEmpty() || model.ConsumeTypeId.IsEmpty())
            {
                return RetErr(nameof(model.AccountId), "账目类型不能为空");
            }

            if (model.BalanceTypeId == (int)BalanceType.支出)
            {
                model.Amount = Math.Abs(model.Amount) * -1;
            }
            else
            {
                model.Amount = Math.Abs(model.Amount);
            }

            try
            {
                DB.TransStart();
                var account = DB.Query<TAccount>(s => s.Id == model.AccountId).ToEntity();

                account.Balance += model.Amount;
                account.UseCount++;

                DB.Update(account);
                model.Balance = account.Balance;

                DB.UpdateByExpr<TConsumeType>(
                    s => s.Id == model.ConsumeTypeId,
                    $"{TConsumeType.CNUseCount} = {TConsumeType.CNUseCount} + 1");

                DB.Insert(model);
                DB.TransCommit();
                return RetOK(string.Empty, "记账成功");
            }
            catch (Exception ex)
            {
                DB.TransRollback();
                LogHelper.LogObj.Error("保存账目记录发生异常", ex);
                return RetErr(string.Empty, $"保存账目记录发生异常, {ex.Message}");
            }
        }
        /// <summary>
        /// 冲账
        /// </summary>
        public ApiResult CancelConsume(TConsume model)
        {
            if (model.IsCancel)
            {
                return RetErr("该记录已冲账");
            }
            if (model.CancelRemark.IsEmpty())
            {
                return RetErr("冲账原因不能为空");
            }
            if (model.BalanceTypeId != (int)BalanceType.支出 && model.BalanceTypeId != (int)BalanceType.收入)
            {
                return RetErr("只能对支出或收入进行冲账");
            }

            try
            {
                DB.TransStart();

                var newId = NewId();

                model.ClearChangedState();
                model.IsCancel = true;
                model.RelationlId = newId;
                DB.Update(model);

                if (model.BalanceTypeId == (int)BalanceType.支出)
                {
                    model.BalanceTypeId = (int)BalanceType.冲账收入;
                    model.BalanceTypeName = BalanceType.冲账收入.ToString();
                    model.Amount = Math.Abs(model.Amount);
                }

                if (model.BalanceTypeId == (int)BalanceType.收入)
                {
                    model.BalanceTypeId = (int)BalanceType.冲账支出;
                    model.BalanceTypeName = BalanceType.冲账支出.ToString();
                    model.Amount = Math.Abs(model.Amount) * -1;
                }

                var accountModel = DB.Query<TAccount>(s => s.Id == model.AccountId).ToEntity();
                accountModel.Balance += model.Amount;
                DB.Update(accountModel);

                model.Balance = accountModel.Balance;
                model.RelationlId = model.Id;
                model.Id = newId;
                model.ConsumeName += "【冲账】";
                model.SetFieldChanged(
                    TConsume.CNConsumeDate,
                    TConsume.CNAccountId,
                    TConsume.CNAccountName,
                    TConsume.CNIsCancel,
                    TConsume.CNCancelRemark,
                    TConsume.CNConsumeTypeId,
                    TConsume.CNConsumeTypeName);

                DB.Insert(model);

                DB.TransCommit();
            }
            catch (Exception ex)
            {
                DB.TransRollback();
                LogHelper.LogObj.Error("冲账发生异常", ex);
                return RetErr("冲账时发生异常，" + ex.Message);
            }

            return RetOK("冲账成功");
        }
        /// <summary>
        /// 移除最后一笔记录
        /// </summary>
        public ApiResult DelLastConsume()
        {
            var query = DB.CreateQuery<TConsume>();
            query.PageNo = 1;
            query.PageSize = 1;
            query.WhereAnd(s => s.AddTime >= DateTime.Now.AddHours(-24));
            query.OrderBy(s => s.AddTime, EmQueryOrder.Desc);
            var delConsume = DB.Query<TConsume>(query).ToEntity();

            if (delConsume == null)
            {
                return RetErr("没有可移除的数据,只能移除24小时内的数据");
            }

            if (BalanceType.转账收入.EqualValue(delConsume.BalanceTypeId) ||
                BalanceType.转账支出.EqualValue(delConsume.BalanceTypeId))
            {
                return RetErr("转账记录不允许移除");
            }

            DB.Delete(delConsume);

            var account = DB.Query<TAccount>(s => s.Id == delConsume.AccountId).ToEntity();
            account.Balance -= delConsume.Amount;
            DB.Update(account);
            //取消冲账标识
            if (delConsume.IsCancel)
            {
                delConsume.IsCancel = false;
                DB.Update(delConsume, s => s.Id == delConsume.RelationlId);
            }

            return RetOK("移除成功");
        }
        /// <summary>
        /// 编辑流水
        /// </summary>
        public ApiResult EditConsume(TConsume model)
        {
            model.ClearChangedState(
                TConsume.CNAccountId,
                TConsume.CNAccountName,
                TConsume.CNAmount,
                TConsume.CNBalanceTypeId,
                TConsume.CNBalanceTypeName);

            var ret = DB.Update(model);
            if (ret.RowCount == 1)
            {
                return RetOK("修改记录成功");
            }
            return RetErr("修改记录失败");
        }
        /// <summary>
        /// 转账操作
        /// </summary>
        public ApiResult SaveTransferAccount(string outAccountId, string inAccountId, decimal amount)
        {
            if (outAccountId.IsEmpty()) return RetErr("转出账户不能为空");
            if (inAccountId.IsEmpty()) return RetErr("转入账户不能为空");
            if (amount == 0) return RetErr("转账金额不能为0");
            if (outAccountId.StringEquals(inAccountId)) return RetErr("转出账户与转入账户不能为空");

            try
            {
                //非单机版,这里最好不使用事务,或尝试其它类型事务
                DB.TransStart();

                TAccount outAccount = DB.Query<TAccount>(s => s.Id == outAccountId).ToEntity();
                TAccount inAccount = DB.Query<TAccount>(s => s.Id == inAccountId).ToEntity();

                var newOutId = NewId();
                var newInId = NewId();

                TConsume c = new TConsume();
                c.Id = newOutId;
                c.ConsumeDate = DateTime.Now;
                c.ConsumeName = $"转出到【{inAccount.AccountName}】";
                c.BalanceTypeId = (int)BalanceType.转账支出;
                c.BalanceTypeName = BalanceType.转账支出.ToString();
                c.AccountId = outAccount.Id;
                c.AccountName = outAccount.AccountName;
                c.Amount = Math.Abs(amount) * -1;
                c.Balance = outAccount.Balance + c.Amount;
                c.RelationlId = newInId;
                outAccount.Balance = c.Balance;
                DB.Insert(c);
                DB.Update(outAccount);

                c.ClearChangedState();
                c.Id = newInId;
                c.ConsumeDate = DateTime.Now;
                c.ConsumeName = $"由【{outAccount.AccountName}】转入";
                c.BalanceTypeId = (int)BalanceType.转账收入;
                c.BalanceTypeName = BalanceType.转账收入.ToString();
                c.AccountId = inAccount.Id;
                c.AccountName = inAccount.AccountName;
                c.Amount = Math.Abs(amount);
                c.Balance = inAccount.Balance + c.Amount;
                c.RelationlId = newOutId;
                inAccount.Balance = c.Balance;

                DB.Insert(c);
                DB.Update(inAccount);

                DB.TransCommit();

                return RetOK("转账成功");
            }
            catch (Exception ex)
            {
                DB.TransRollback();
                LogHelper.LogObj.Error("转账时发生异常", ex);
                return RetOK($"转账时发生异常, {ex.Message}");
            }

        }
        /// <summary>
        /// 对账操作
        /// </summary>
        public ApiResult SaveCheckAccount(string accountId, decimal realBalance, string remark)
        {
            var account = DB.Query<TAccount>(s => s.Id == accountId).ToEntity();

            if (account.Balance == realBalance)
            {
                return RetErr("余额相等不用对账");
            }

            BalanceType bType = account.Balance > realBalance ? BalanceType.对账支出 : BalanceType.对账收入;

            TConsume c = new TConsume
            {
                Id = NewId(),
                ConsumeDate = DateTime.Now,
                ConsumeName = $"对账",
                BalanceTypeId = (int)bType,
                BalanceTypeName = bType.ToString(),
                AccountId = account.Id,
                AccountName = account.AccountName,
                Amount = realBalance - account.Balance,
                Balance = realBalance,
                Remark = remark
            };
            account.Balance = realBalance;
            DB.Insert(c);
            DB.Update(account);

            return RetOK("对账成功");
        }
    }
}