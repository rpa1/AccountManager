﻿/**************************************************************************
创建时间:	2020/5/20 13:28:27    
作	  者:	张存
邮 	  箱:	zhangcunliang@126.com

Copyright (c) zhcun.cn

描	述：
记	录：
***************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZhCun.AccountManager.DBModel;
using ZhCun.Utils;
using ZhCun.Utils.Helpers;

namespace ZhCun.AccountManager.BLL
{
    public class ConsumeTypeTreeBLL : ConsumeTypeBLL
    {
        public List<TConsumeType> GetTreeData()
        {
            var query = DB.CreateQuery<TConsumeType>();
            query.OrderBy(s => s.PId).OrderBy(s => s.OrderNo);
            var rList = DB.Query<TConsumeType>(query).ToList(true);
            return rList;
        }

        ApiResult VerifyNodeModel(TConsumeType model, bool isAdd)
        {
            if (model.TypeName.IsEmpty())
            {
                return RetErr("名称不能为空");
            }

            if (DB.QueryExist<TConsumeType>(s => s.TypeName == model.TypeName && s.PId == model.PId && s.Id != model.Id))
            {
                return RetErr("当前节点名称重复");
            }
            //if (DB.QueryExist<TConsumeType>(s => s.Id == model.PId && s.IsType == true))
            //{
            //    return RetErr("不能往该类型下添加子节点");
            //}

            if (isAdd) model.IsType = false;
            return RetOK(isAdd ? "新增成功" : "更新成功");
        }

        public ApiResult AddNode(TConsumeType model)
        {
            var ret = VerifyNodeModel(model, true);
            DB.Insert(model);
            return ret;
        }

        public ApiResult EditNode(TConsumeType model)
        {
            var ret = VerifyNodeModel(model, false);
            var r = DB.Update(model);
            if (r.RowCount != 1)
            {
                return RetErr("更新失败");
            }
            return ret;
        }

        public ApiResult DelNode(TConsumeType model)
        {
            if (DB.QueryExist<TConsumeType>(s => s.PId == model.Id))
            {
                return RetErr("请先移除当前节点下的所有项");
            }

            var ret = DB.Delete<TConsumeType>(model);
            if (ret.RowCount == 1)
            {
                return RetOK("删除成功");
            }
            return RetErr("删除失败");
        }

        public ApiResult ChangeOrder(string srcId, int srcOrder, string tarId, int tarOrder)
        {
            TConsumeType model = new TConsumeType();
            model.OrderNo = tarOrder;
            model.Id = srcId;
            DB.Update(model);

            model.OrderNo = srcOrder;
            model.Id = tarId;
            DB.Update(model);

            return RetOK("更新排序成功");
        }
    }
}