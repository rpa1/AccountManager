﻿/**************************************************************************
创建时间:	2020/5/15
作	  者:	张存
邮 	  箱:	zhangcunliang@126.com

Copyright (c) zhcun.cn

描	述：
记	录：
***************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZhCun.DbCore;
using ZhCun.DbCore.BuildSQLText;
using ZhCun.DbCore.Cores;
using ZhCun.DbCore.Entitys;
using ZhCun.Utils;
using ZhCun.Utils.Helpers;

namespace ZhCun.AccountManager.BLL
{
    class DBHelper : DBContextDebug
    {
        static DBHelper()
        {
            EnumHelper.StringToEnum(Config.Settings.DbType, out DbType);
            string ciphertext = ConfigHelper.GetConnectString("Core");
            ConnStr = DynamicEncrypty.GetPlaintext("Core", ciphertext);
            //ConnStr = ConfigHelper.GetConnectString("Core");
            LoginHost = HostHelper.GetIp();
        }

        public readonly static EmDbType DbType;

        public readonly static string ConnStr;

        public DBHelper(string currUserId)
            : base(DbType, ConnStr)
        {
            CurrUserId = currUserId;
        }

        /// <summary>
        /// 本机IP地址
        /// </summary>
        readonly static string LoginHost;

        const string FN_ADD_USER_ID = "AddUserId";
        const string FN_ADD_HOST = "AddHost";
        const string FN_LAST_USER_ID = "LastUserId";
        const string FN_LAST_HOST = "LastHost";
        const string FN_LAST_TIME = "LastTime";
        const string P_PARAM_HOST = "Arg_LoginHost";
        const string P_PARAM_USER_ID = "Arg_LoginUserId";

        private readonly string CurrUserId;

        protected override void ResultFinish(BaseResult result)
        {
            //TODO: 这里可以记录sql执行日志
            Console.WriteLine(result.SqlContent);
        }

        public ExecResult Insert<TEntity>(TEntity entity, bool autoId, bool autoUserId)
            where TEntity : EntityBase, new()
        {
            if (autoId)
            {
                var id = ReflectionHelper.GetPropertyValue<string>(entity, "Id");
                if (id.IsEmpty())
                {
                    id = GuidHelper.NewId();
                    ReflectionHelper.SetPropertyValue(entity, "Id", id);
                }
            }
            if (autoUserId)
            {
                ReflectionHelper.SetPropertyValue(entity, FN_ADD_HOST, LoginHost);
            }
            ReflectionHelper.SetPropertyValue(entity, FN_ADD_USER_ID, CurrUserId);

            return base.Insert(entity);
        }

        public override ExecResult Insert<TEntity>(TEntity entity)
        {
            return Insert(entity, true, true);

            ////Id主键自动赋值
            //var id = ReflectionHelper.GetPropertyValue<string>(entity, "Id");
            //if (id.IsEmpty())
            //{
            //    id = GuidHelper.NewId();
            //    ReflectionHelper.SetPropertyValue(entity, "Id", id);
            //}
            //ReflectionHelper.SetPropertyValue(entity, FN_ADD_HOST, LoginHost);
            //ReflectionHelper.SetPropertyValue(entity, FN_ADD_USER_ID, CurrUserId);
            //return base.Insert(entity);
        }

        protected override ExecResult UpdateBase<TEntity>(TEntity entity, Func<ISqlBuilder, string> whereFun)
        {
            ReflectionHelper.SetPropertyValue(entity, FN_LAST_HOST, LoginHost);
            ReflectionHelper.SetPropertyValue(entity, FN_LAST_USER_ID, CurrUserId);

            return base.UpdateBase(entity, whereFun);
        }

        public override ProcResult ExecProcedure<ProcEntity>(ProcEntity procObj)
        {
            ReflectionHelper.SetPropertyValue(procObj, P_PARAM_HOST, LoginHost);
            ReflectionHelper.SetPropertyValue(procObj, P_PARAM_USER_ID, CurrUserId);

            return base.ExecProcedure<ProcEntity>(procObj);
        }

        protected override void BeforeExecSqlBuilder<TEntity>(ISqlBuilder sqlBuilder, EmDbOperation opType)
        {
            if (CurrUserId == null) return;

            if (ReflectionHelper.GetPropertyInfo<TEntity>(FN_ADD_USER_ID) == null)
            {
                return;
            }

            var methodAttr = ReflectionHelper.GetMethodAttribute<IgnoreDataRightAttribute>();
            if (methodAttr != null) return; //指定方法忽略了数据权限
            sqlBuilder.AddSqlTextByGroup("{0} = {1}", FN_ADD_USER_ID, sqlBuilder.AddParam(CurrUserId));
        }
    }
}