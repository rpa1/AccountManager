/****** Object:  Table [dbo].[TDictionary]    Script Date: 05/28/2020 11:03:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TDictionary](
	[DictKey] [int] NOT NULL,
	[DictKeyPrompt] [varchar](100) NULL,
	[DictValue] [int] NOT NULL,
	[DictValuePrompt] [varchar](100) NULL,
	[Remark] [nvarchar](50) NULL,
 CONSTRAINT [PK_TDictionary] PRIMARY KEY CLUSTERED 
(
	[DictKey] ASC,
	[DictValue] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TConsumeTypeTree]    Script Date: 05/28/2020 11:03:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TConsumeTypeTree](
	[Id] [varchar](25) NOT NULL,
	[PId] [varchar](25) NOT NULL,
	[TreeName] [nvarchar](10) NOT NULL,
	[ConsumeTypeId] [varchar](25) NULL,
	[ConsumeTypeName] [nvarchar](10) NULL,
	[AddTime] [datetime] NOT NULL,
	[AddUserId] [varchar](25) NULL,
	[AddHost] [varchar](50) NULL,
	[LastTime] [datetime] NULL,
	[LastHost] [varchar](50) NULL,
	[LastUserId] [varchar](25) NULL,
 CONSTRAINT [PK_TConsumeTypeTree] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TConsumeType]    Script Date: 05/28/2020 11:03:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TConsumeType](
	[Id] [varchar](25) NOT NULL,
	[PId] [varchar](25) NULL,
	[TypeName] [varchar](20) NOT NULL,
	[TypeNamePY] [varchar](20) NULL,
	[TypeNameWB] [varchar](20) NULL,
	[UseCount] [int] NOT NULL,
	[Remark] [nvarchar](200) NULL,
	[IsType] [bit] NOT NULL,
	[OrderNo] [int] NOT NULL,
	[IsBalanceOut] [bit] NOT NULL,
	[IsBalanceIn] [bit] NOT NULL,
	[AddTime] [datetime] NOT NULL,
	[AddUserId] [varchar](25) NULL,
	[AddHost] [varchar](50) NULL,
	[LastTime] [datetime] NULL,
	[LastHost] [varchar](50) NULL,
	[LastUserId] [varchar](25) NULL,
 CONSTRAINT [PK_TConsumeType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否为类型，二不是分类节点' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TConsumeType', @level2type=N'COLUMN',@level2name=N'IsType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否资金支出' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TConsumeType', @level2type=N'COLUMN',@level2name=N'IsBalanceOut'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否资金收入' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TConsumeType', @level2type=N'COLUMN',@level2name=N'IsBalanceIn'
GO
/****** Object:  Table [dbo].[TConsume]    Script Date: 05/28/2020 11:03:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TConsume](
	[Id] [varchar](25) NOT NULL,
	[ConsumeDate] [date] NOT NULL,
	[ConsumeName] [nvarchar](50) NULL,
	[ConsumeTypeId] [varchar](25) NULL,
	[ConsumeTypeName] [nvarchar](20) NULL,
	[BalanceTypeId] [int] NOT NULL,
	[BalanceTypeName] [varchar](50) NOT NULL,
	[AccountId] [varchar](25) NOT NULL,
	[AccountName] [nvarchar](50) NOT NULL,
	[Amount] [decimal](18, 2) NOT NULL,
	[Balance] [decimal](18, 2) NOT NULL,
	[IsIncome] [bit] NOT NULL,
	[IsCancel] [bit] NOT NULL,
	[CancelRemark] [varchar](20) NULL,
	[RelationlId] [varchar](25) NULL,
	[Remark] [varchar](50) NULL,
	[AddTime] [datetime] NOT NULL,
	[AddUserId] [varchar](25) NULL,
	[AddHost] [varchar](50) NULL,
	[LastTime] [datetime] NULL,
	[LastHost] [varchar](50) NULL,
	[LastUserId] [varchar](25) NULL,
 CONSTRAINT [PK_TConsume] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发生日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TConsume', @level2type=N'COLUMN',@level2name=N'ConsumeDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'资金类型Id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TConsume', @level2type=N'COLUMN',@level2name=N'BalanceTypeId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'资金类型' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TConsume', @level2type=N'COLUMN',@level2name=N'BalanceTypeName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'账户ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TConsume', @level2type=N'COLUMN',@level2name=N'AccountId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'账户名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TConsume', @level2type=N'COLUMN',@level2name=N'AccountName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发生额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TConsume', @level2type=N'COLUMN',@level2name=N'Amount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'账户余额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TConsume', @level2type=N'COLUMN',@level2name=N'Balance'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否收入' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TConsume', @level2type=N'COLUMN',@level2name=N'IsIncome'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否冲账' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TConsume', @level2type=N'COLUMN',@level2name=N'IsCancel'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'冲账说明' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TConsume', @level2type=N'COLUMN',@level2name=N'CancelRemark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'关系ID，冲账关联的Id，转账关联的Id 等与其它记录相关联的id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TConsume', @level2type=N'COLUMN',@level2name=N'RelationlId'
GO
/****** Object:  Table [dbo].[TAccount]    Script Date: 05/28/2020 11:03:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TAccount](
	[Id] [varchar](50) NOT NULL,
	[AccountName] [nvarchar](20) NOT NULL,
	[AccountNameCode] [varchar](20) NULL,
	[Balance] [decimal](18, 2) NOT NULL,
	[UseCount] [int] NOT NULL,
	[Remark] [nvarchar](200) NULL,
	[IsDel] [bit] NOT NULL,
	[AddTime] [datetime] NOT NULL,
	[AddUserId] [varchar](25) NULL,
	[AddHost] [varchar](50) NULL,
	[LastTime] [datetime] NULL,
	[LastHost] [varchar](50) NULL,
	[LastUserId] [varchar](25) NULL,
 CONSTRAINT [PK_TAccount] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TUser]    Script Date: 05/28/2020 11:03:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TUser](
	[Id] [varchar](25) NOT NULL,
	[UserName] [nvarchar](10) NOT NULL,
	[LoginName] [varchar](10) NOT NULL,
	[LoginPwd] [varchar](20) NOT NULL,
	[RoleId] [varchar](20) NOT NULL,
	[LoginCount] [int] NOT NULL,
	[LastLoginTime] [datetime] NULL,
	[IsUse] [bit] NOT NULL,
	[IsManager] [bit] NOT NULL,
	[AddTime] [datetime] NOT NULL,
	[LastTime] [datetime] NULL,
 CONSTRAINT [PK_TUser] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否管理员' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TUser', @level2type=N'COLUMN',@level2name=N'IsManager'
GO
/****** Object:  Trigger [Tgr_Sys_TUser_LastTime]    Script Date: 05/28/2020 11:03:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create TRIGGER [dbo].[Tgr_Sys_TUser_LastTime] ON [dbo].[TUser]
 for UPDATE AS 
BEGIN
	SET NOCOUNT ON;
	update TUser set LastTime = GETDATE() where Id in (select Id from inserted);
END
GO
/****** Object:  Trigger [Tgr_Sys_TConsumeTypeTree_LastTime]    Script Date: 05/28/2020 11:03:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create TRIGGER [dbo].[Tgr_Sys_TConsumeTypeTree_LastTime] ON [dbo].[TConsumeTypeTree]
 for UPDATE AS 
BEGIN
	SET NOCOUNT ON;
	update TConsumeTypeTree set LastTime = GETDATE() where Id in (select Id from inserted);
END
GO
/****** Object:  Trigger [Tgr_Sys_TConsumeType_LastTime]    Script Date: 05/28/2020 11:03:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create TRIGGER [dbo].[Tgr_Sys_TConsumeType_LastTime] ON [dbo].[TConsumeType]
 for UPDATE AS 
BEGIN
	SET NOCOUNT ON;
	update TConsumeType set LastTime = GETDATE() where Id in (select Id from inserted);
END
GO
/****** Object:  Trigger [Tgr_Sys_TConsume_LastTime]    Script Date: 05/28/2020 11:03:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create TRIGGER [dbo].[Tgr_Sys_TConsume_LastTime] ON [dbo].[TConsume]
 for UPDATE AS 
BEGIN
	SET NOCOUNT ON;
	update TConsume set LastTime = GETDATE() where Id in (select Id from inserted);
END
GO
/****** Object:  Trigger [Tgr_Sys_TAccount_LastTime]    Script Date: 05/28/2020 11:03:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create TRIGGER [dbo].[Tgr_Sys_TAccount_LastTime] ON [dbo].[TAccount]
 for UPDATE AS 
BEGIN
	SET NOCOUNT ON;
	update [TAccount] set LastTime = GETDATE() where Id in (select Id from inserted);
END
GO
/****** Object:  Default [DF_TAccount_Balance]    Script Date: 05/28/2020 11:03:48 ******/
ALTER TABLE [dbo].[TAccount] ADD  CONSTRAINT [DF_TAccount_Balance]  DEFAULT ((0)) FOR [Balance]
GO
/****** Object:  Default [DF_TAccount_UseCount]    Script Date: 05/28/2020 11:03:48 ******/
ALTER TABLE [dbo].[TAccount] ADD  CONSTRAINT [DF_TAccount_UseCount]  DEFAULT ((0)) FOR [UseCount]
GO
/****** Object:  Default [DF_TAccount_IsDel]    Script Date: 05/28/2020 11:03:48 ******/
ALTER TABLE [dbo].[TAccount] ADD  CONSTRAINT [DF_TAccount_IsDel]  DEFAULT ((0)) FOR [IsDel]
GO
/****** Object:  Default [DF_TAccount_AddTime]    Script Date: 05/28/2020 11:03:48 ******/
ALTER TABLE [dbo].[TAccount] ADD  CONSTRAINT [DF_TAccount_AddTime]  DEFAULT (getdate()) FOR [AddTime]
GO
/****** Object:  Default [DF_TAccount_AddUserId]    Script Date: 05/28/2020 11:03:48 ******/
ALTER TABLE [dbo].[TAccount] ADD  CONSTRAINT [DF_TAccount_AddUserId]  DEFAULT (NULL) FOR [AddUserId]
GO
/****** Object:  Default [DF_TAccount_AddHost]    Script Date: 05/28/2020 11:03:48 ******/
ALTER TABLE [dbo].[TAccount] ADD  CONSTRAINT [DF_TAccount_AddHost]  DEFAULT (NULL) FOR [AddHost]
GO
/****** Object:  Default [DF_TConsume_IsIncome]    Script Date: 05/28/2020 11:03:48 ******/
ALTER TABLE [dbo].[TConsume] ADD  CONSTRAINT [DF_TConsume_IsIncome]  DEFAULT ((0)) FOR [IsIncome]
GO
/****** Object:  Default [DF_TConsume_IsCancel]    Script Date: 05/28/2020 11:03:48 ******/
ALTER TABLE [dbo].[TConsume] ADD  CONSTRAINT [DF_TConsume_IsCancel]  DEFAULT ((0)) FOR [IsCancel]
GO
/****** Object:  Default [DF_TConsume_AddTime]    Script Date: 05/28/2020 11:03:48 ******/
ALTER TABLE [dbo].[TConsume] ADD  CONSTRAINT [DF_TConsume_AddTime]  DEFAULT (getdate()) FOR [AddTime]
GO
/****** Object:  Default [DF_TConsume_AddUserId]    Script Date: 05/28/2020 11:03:48 ******/
ALTER TABLE [dbo].[TConsume] ADD  CONSTRAINT [DF_TConsume_AddUserId]  DEFAULT (NULL) FOR [AddUserId]
GO
/****** Object:  Default [DF_TConsume_AddHost]    Script Date: 05/28/2020 11:03:48 ******/
ALTER TABLE [dbo].[TConsume] ADD  CONSTRAINT [DF_TConsume_AddHost]  DEFAULT (NULL) FOR [AddHost]
GO
/****** Object:  Default [DF_TConsumeType_UseCount]    Script Date: 05/28/2020 11:03:48 ******/
ALTER TABLE [dbo].[TConsumeType] ADD  CONSTRAINT [DF_TConsumeType_UseCount]  DEFAULT ((0)) FOR [UseCount]
GO
/****** Object:  Default [DF_TConsumeType_IsType]    Script Date: 05/28/2020 11:03:48 ******/
ALTER TABLE [dbo].[TConsumeType] ADD  CONSTRAINT [DF_TConsumeType_IsType]  DEFAULT ((0)) FOR [IsType]
GO
/****** Object:  Default [DF_TConsumeType_OrderNo]    Script Date: 05/28/2020 11:03:48 ******/
ALTER TABLE [dbo].[TConsumeType] ADD  CONSTRAINT [DF_TConsumeType_OrderNo]  DEFAULT ((0)) FOR [OrderNo]
GO
/****** Object:  Default [DF_TConsumeType_IsOutType]    Script Date: 05/28/2020 11:03:48 ******/
ALTER TABLE [dbo].[TConsumeType] ADD  CONSTRAINT [DF_TConsumeType_IsOutType]  DEFAULT ((0)) FOR [IsBalanceOut]
GO
/****** Object:  Default [DF_TConsumeType_IsBalanceIn]    Script Date: 05/28/2020 11:03:48 ******/
ALTER TABLE [dbo].[TConsumeType] ADD  CONSTRAINT [DF_TConsumeType_IsBalanceIn]  DEFAULT ((0)) FOR [IsBalanceIn]
GO
/****** Object:  Default [DF_TConsumeType_AddTime]    Script Date: 05/28/2020 11:03:48 ******/
ALTER TABLE [dbo].[TConsumeType] ADD  CONSTRAINT [DF_TConsumeType_AddTime]  DEFAULT (getdate()) FOR [AddTime]
GO
/****** Object:  Default [DF_TConsumeType_AddUserId]    Script Date: 05/28/2020 11:03:48 ******/
ALTER TABLE [dbo].[TConsumeType] ADD  CONSTRAINT [DF_TConsumeType_AddUserId]  DEFAULT (NULL) FOR [AddUserId]
GO
/****** Object:  Default [DF_TConsumeType_AddHost]    Script Date: 05/28/2020 11:03:48 ******/
ALTER TABLE [dbo].[TConsumeType] ADD  CONSTRAINT [DF_TConsumeType_AddHost]  DEFAULT (NULL) FOR [AddHost]
GO
/****** Object:  Default [DF_TConsumeTypeTree_AddTime]    Script Date: 05/28/2020 11:03:48 ******/
ALTER TABLE [dbo].[TConsumeTypeTree] ADD  CONSTRAINT [DF_TConsumeTypeTree_AddTime]  DEFAULT (getdate()) FOR [AddTime]
GO
/****** Object:  Default [DF_TConsumeTypeTree_AddUserId]    Script Date: 05/28/2020 11:03:48 ******/
ALTER TABLE [dbo].[TConsumeTypeTree] ADD  CONSTRAINT [DF_TConsumeTypeTree_AddUserId]  DEFAULT (NULL) FOR [AddUserId]
GO
/****** Object:  Default [DF_TConsumeTypeTree_AddHost]    Script Date: 05/28/2020 11:03:48 ******/
ALTER TABLE [dbo].[TConsumeTypeTree] ADD  CONSTRAINT [DF_TConsumeTypeTree_AddHost]  DEFAULT (NULL) FOR [AddHost]
GO
/****** Object:  Default [DF_TUser_LoginCount]    Script Date: 05/28/2020 11:03:48 ******/
ALTER TABLE [dbo].[TUser] ADD  CONSTRAINT [DF_TUser_LoginCount]  DEFAULT ((0)) FOR [LoginCount]
GO
/****** Object:  Default [DF_TUser_IsUse]    Script Date: 05/28/2020 11:03:48 ******/
ALTER TABLE [dbo].[TUser] ADD  CONSTRAINT [DF_TUser_IsUse]  DEFAULT ((1)) FOR [IsUse]
GO
/****** Object:  Default [DF_TUser_IsManager]    Script Date: 05/28/2020 11:03:48 ******/
ALTER TABLE [dbo].[TUser] ADD  CONSTRAINT [DF_TUser_IsManager]  DEFAULT ((0)) FOR [IsManager]
GO
/****** Object:  Default [DF_TUser_AddTime]    Script Date: 05/28/2020 11:03:48 ******/
ALTER TABLE [dbo].[TUser] ADD  CONSTRAINT [DF_TUser_AddTime]  DEFAULT (getdate()) FOR [AddTime]
GO
