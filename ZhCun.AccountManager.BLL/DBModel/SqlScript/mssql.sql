/****** Object:  View [dbo].[V_Random]    Script Date: 05/28/2020 10:02:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create VIEW [dbo].[V_Random]
AS
    SELECT  RAND() AS RndNumber;
GO
/****** Object:  UserDefinedFunction [dbo].[F_BinToHex]    Script Date: 05/28/2020 10:02:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
	功能: 将指定二进制转换为16进制字符串
	描述:	
		
	作者: 张存
	日期: 2019.8.5
	修改记录(注明修改人,修改时间):
*/
CREATE function [dbo].[F_BinToHex](
    @Bin varbinary(100)
)
returns varchar(200)
as
begin
    declare 
		@re varchar(200) = '',
		@i int = datalength(@bin);
    while @i>0
    begin
        select 
			@re = substring('0123456789ABCDEF', substring(@bin,@i,1)/16+1,1)
                + substring('0123456789ABCDEF',substring(@bin,@i,1)%16+1,1)
                +@re
            ,@i = @i-1
   end
   return @re
end
GO
/****** Object:  UserDefinedFunction [dbo].[F_GenRandom]    Script Date: 05/28/2020 10:02:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Function [dbo].[F_GenRandom](
	@Len int, --生成的记过长度
	@RndType int = 1 -- 随机类型，1：纯数字，2：数字加小写字母，3：数字加大小写字母
	)
	returns varchar(20)
	/*
	select dbo.F_GenRandom(6,1)
	
	*/
	
as
begin
	declare
		@flag  int,
		@Ret varchar(20) = ''  ---最后会生成的随机码        
        
	if (@RndType> 3 or @RndType <1)
	begin
		set @RndType = 3;
	end	
	while LEN(@Ret) < @Len
	begin
		select @flag = Ceiling(RndNumber * @RndType) from V_Random
		if @flag = 1
		begin
			--1至9的随机数字(整数)
			Select @Ret = @Ret + CAST(CEILING(RndNumber * 9) AS VARCHAR(1)) From V_Random
		end else 
		if @flag = 2
		begin
			--随机字母(小写)			
			Select @Ret = @Ret + CHAR(97 + CEILING(RndNumber * 25)) From V_Random;
			
		end else
		begin
			--随机字母（大写）
			Select @Ret = @Ret + CHAR(65 + CEILING(RndNumber * 25)) From V_Random;			
		end		
	end
return @Ret;
end
GO
/****** Object:  UserDefinedFunction [dbo].[F_NewId]    Script Date: 05/28/2020 10:02:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
	功能:	生成自定义字符串的ID
	描述:				
			
	作者: 张存
	日期: 2019.8.5
	修改记录(注明修改人,修改时间):		
		
*/
CREATE Function [dbo].[F_NewId]()
Returns varchar(22)
AS
BEGIN
	declare
		@Now DATETIME,
		@TimeStamp bigint,
		@Rmd int
		
	set @Now = GetUTCDate();
	set @TimeStamp = CONVERT(BIGINT,DATEDIFF(MI,'1970-01-01 00:00:00', @Now)) * 60000 + DATEPART(S,@Now) * 1000 + DATEPART(MS, @Now);	
	set @Rmd = dbo.F_GenRandom(9,1);
	
	return Right(dbo.F_BinToHex(@TimeStamp),12) + dbo.F_BinToHex(@Rmd) + '00';	
END
GO
/****** Object:  Table [dbo].[Sys_Role_Menu]    Script Date: 05/28/2020 10:02:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sys_Role_Menu](
	[Id] [varchar](25) NOT NULL,
	[PId] [varchar](25) NULL,
	[RoleId] [varchar](25) NOT NULL,
	[MenuId] [varchar](25) NULL,
	[MenuCaption] [varchar](50) NULL,
	[MenuName] [varchar](200) NULL,
	[IsMenuSplit] [bit] NOT NULL,
	[ControlRights] [varchar](500) NULL,
	[OrderNo] [int] NOT NULL,
	[IsMenu] [bit] NOT NULL,
	[IsModalForm] [bit] NOT NULL,
	[ToolBarVisible] [bit] NOT NULL,
	[ToolBarCaption] [nvarchar](10) NULL,
	[ToolBarIcon] [varchar](100) NULL,
	[ToolBarOrderNo] [int] NOT NULL,
	[ToolBarSplit] [bit] NOT NULL,
	[AddTime] [datetime] NOT NULL,
	[AddUserId] [varchar](25) NULL,
	[AddHost] [varchar](50) NULL,
	[LastTime] [datetime] NULL,
	[LastHost] [varchar](50) NULL,
	[LastUserId] [varchar](25) NULL,
 CONSTRAINT [PK_Sys_Role_Menu] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'菜单导航下一个是否为分隔符 -' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sys_Role_Menu', @level2type=N'COLUMN',@level2name=N'IsMenuSplit'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'窗体控件权限' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sys_Role_Menu', @level2type=N'COLUMN',@level2name=N'ControlRights'
GO
/****** Object:  Table [dbo].[Sys_Role]    Script Date: 05/28/2020 10:02:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sys_Role](
	[Id] [varchar](25) NOT NULL,
	[RoleName] [varchar](20) NOT NULL,
	[RoleNamePY] [varchar](20) NULL,
	[Remark] [varchar](200) NULL,
	[AddTime] [datetime] NOT NULL,
	[AddUserId] [varchar](25) NULL,
	[AddHost] [varchar](50) NULL,
	[LastTime] [datetime] NULL,
	[LastHost] [varchar](50) NULL,
	[LastUserId] [varchar](25) NULL,
 CONSTRAINT [PK_Sys_Role] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sys_Menu]    Script Date: 05/28/2020 10:02:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sys_Menu](
	[Id] [varchar](25) NOT NULL,
	[PId] [varchar](25) NULL,
	[MenuCaption] [varchar](20) NOT NULL,
	[MenuName] [varchar](200) NULL,
	[IsModalForm] [bit] NOT NULL,
	[MenuLevel] [int] NULL,
	[IsMenu] [bit] NOT NULL,
	[OrderNo] [int] NOT NULL,
	[Remark] [varchar](200) NULL,
	[AddTime] [datetime] NOT NULL,
	[AddUserId] [varchar](25) NULL,
	[AddHost] [varchar](50) NULL,
	[LastTime] [datetime] NULL,
	[LastHost] [varchar](50) NULL,
	[LastUserId] [varchar](25) NULL,
 CONSTRAINT [PK_Sys_Menu] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'缺省是否为模态窗口' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sys_Menu', @level2type=N'COLUMN',@level2name=N'IsModalForm'
GO
/****** Object:  Table [dbo].[Sys_GridCombox_Column]    Script Date: 05/28/2020 10:02:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sys_GridCombox_Column](
	[Id] [varchar](25) NOT NULL,
	[MainId] [varchar](25) NOT NULL,
	[ColumnIndex] [int] NOT NULL,
	[ColumnCaption] [varchar](50) NOT NULL,
	[FiledName] [varchar](50) NOT NULL,
	[IsVisible] [bit] NOT NULL,
	[ColumnWidth] [int] NOT NULL,
	[FormatStr] [varchar](50) NULL,
	[Alignment] [varchar](50) NULL,
	[AutoSizeMode] [varchar](50) NULL,
	[ColumnType] [varchar](20) NOT NULL,
	[AddTime] [datetime] NOT NULL,
	[AddUserId] [varchar](25) NULL,
	[AddHost] [varchar](50) NULL,
	[LastTime] [datetime] NULL,
	[LastHost] [varchar](50) NULL,
	[LastUserId] [varchar](25) NULL,
 CONSTRAINT [PK_Sys_GridCombox_Column] PRIMARY KEY NONCLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE CLUSTERED INDEX [IX_Sys_GridCombox_Column] ON [dbo].[Sys_GridCombox_Column] 
(
	[MainId] ASC,
	[ColumnIndex] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'列顺序，不能重复，排序' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sys_GridCombox_Column', @level2type=N'COLUMN',@level2name=N'ColumnIndex'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'列标题' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sys_GridCombox_Column', @level2type=N'COLUMN',@level2name=N'ColumnCaption'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'字段名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sys_GridCombox_Column', @level2type=N'COLUMN',@level2name=N'FiledName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'列宽,默认为0，表示自适应' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sys_GridCombox_Column', @level2type=N'COLUMN',@level2name=N'ColumnWidth'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'值格式化显示' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sys_GridCombox_Column', @level2type=N'COLUMN',@level2name=N'FormatStr'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'对齐方式' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sys_GridCombox_Column', @level2type=N'COLUMN',@level2name=N'Alignment'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'列大小模式' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sys_GridCombox_Column', @level2type=N'COLUMN',@level2name=N'AutoSizeMode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'列类型，0：文本，1：数字（左对齐），2：CheckBox' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sys_GridCombox_Column', @level2type=N'COLUMN',@level2name=N'ColumnType'
GO
/****** Object:  Table [dbo].[Sys_GridCombox]    Script Date: 05/28/2020 10:02:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sys_GridCombox](
	[Id] [varchar](25) NOT NULL,
	[FormName] [varchar](100) NOT NULL,
	[TextBoxName] [varchar](100) NOT NULL,
	[MaxRowCount] [tinyint] NOT NULL,
	[RetrunTagField] [varchar](50) NULL,
	[ReturnTextField] [varchar](50) NOT NULL,
	[AutoSizeColumnsMode] [varchar](50) NULL,
	[IsAnyKeyShow] [bit] NOT NULL,
	[IsAutoSelected] [bit] NOT NULL,
	[KeyPressMinlength] [tinyint] NOT NULL,
	[IsSendTabKey] [bit] NOT NULL,
	[Remark] [varchar](50) NULL,
	[IsUse] [bit] NOT NULL,
	[AddTime] [datetime] NOT NULL,
	[AddUserId] [varchar](25) NULL,
	[AddHost] [varchar](50) NULL,
	[LastTime] [datetime] NULL,
	[LastHost] [varchar](50) NULL,
	[LastUserId] [varchar](25) NULL,
 CONSTRAINT [PK_Sys_GridCombox] PRIMARY KEY NONCLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE CLUSTERED INDEX [IX_Sys_GridCombox] ON [dbo].[Sys_GridCombox] 
(
	[FormName] ASC,
	[TextBoxName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'窗体的FullName' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sys_GridCombox', @level2type=N'COLUMN',@level2name=N'FormName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'绑定的文本框控件名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sys_GridCombox', @level2type=N'COLUMN',@level2name=N'TextBoxName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Grid最多显示的行号，默认为10，最高不不超过255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sys_GridCombox', @level2type=N'COLUMN',@level2name=N'MaxRowCount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'返回文本框Tag属性的字段' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sys_GridCombox', @level2type=N'COLUMN',@level2name=N'RetrunTagField'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'返回文本框Text属性的字段' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sys_GridCombox', @level2type=N'COLUMN',@level2name=N'ReturnTextField'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否任意键搜索数据源' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sys_GridCombox', @level2type=N'COLUMN',@level2name=N'IsAnyKeyShow'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'当只有一行数据时是否自动选择退出（默认为true）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sys_GridCombox', @level2type=N'COLUMN',@level2name=N'IsAutoSelected'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'当非回车显示时，文本中内容最小值是多少才进行搜索显示（默认为0）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sys_GridCombox', @level2type=N'COLUMN',@level2name=N'KeyPressMinlength'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'当选中节点后是否自动响应Tab按键，焦点到下一个TabIdnex（默认为true）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sys_GridCombox', @level2type=N'COLUMN',@level2name=N'IsSendTabKey'
GO
/****** Object:  Table [dbo].[Sys_GridColumn]    Script Date: 05/28/2020 10:02:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sys_GridColumn](
	[Id] [varchar](25) NOT NULL,
	[FormName] [varchar](100) NOT NULL,
	[DgvName] [varchar](50) NOT NULL,
	[ColumnIndex] [int] NOT NULL,
	[ColumnName] [varchar](50) NOT NULL,
	[ColumnTitle] [varchar](20) NOT NULL,
	[ColumnType] [varchar](20) NOT NULL,
	[ColumnWidth] [int] NOT NULL,
	[Alignment] [varchar](50) NULL,
	[IsReadOnly] [bit] NOT NULL,
	[IsVisible] [bit] NOT NULL,
	[IsAdSearch] [bit] NOT NULL,
	[DataType] [varchar](50) NULL,
	[AutoSizeMode] [varchar](50) NULL,
	[FormatStr] [varchar](50) NULL,
	[Remark] [varchar](200) NULL,
	[AddTime] [datetime] NOT NULL,
	[AddUserId] [varchar](25) NULL,
	[AddHost] [varchar](50) NULL,
	[LastTime] [datetime] NULL,
	[LastHost] [varchar](50) NULL,
	[LastUserId] [varchar](25) NULL,
 CONSTRAINT [PK_Sys_GridColumn] PRIMARY KEY NONCLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE CLUSTERED INDEX [IX_Sys_GridColumn] ON [dbo].[Sys_GridColumn] 
(
	[FormName] ASC,
	[DgvName] ASC,
	[ColumnIndex] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'列顺序，不能重复，排序' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sys_GridColumn', @level2type=N'COLUMN',@level2name=N'ColumnIndex'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'列类型，0：文本，1：数字（左对齐），2：CheckBox' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sys_GridColumn', @level2type=N'COLUMN',@level2name=N'ColumnType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'列宽，默认为0为自适应' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sys_GridColumn', @level2type=N'COLUMN',@level2name=N'ColumnWidth'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'对齐方式' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sys_GridColumn', @level2type=N'COLUMN',@level2name=N'Alignment'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否启用高级搜索' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sys_GridColumn', @level2type=N'COLUMN',@level2name=N'IsAdSearch'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'高级搜索的数据类型' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sys_GridColumn', @level2type=N'COLUMN',@level2name=N'DataType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'列大小模式' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sys_GridColumn', @level2type=N'COLUMN',@level2name=N'AutoSizeMode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'格式化字符串' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sys_GridColumn', @level2type=N'COLUMN',@level2name=N'FormatStr'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sys_GridColumn', @level2type=N'CONSTRAINT',@level2name=N'PK_Sys_GridColumn'
GO
/****** Object:  Table [dbo].[Sys_ADRecord_Detail]    Script Date: 05/28/2020 10:02:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sys_ADRecord_Detail](
	[Id] [varchar](25) NOT NULL,
	[RecordId] [varchar](25) NULL,
	[FieldName] [varchar](50) NULL,
	[CompareSignStr] [varchar](50) NULL,
	[SearchValue] [varchar](50) NULL,
	[ConnectorSignStr] [varchar](50) NULL,
	[DataTypeStr] [varchar](50) NULL,
	[OrderNo] [int] NOT NULL,
 CONSTRAINT [PK_Sys_ADRecord_Detail] PRIMARY KEY NONCLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [IX_Sys_ADRecord_Detail_MainId] ON [dbo].[Sys_ADRecord_Detail] 
(
	[RecordId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'比较符
 EqualTo,Like,等' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sys_ADRecord_Detail', @level2type=N'COLUMN',@level2name=N'Id'
GO
/****** Object:  Table [dbo].[Sys_ADRecord]    Script Date: 05/28/2020 10:02:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sys_ADRecord](
	[Id] [varchar](25) NOT NULL,
	[FormName] [varchar](200) NOT NULL,
	[DgvName] [varchar](50) NOT NULL,
	[Caption] [nvarchar](50) NOT NULL,
	[CaptionCode] [varchar](50) NOT NULL,
	[IsPublic] [bit] NOT NULL,
	[UserId] [varchar](25) NOT NULL,
	[LastUseTime] [datetime] NOT NULL,
 CONSTRAINT [PK_Sys_ADRecord] PRIMARY KEY NONCLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE CLUSTERED INDEX [IX_Sys_ADRecord] ON [dbo].[Sys_ADRecord] 
(
	[FormName] ASC,
	[DgvName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Trigger [Tgr_Sys_Role_Menu_LastTime]    Script Date: 05/28/2020 10:02:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[Tgr_Sys_Role_Menu_LastTime] ON [dbo].[Sys_Role_Menu]
 for UPDATE AS 
BEGIN
	SET NOCOUNT ON;
	update Sys_Role_Menu set LastTime = GETDATE() where Id in (select Id from inserted);	
END
GO
/****** Object:  Trigger [Tgr_Sys_Role_LastTime]    Script Date: 05/28/2020 10:02:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[Tgr_Sys_Role_LastTime] ON [dbo].[Sys_Role]
 for UPDATE AS 
BEGIN
	SET NOCOUNT ON;
	update Sys_Role set LastTime = GETDATE() where Id in (select Id from inserted);	
END
GO
/****** Object:  Trigger [Tgr_Sys_Menu_LastTime]    Script Date: 05/28/2020 10:02:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[Tgr_Sys_Menu_LastTime] ON [dbo].[Sys_Menu]
 for UPDATE AS 
BEGIN
	SET NOCOUNT ON;
	update Sys_Menu set LastTime = GETDATE() where Id in (select Id from inserted);
END
GO
/****** Object:  Trigger [Tgr_Sys_GridColumn_LastTime]    Script Date: 05/28/2020 10:02:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[Tgr_Sys_GridColumn_LastTime] ON [dbo].[Sys_GridColumn]
 for UPDATE AS 
BEGIN
	SET NOCOUNT ON;
	update Sys_GridColumn set LastTime = GETDATE() where Id in (select Id from inserted);
END
GO
/****** Object:  Default [DF_AD_Record_Id]    Script Date: 05/28/2020 10:02:12 ******/
ALTER TABLE [dbo].[Sys_ADRecord] ADD  CONSTRAINT [DF_AD_Record_Id]  DEFAULT ([dbo].[F_NewId]()) FOR [Id]
GO
/****** Object:  Default [DF_AD_Record_GridViewName]    Script Date: 05/28/2020 10:02:12 ******/
ALTER TABLE [dbo].[Sys_ADRecord] ADD  CONSTRAINT [DF_AD_Record_GridViewName]  DEFAULT ('dgv') FOR [DgvName]
GO
/****** Object:  Default [DF_AD_Record_IsPublic]    Script Date: 05/28/2020 10:02:12 ******/
ALTER TABLE [dbo].[Sys_ADRecord] ADD  CONSTRAINT [DF_AD_Record_IsPublic]  DEFAULT ((0)) FOR [IsPublic]
GO
/****** Object:  Default [DF_AD_Record_LastUseTime]    Script Date: 05/28/2020 10:02:12 ******/
ALTER TABLE [dbo].[Sys_ADRecord] ADD  CONSTRAINT [DF_AD_Record_LastUseTime]  DEFAULT (getdate()) FOR [LastUseTime]
GO
/****** Object:  Default [DF_AD_RecordDetail_Id]    Script Date: 05/28/2020 10:02:12 ******/
ALTER TABLE [dbo].[Sys_ADRecord_Detail] ADD  CONSTRAINT [DF_AD_RecordDetail_Id]  DEFAULT ([dbo].[F_NewId]()) FOR [Id]
GO
/****** Object:  Default [DF_AD_RecordDetail_OrderNo]    Script Date: 05/28/2020 10:02:12 ******/
ALTER TABLE [dbo].[Sys_ADRecord_Detail] ADD  CONSTRAINT [DF_AD_RecordDetail_OrderNo]  DEFAULT ((1)) FOR [OrderNo]
GO
/****** Object:  Default [DF_Com_Grid_Cloumn_Id]    Script Date: 05/28/2020 10:02:12 ******/
ALTER TABLE [dbo].[Sys_GridColumn] ADD  CONSTRAINT [DF_Com_Grid_Cloumn_Id]  DEFAULT ([dbo].[F_NewId]()) FOR [Id]
GO
/****** Object:  Default [DF_Com_Grid_Cloumn_OrderNo]    Script Date: 05/28/2020 10:02:12 ******/
ALTER TABLE [dbo].[Sys_GridColumn] ADD  CONSTRAINT [DF_Com_Grid_Cloumn_OrderNo]  DEFAULT ((0)) FOR [ColumnIndex]
GO
/****** Object:  Default [DF_Com_Grid_Cloumn_ColumnType]    Script Date: 05/28/2020 10:02:12 ******/
ALTER TABLE [dbo].[Sys_GridColumn] ADD  CONSTRAINT [DF_Com_Grid_Cloumn_ColumnType]  DEFAULT ((0)) FOR [ColumnType]
GO
/****** Object:  Default [DF_Com_Grid_Cloumn_ColumnWidth]    Script Date: 05/28/2020 10:02:12 ******/
ALTER TABLE [dbo].[Sys_GridColumn] ADD  CONSTRAINT [DF_Com_Grid_Cloumn_ColumnWidth]  DEFAULT ((0)) FOR [ColumnWidth]
GO
/****** Object:  Default [DF_Com_Grid_Cloumn_Alignment]    Script Date: 05/28/2020 10:02:12 ******/
ALTER TABLE [dbo].[Sys_GridColumn] ADD  CONSTRAINT [DF_Com_Grid_Cloumn_Alignment]  DEFAULT ('NotSet') FOR [Alignment]
GO
/****** Object:  Default [DF_Com_Grid_Cloumn_IsReadOnly]    Script Date: 05/28/2020 10:02:12 ******/
ALTER TABLE [dbo].[Sys_GridColumn] ADD  CONSTRAINT [DF_Com_Grid_Cloumn_IsReadOnly]  DEFAULT ((1)) FOR [IsReadOnly]
GO
/****** Object:  Default [DF_Com_Grid_Cloumn_IsVisible]    Script Date: 05/28/2020 10:02:12 ******/
ALTER TABLE [dbo].[Sys_GridColumn] ADD  CONSTRAINT [DF_Com_Grid_Cloumn_IsVisible]  DEFAULT ((1)) FOR [IsVisible]
GO
/****** Object:  Default [DF_Com_Grid_Cloumn_IsAdSearch]    Script Date: 05/28/2020 10:02:12 ******/
ALTER TABLE [dbo].[Sys_GridColumn] ADD  CONSTRAINT [DF_Com_Grid_Cloumn_IsAdSearch]  DEFAULT ((1)) FOR [IsAdSearch]
GO
/****** Object:  Default [DF_Com_Grid_Cloumn_AutoSizeMode]    Script Date: 05/28/2020 10:02:12 ******/
ALTER TABLE [dbo].[Sys_GridColumn] ADD  CONSTRAINT [DF_Com_Grid_Cloumn_AutoSizeMode]  DEFAULT ('NotSet') FOR [AutoSizeMode]
GO
/****** Object:  Default [DF_Com_Grid_Cloumn_FormatStr]    Script Date: 05/28/2020 10:02:12 ******/
ALTER TABLE [dbo].[Sys_GridColumn] ADD  CONSTRAINT [DF_Com_Grid_Cloumn_FormatStr]  DEFAULT ('') FOR [FormatStr]
GO
/****** Object:  Default [DF_Sys_GridColumn_AddTime]    Script Date: 05/28/2020 10:02:12 ******/
ALTER TABLE [dbo].[Sys_GridColumn] ADD  CONSTRAINT [DF_Sys_GridColumn_AddTime]  DEFAULT (getdate()) FOR [AddTime]
GO
/****** Object:  Default [DF_Sys_GridColumn_AddUserId]    Script Date: 05/28/2020 10:02:12 ******/
ALTER TABLE [dbo].[Sys_GridColumn] ADD  CONSTRAINT [DF_Sys_GridColumn_AddUserId]  DEFAULT (NULL) FOR [AddUserId]
GO
/****** Object:  Default [DF_Sys_GridColumn_AddHost]    Script Date: 05/28/2020 10:02:12 ******/
ALTER TABLE [dbo].[Sys_GridColumn] ADD  CONSTRAINT [DF_Sys_GridColumn_AddHost]  DEFAULT (NULL) FOR [AddHost]
GO
/****** Object:  Default [DF_Com_ComboxGrid_Id]    Script Date: 05/28/2020 10:02:12 ******/
ALTER TABLE [dbo].[Sys_GridCombox] ADD  CONSTRAINT [DF_Com_ComboxGrid_Id]  DEFAULT ([dbo].[F_NewId]()) FOR [Id]
GO
/****** Object:  Default [DF_Com_ComboxGrid_MaxRowCount]    Script Date: 05/28/2020 10:02:12 ******/
ALTER TABLE [dbo].[Sys_GridCombox] ADD  CONSTRAINT [DF_Com_ComboxGrid_MaxRowCount]  DEFAULT ((10)) FOR [MaxRowCount]
GO
/****** Object:  Default [DF_Com_ComboxGrid_IsAnyKeyShow]    Script Date: 05/28/2020 10:02:12 ******/
ALTER TABLE [dbo].[Sys_GridCombox] ADD  CONSTRAINT [DF_Com_ComboxGrid_IsAnyKeyShow]  DEFAULT ((0)) FOR [IsAnyKeyShow]
GO
/****** Object:  Default [DF_Com_ComboxGrid_IsAutoSelected]    Script Date: 05/28/2020 10:02:12 ******/
ALTER TABLE [dbo].[Sys_GridCombox] ADD  CONSTRAINT [DF_Com_ComboxGrid_IsAutoSelected]  DEFAULT ((1)) FOR [IsAutoSelected]
GO
/****** Object:  Default [DF_Com_ComboxGrid_KeyPressMinlength]    Script Date: 05/28/2020 10:02:12 ******/
ALTER TABLE [dbo].[Sys_GridCombox] ADD  CONSTRAINT [DF_Com_ComboxGrid_KeyPressMinlength]  DEFAULT ((0)) FOR [KeyPressMinlength]
GO
/****** Object:  Default [DF_Com_ComboxGrid_IsSendTabKey]    Script Date: 05/28/2020 10:02:12 ******/
ALTER TABLE [dbo].[Sys_GridCombox] ADD  CONSTRAINT [DF_Com_ComboxGrid_IsSendTabKey]  DEFAULT ((1)) FOR [IsSendTabKey]
GO
/****** Object:  Default [DF_Com_GridCombox_IsUse]    Script Date: 05/28/2020 10:02:12 ******/
ALTER TABLE [dbo].[Sys_GridCombox] ADD  CONSTRAINT [DF_Com_GridCombox_IsUse]  DEFAULT ((1)) FOR [IsUse]
GO
/****** Object:  Default [DF_Com_ComboxGrid_AddTime]    Script Date: 05/28/2020 10:02:12 ******/
ALTER TABLE [dbo].[Sys_GridCombox] ADD  CONSTRAINT [DF_Com_ComboxGrid_AddTime]  DEFAULT (getdate()) FOR [AddTime]
GO
/****** Object:  Default [DF_Com_ComboxGrid_AddUserId]    Script Date: 05/28/2020 10:02:12 ******/
ALTER TABLE [dbo].[Sys_GridCombox] ADD  CONSTRAINT [DF_Com_ComboxGrid_AddUserId]  DEFAULT (NULL) FOR [AddUserId]
GO
/****** Object:  Default [DF_Com_ComboxGrid_AddHost]    Script Date: 05/28/2020 10:02:12 ******/
ALTER TABLE [dbo].[Sys_GridCombox] ADD  CONSTRAINT [DF_Com_ComboxGrid_AddHost]  DEFAULT (NULL) FOR [AddHost]
GO
/****** Object:  Default [DF_Com_ComboxGrid_Column_Id]    Script Date: 05/28/2020 10:02:12 ******/
ALTER TABLE [dbo].[Sys_GridCombox_Column] ADD  CONSTRAINT [DF_Com_ComboxGrid_Column_Id]  DEFAULT ([dbo].[F_NewId]()) FOR [Id]
GO
/****** Object:  Default [DF_Com_ComboxGrid_Column_ColumnIndex]    Script Date: 05/28/2020 10:02:12 ******/
ALTER TABLE [dbo].[Sys_GridCombox_Column] ADD  CONSTRAINT [DF_Com_ComboxGrid_Column_ColumnIndex]  DEFAULT ((0)) FOR [ColumnIndex]
GO
/****** Object:  Default [DF_Com_ComboxGrid_Column_IsVisible]    Script Date: 05/28/2020 10:02:12 ******/
ALTER TABLE [dbo].[Sys_GridCombox_Column] ADD  CONSTRAINT [DF_Com_ComboxGrid_Column_IsVisible]  DEFAULT ((1)) FOR [IsVisible]
GO
/****** Object:  Default [DF_Com_ComboxGrid_Column_ColumnWidth]    Script Date: 05/28/2020 10:02:12 ******/
ALTER TABLE [dbo].[Sys_GridCombox_Column] ADD  CONSTRAINT [DF_Com_ComboxGrid_Column_ColumnWidth]  DEFAULT ((0)) FOR [ColumnWidth]
GO
/****** Object:  Default [DF_Com_ComboxGrid_Column_Alignment]    Script Date: 05/28/2020 10:02:12 ******/
ALTER TABLE [dbo].[Sys_GridCombox_Column] ADD  CONSTRAINT [DF_Com_ComboxGrid_Column_Alignment]  DEFAULT ('NotSet') FOR [Alignment]
GO
/****** Object:  Default [DF_Com_ComboxGrid_Column_AddTime]    Script Date: 05/28/2020 10:02:12 ******/
ALTER TABLE [dbo].[Sys_GridCombox_Column] ADD  CONSTRAINT [DF_Com_ComboxGrid_Column_AddTime]  DEFAULT (getdate()) FOR [AddTime]
GO
/****** Object:  Default [DF_Com_ComboxGrid_Column_AddUserId]    Script Date: 05/28/2020 10:02:12 ******/
ALTER TABLE [dbo].[Sys_GridCombox_Column] ADD  CONSTRAINT [DF_Com_ComboxGrid_Column_AddUserId]  DEFAULT (NULL) FOR [AddUserId]
GO
/****** Object:  Default [DF_Com_ComboxGrid_Column_AddHost]    Script Date: 05/28/2020 10:02:12 ******/
ALTER TABLE [dbo].[Sys_GridCombox_Column] ADD  CONSTRAINT [DF_Com_ComboxGrid_Column_AddHost]  DEFAULT (NULL) FOR [AddHost]
GO
/****** Object:  Default [DF_Com_Menu_Id]    Script Date: 05/28/2020 10:02:12 ******/
ALTER TABLE [dbo].[Sys_Menu] ADD  CONSTRAINT [DF_Com_Menu_Id]  DEFAULT ([dbo].[F_NewId]()) FOR [Id]
GO
/****** Object:  Default [DF_Com_Menu_IsModalForm]    Script Date: 05/28/2020 10:02:12 ******/
ALTER TABLE [dbo].[Sys_Menu] ADD  CONSTRAINT [DF_Com_Menu_IsModalForm]  DEFAULT ((0)) FOR [IsModalForm]
GO
/****** Object:  Default [DF_Com_Menu_MenuLevel]    Script Date: 05/28/2020 10:02:12 ******/
ALTER TABLE [dbo].[Sys_Menu] ADD  CONSTRAINT [DF_Com_Menu_MenuLevel]  DEFAULT ((0)) FOR [MenuLevel]
GO
/****** Object:  Default [DF_Com_Menu_IsMenu]    Script Date: 05/28/2020 10:02:12 ******/
ALTER TABLE [dbo].[Sys_Menu] ADD  CONSTRAINT [DF_Com_Menu_IsMenu]  DEFAULT ((0)) FOR [IsMenu]
GO
/****** Object:  Default [DF_Com_Menu_OrderNo]    Script Date: 05/28/2020 10:02:12 ******/
ALTER TABLE [dbo].[Sys_Menu] ADD  CONSTRAINT [DF_Com_Menu_OrderNo]  DEFAULT ((1)) FOR [OrderNo]
GO
/****** Object:  Default [DF_Sys_Menu_AddTime]    Script Date: 05/28/2020 10:02:12 ******/
ALTER TABLE [dbo].[Sys_Menu] ADD  CONSTRAINT [DF_Sys_Menu_AddTime]  DEFAULT (getdate()) FOR [AddTime]
GO
/****** Object:  Default [DF_Sys_Menu_AddUserId]    Script Date: 05/28/2020 10:02:12 ******/
ALTER TABLE [dbo].[Sys_Menu] ADD  CONSTRAINT [DF_Sys_Menu_AddUserId]  DEFAULT (NULL) FOR [AddUserId]
GO
/****** Object:  Default [DF_Sys_Menu_AddHost]    Script Date: 05/28/2020 10:02:12 ******/
ALTER TABLE [dbo].[Sys_Menu] ADD  CONSTRAINT [DF_Sys_Menu_AddHost]  DEFAULT (NULL) FOR [AddHost]
GO
/****** Object:  Default [DF_Com_Role_Id]    Script Date: 05/28/2020 10:02:12 ******/
ALTER TABLE [dbo].[Sys_Role] ADD  CONSTRAINT [DF_Com_Role_Id]  DEFAULT ([dbo].[F_NewId]()) FOR [Id]
GO
/****** Object:  Default [DF_Com_Role_AddTime]    Script Date: 05/28/2020 10:02:12 ******/
ALTER TABLE [dbo].[Sys_Role] ADD  CONSTRAINT [DF_Com_Role_AddTime]  DEFAULT (getdate()) FOR [AddTime]
GO
/****** Object:  Default [DF_Com_Role_AddUserId]    Script Date: 05/28/2020 10:02:12 ******/
ALTER TABLE [dbo].[Sys_Role] ADD  CONSTRAINT [DF_Com_Role_AddUserId]  DEFAULT (NULL) FOR [AddUserId]
GO
/****** Object:  Default [DF_Com_Role_AddHost]    Script Date: 05/28/2020 10:02:12 ******/
ALTER TABLE [dbo].[Sys_Role] ADD  CONSTRAINT [DF_Com_Role_AddHost]  DEFAULT (NULL) FOR [AddHost]
GO
/****** Object:  Default [DF_Com_Role_Menu_Id]    Script Date: 05/28/2020 10:02:12 ******/
ALTER TABLE [dbo].[Sys_Role_Menu] ADD  CONSTRAINT [DF_Com_Role_Menu_Id]  DEFAULT ([dbo].[F_NewId]()) FOR [Id]
GO
/****** Object:  Default [DF_Com_Role_Menu_MenuSplit]    Script Date: 05/28/2020 10:02:12 ******/
ALTER TABLE [dbo].[Sys_Role_Menu] ADD  CONSTRAINT [DF_Com_Role_Menu_MenuSplit]  DEFAULT ((0)) FOR [IsMenuSplit]
GO
/****** Object:  Default [DF_Com_Role_Menu_OrderNo]    Script Date: 05/28/2020 10:02:12 ******/
ALTER TABLE [dbo].[Sys_Role_Menu] ADD  CONSTRAINT [DF_Com_Role_Menu_OrderNo]  DEFAULT ((0)) FOR [OrderNo]
GO
/****** Object:  Default [DF_Com_Role_Menu_IsMenu]    Script Date: 05/28/2020 10:02:12 ******/
ALTER TABLE [dbo].[Sys_Role_Menu] ADD  CONSTRAINT [DF_Com_Role_Menu_IsMenu]  DEFAULT ((0)) FOR [IsMenu]
GO
/****** Object:  Default [DF_Com_Role_Menu_IsModalForm]    Script Date: 05/28/2020 10:02:12 ******/
ALTER TABLE [dbo].[Sys_Role_Menu] ADD  CONSTRAINT [DF_Com_Role_Menu_IsModalForm]  DEFAULT ((0)) FOR [IsModalForm]
GO
/****** Object:  Default [DF_Com_Role_Menu_ToolBarVisible]    Script Date: 05/28/2020 10:02:12 ******/
ALTER TABLE [dbo].[Sys_Role_Menu] ADD  CONSTRAINT [DF_Com_Role_Menu_ToolBarVisible]  DEFAULT ((0)) FOR [ToolBarVisible]
GO
/****** Object:  Default [DF_Com_Role_Menu_ToolBarOrderNo]    Script Date: 05/28/2020 10:02:12 ******/
ALTER TABLE [dbo].[Sys_Role_Menu] ADD  CONSTRAINT [DF_Com_Role_Menu_ToolBarOrderNo]  DEFAULT ((0)) FOR [ToolBarOrderNo]
GO
/****** Object:  Default [DF_Com_Role_Menu_ToolBarSplit]    Script Date: 05/28/2020 10:02:12 ******/
ALTER TABLE [dbo].[Sys_Role_Menu] ADD  CONSTRAINT [DF_Com_Role_Menu_ToolBarSplit]  DEFAULT ((0)) FOR [ToolBarSplit]
GO
/****** Object:  Default [DF_Com_Role_Menu_AddTime]    Script Date: 05/28/2020 10:02:12 ******/
ALTER TABLE [dbo].[Sys_Role_Menu] ADD  CONSTRAINT [DF_Com_Role_Menu_AddTime]  DEFAULT (getdate()) FOR [AddTime]
GO
/****** Object:  Default [DF_Com_Role_Menu_AddUserId]    Script Date: 05/28/2020 10:02:12 ******/
ALTER TABLE [dbo].[Sys_Role_Menu] ADD  CONSTRAINT [DF_Com_Role_Menu_AddUserId]  DEFAULT (NULL) FOR [AddUserId]
GO
/****** Object:  Default [DF_Com_Role_Menu_AddHost]    Script Date: 05/28/2020 10:02:12 ******/
ALTER TABLE [dbo].[Sys_Role_Menu] ADD  CONSTRAINT [DF_Com_Role_Menu_AddHost]  DEFAULT (NULL) FOR [AddHost]
GO
