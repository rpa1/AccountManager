using System;
using ZhCun.DbCore.Entitys;

namespace ZhCun.AccountManager.DBModel
{
	public partial class TAccount : EntityBase
	{
		private System.String _Id;
		/// <summary>
		/// Id
		/// </summary>
		[Entity(ColumnName = CNId, IsPrimaryKey = true, IsNotNull = true)]
		public System.String Id
		{
			get { return _Id; }
			set
			{
				_Id = value;
				base.SetFieldChanged(CNId) ;
			}
		}

		private System.String _AccountName;
		/// <summary>
		/// AccountName
		/// </summary>
		[Entity(ColumnName = CNAccountName, IsNotNull = true)]
		public System.String AccountName
		{
			get { return _AccountName; }
			set
			{
				_AccountName = value;
				base.SetFieldChanged(CNAccountName) ;
			}
		}

		private System.String _AccountNameCode;
		/// <summary>
		/// AccountNameCode
		/// </summary>
		[Entity(ColumnName = CNAccountNameCode)]
		public System.String AccountNameCode
		{
			get { return _AccountNameCode; }
			set
			{
				_AccountNameCode = value;
				base.SetFieldChanged(CNAccountNameCode) ;
			}
		}

		private System.Decimal _Balance;
		/// <summary>
		/// Balance
		/// </summary>
		[Entity(ColumnName = CNBalance, IsNotNull = true)]
		public System.Decimal Balance
		{
			get { return _Balance; }
			set
			{
				_Balance = value;
				base.SetFieldChanged(CNBalance) ;
			}
		}

		private System.Int32 _UseCount;
		/// <summary>
		/// UseCount
		/// </summary>
		[Entity(ColumnName = CNUseCount, IsNotNull = true)]
		public System.Int32 UseCount
		{
			get { return _UseCount; }
			set
			{
				_UseCount = value;
				base.SetFieldChanged(CNUseCount) ;
			}
		}

		private System.String _Remark;
		/// <summary>
		/// Remark
		/// </summary>
		[Entity(ColumnName = CNRemark)]
		public System.String Remark
		{
			get { return _Remark; }
			set
			{
				_Remark = value;
				base.SetFieldChanged(CNRemark) ;
			}
		}

		private System.Boolean _IsDel;
		/// <summary>
		/// IsDel
		/// </summary>
		[Entity(ColumnName = CNIsDel, IsNotNull = true)]
		public System.Boolean IsDel
		{
			get { return _IsDel; }
			set
			{
				_IsDel = value;
				base.SetFieldChanged(CNIsDel) ;
			}
		}

		private System.DateTime _AddTime;
		/// <summary>
		/// AddTime
		/// </summary>
		[Entity(ColumnName = CNAddTime, IsNotNull = true)]
		public System.DateTime AddTime
		{
			get { return _AddTime; }
			set
			{
				_AddTime = value;
				base.SetFieldChanged(CNAddTime) ;
			}
		}

		private System.String _AddUserId;
		/// <summary>
		/// AddUserId
		/// </summary>
		[Entity(ColumnName = CNAddUserId)]
		public System.String AddUserId
		{
			get { return _AddUserId; }
			set
			{
				_AddUserId = value;
				base.SetFieldChanged(CNAddUserId) ;
			}
		}

		private System.String _AddHost;
		/// <summary>
		/// AddHost
		/// </summary>
		[Entity(ColumnName = CNAddHost)]
		public System.String AddHost
		{
			get { return _AddHost; }
			set
			{
				_AddHost = value;
				base.SetFieldChanged(CNAddHost) ;
			}
		}

		private System.DateTime? _LastTime;
		/// <summary>
		/// LastTime
		/// </summary>
		[Entity(ColumnName = CNLastTime)]
		public System.DateTime? LastTime
		{
			get { return _LastTime; }
			set
			{
				_LastTime = value;
				base.SetFieldChanged(CNLastTime) ;
			}
		}

		private System.String _LastHost;
		/// <summary>
		/// LastHost
		/// </summary>
		[Entity(ColumnName = CNLastHost)]
		public System.String LastHost
		{
			get { return _LastHost; }
			set
			{
				_LastHost = value;
				base.SetFieldChanged(CNLastHost) ;
			}
		}

		private System.String _LastUserId;
		/// <summary>
		/// LastUserId
		/// </summary>
		[Entity(ColumnName = CNLastUserId)]
		public System.String LastUserId
		{
			get { return _LastUserId; }
			set
			{
				_LastUserId = value;
				base.SetFieldChanged(CNLastUserId) ;
			}
		}

		#region 字段名的定义
		public const string CNId = "Id";
		public const string CNAccountName = "AccountName";
		public const string CNAccountNameCode = "AccountNameCode";
		public const string CNBalance = "Balance";
		public const string CNUseCount = "UseCount";
		public const string CNRemark = "Remark";
		public const string CNIsDel = "IsDel";
		public const string CNAddTime = "AddTime";
		public const string CNAddUserId = "AddUserId";
		public const string CNAddHost = "AddHost";
		public const string CNLastTime = "LastTime";
		public const string CNLastHost = "LastHost";
		public const string CNLastUserId = "LastUserId";
		#endregion

	}
}
