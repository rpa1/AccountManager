using System;
using ZhCun.DbCore.Entitys;

namespace ZhCun.AccountManager.DBModel
{
	public partial class TConsume : EntityBase
	{
		private System.String _Id;
		/// <summary>
		/// Id
		/// </summary>
		[Entity(ColumnName = CNId, IsPrimaryKey = true, IsNotNull = true)]
		public System.String Id
		{
			get { return _Id; }
			set
			{
				_Id = value;
				base.SetFieldChanged(CNId) ;
			}
		}

		private System.DateTime _ConsumeDate;
		/// <summary>
		/// 发生日期
		/// </summary>
		[Entity(ColumnName = CNConsumeDate, IsNotNull = true)]
		public System.DateTime ConsumeDate
		{
			get { return _ConsumeDate; }
			set
			{
				_ConsumeDate = value;
				base.SetFieldChanged(CNConsumeDate) ;
			}
		}

		private System.String _ConsumeName;
		/// <summary>
		/// ConsumeName
		/// </summary>
		[Entity(ColumnName = CNConsumeName)]
		public System.String ConsumeName
		{
			get { return _ConsumeName; }
			set
			{
				_ConsumeName = value;
				base.SetFieldChanged(CNConsumeName) ;
			}
		}

		private System.String _ConsumeTypeId;
		/// <summary>
		/// ConsumeTypeId
		/// </summary>
		[Entity(ColumnName = CNConsumeTypeId)]
		public System.String ConsumeTypeId
		{
			get { return _ConsumeTypeId; }
			set
			{
				_ConsumeTypeId = value;
				base.SetFieldChanged(CNConsumeTypeId) ;
			}
		}

		private System.String _ConsumeTypeName;
		/// <summary>
		/// ConsumeTypeName
		/// </summary>
		[Entity(ColumnName = CNConsumeTypeName)]
		public System.String ConsumeTypeName
		{
			get { return _ConsumeTypeName; }
			set
			{
				_ConsumeTypeName = value;
				base.SetFieldChanged(CNConsumeTypeName) ;
			}
		}

		private System.Int32 _BalanceTypeId;
		/// <summary>
		/// 资金类型Id
		/// </summary>
		[Entity(ColumnName = CNBalanceTypeId, IsNotNull = true)]
		public System.Int32 BalanceTypeId
		{
			get { return _BalanceTypeId; }
			set
			{
				_BalanceTypeId = value;
				base.SetFieldChanged(CNBalanceTypeId) ;
			}
		}

		private System.String _BalanceTypeName;
		/// <summary>
		/// 资金类型
		/// </summary>
		[Entity(ColumnName = CNBalanceTypeName, IsNotNull = true)]
		public System.String BalanceTypeName
		{
			get { return _BalanceTypeName; }
			set
			{
				_BalanceTypeName = value;
				base.SetFieldChanged(CNBalanceTypeName) ;
			}
		}

		private System.String _AccountId;
		/// <summary>
		/// 账户ID
		/// </summary>
		[Entity(ColumnName = CNAccountId, IsNotNull = true)]
		public System.String AccountId
		{
			get { return _AccountId; }
			set
			{
				_AccountId = value;
				base.SetFieldChanged(CNAccountId) ;
			}
		}

		private System.String _AccountName;
		/// <summary>
		/// 账户名称
		/// </summary>
		[Entity(ColumnName = CNAccountName, IsNotNull = true)]
		public System.String AccountName
		{
			get { return _AccountName; }
			set
			{
				_AccountName = value;
				base.SetFieldChanged(CNAccountName) ;
			}
		}

		private System.Decimal _Amount;
		/// <summary>
		/// 发生额
		/// </summary>
		[Entity(ColumnName = CNAmount, IsNotNull = true)]
		public System.Decimal Amount
		{
			get { return _Amount; }
			set
			{
				_Amount = value;
				base.SetFieldChanged(CNAmount) ;
			}
		}

		private System.Decimal _Balance;
		/// <summary>
		/// 账户余额
		/// </summary>
		[Entity(ColumnName = CNBalance, IsNotNull = true)]
		public System.Decimal Balance
		{
			get { return _Balance; }
			set
			{
				_Balance = value;
				base.SetFieldChanged(CNBalance) ;
			}
		}

		private System.Boolean _IsIncome;
		/// <summary>
		/// 是否收入
		/// </summary>
		[Entity(ColumnName = CNIsIncome, IsNotNull = true)]
		public System.Boolean IsIncome
		{
			get { return _IsIncome; }
			set
			{
				_IsIncome = value;
				base.SetFieldChanged(CNIsIncome) ;
			}
		}

		private System.Boolean _IsCancel;
		/// <summary>
		/// 是否冲账
		/// </summary>
		[Entity(ColumnName = CNIsCancel, IsNotNull = true)]
		public System.Boolean IsCancel
		{
			get { return _IsCancel; }
			set
			{
				_IsCancel = value;
				base.SetFieldChanged(CNIsCancel) ;
			}
		}

		private System.String _CancelRemark;
		/// <summary>
		/// 冲账说明
		/// </summary>
		[Entity(ColumnName = CNCancelRemark)]
		public System.String CancelRemark
		{
			get { return _CancelRemark; }
			set
			{
				_CancelRemark = value;
				base.SetFieldChanged(CNCancelRemark) ;
			}
		}

		private System.String _RelationlId;
		/// <summary>
		/// 关系ID，冲账关联的Id，转账关联的Id 等与其它记录相关联的id
		/// </summary>
		[Entity(ColumnName = CNRelationlId)]
		public System.String RelationlId
		{
			get { return _RelationlId; }
			set
			{
				_RelationlId = value;
				base.SetFieldChanged(CNRelationlId) ;
			}
		}

		private System.String _Remark;
		/// <summary>
		/// Remark
		/// </summary>
		[Entity(ColumnName = CNRemark)]
		public System.String Remark
		{
			get { return _Remark; }
			set
			{
				_Remark = value;
				base.SetFieldChanged(CNRemark) ;
			}
		}

		private System.DateTime _AddTime;
		/// <summary>
		/// AddTime
		/// </summary>
		[Entity(ColumnName = CNAddTime, IsNotNull = true)]
		public System.DateTime AddTime
		{
			get { return _AddTime; }
			set
			{
				_AddTime = value;
				base.SetFieldChanged(CNAddTime) ;
			}
		}

		private System.String _AddUserId;
		/// <summary>
		/// AddUserId
		/// </summary>
		[Entity(ColumnName = CNAddUserId)]
		public System.String AddUserId
		{
			get { return _AddUserId; }
			set
			{
				_AddUserId = value;
				base.SetFieldChanged(CNAddUserId) ;
			}
		}

		private System.String _AddHost;
		/// <summary>
		/// AddHost
		/// </summary>
		[Entity(ColumnName = CNAddHost)]
		public System.String AddHost
		{
			get { return _AddHost; }
			set
			{
				_AddHost = value;
				base.SetFieldChanged(CNAddHost) ;
			}
		}

		private System.DateTime? _LastTime;
		/// <summary>
		/// LastTime
		/// </summary>
		[Entity(ColumnName = CNLastTime)]
		public System.DateTime? LastTime
		{
			get { return _LastTime; }
			set
			{
				_LastTime = value;
				base.SetFieldChanged(CNLastTime) ;
			}
		}

		private System.String _LastHost;
		/// <summary>
		/// LastHost
		/// </summary>
		[Entity(ColumnName = CNLastHost)]
		public System.String LastHost
		{
			get { return _LastHost; }
			set
			{
				_LastHost = value;
				base.SetFieldChanged(CNLastHost) ;
			}
		}

		private System.String _LastUserId;
		/// <summary>
		/// LastUserId
		/// </summary>
		[Entity(ColumnName = CNLastUserId)]
		public System.String LastUserId
		{
			get { return _LastUserId; }
			set
			{
				_LastUserId = value;
				base.SetFieldChanged(CNLastUserId) ;
			}
		}

		#region 字段名的定义
		public const string CNId = "Id";
		public const string CNConsumeDate = "ConsumeDate";
		public const string CNConsumeName = "ConsumeName";
		public const string CNConsumeTypeId = "ConsumeTypeId";
		public const string CNConsumeTypeName = "ConsumeTypeName";
		public const string CNBalanceTypeId = "BalanceTypeId";
		public const string CNBalanceTypeName = "BalanceTypeName";
		public const string CNAccountId = "AccountId";
		public const string CNAccountName = "AccountName";
		public const string CNAmount = "Amount";
		public const string CNBalance = "Balance";
		public const string CNIsIncome = "IsIncome";
		public const string CNIsCancel = "IsCancel";
		public const string CNCancelRemark = "CancelRemark";
		public const string CNRelationlId = "RelationlId";
		public const string CNRemark = "Remark";
		public const string CNAddTime = "AddTime";
		public const string CNAddUserId = "AddUserId";
		public const string CNAddHost = "AddHost";
		public const string CNLastTime = "LastTime";
		public const string CNLastHost = "LastHost";
		public const string CNLastUserId = "LastUserId";
		#endregion

	}
}
