using System;
using ZhCun.DbCore.Entitys;

namespace ZhCun.AccountManager.DBModel
{
	public partial class TConsumeType : EntityBase
	{
		private System.String _Id;
		/// <summary>
		/// Id
		/// </summary>
		[Entity(ColumnName = CNId, IsPrimaryKey = true, IsNotNull = true)]
		public System.String Id
		{
			get { return _Id; }
			set
			{
				_Id = value;
				base.SetFieldChanged(CNId) ;
			}
		}

		private System.String _PId;
		/// <summary>
		/// PId
		/// </summary>
		[Entity(ColumnName = CNPId)]
		public System.String PId
		{
			get { return _PId; }
			set
			{
				_PId = value;
				base.SetFieldChanged(CNPId) ;
			}
		}

		private System.String _TypeName;
		/// <summary>
		/// TypeName
		/// </summary>
		[Entity(ColumnName = CNTypeName, IsNotNull = true)]
		public System.String TypeName
		{
			get { return _TypeName; }
			set
			{
				_TypeName = value;
				base.SetFieldChanged(CNTypeName) ;
			}
		}

		private System.String _TypeNamePY;
		/// <summary>
		/// TypeNamePY
		/// </summary>
		[Entity(ColumnName = CNTypeNamePY)]
		public System.String TypeNamePY
		{
			get { return _TypeNamePY; }
			set
			{
				_TypeNamePY = value;
				base.SetFieldChanged(CNTypeNamePY) ;
			}
		}

		private System.String _TypeNameWB;
		/// <summary>
		/// TypeNameWB
		/// </summary>
		[Entity(ColumnName = CNTypeNameWB)]
		public System.String TypeNameWB
		{
			get { return _TypeNameWB; }
			set
			{
				_TypeNameWB = value;
				base.SetFieldChanged(CNTypeNameWB) ;
			}
		}

		private System.Int32 _UseCount;
		/// <summary>
		/// UseCount
		/// </summary>
		[Entity(ColumnName = CNUseCount, IsNotNull = true)]
		public System.Int32 UseCount
		{
			get { return _UseCount; }
			set
			{
				_UseCount = value;
				base.SetFieldChanged(CNUseCount) ;
			}
		}

		private System.String _Remark;
		/// <summary>
		/// Remark
		/// </summary>
		[Entity(ColumnName = CNRemark)]
		public System.String Remark
		{
			get { return _Remark; }
			set
			{
				_Remark = value;
				base.SetFieldChanged(CNRemark) ;
			}
		}

		private System.Boolean _IsType;
		/// <summary>
		/// 是否为类型，二不是分类节点
		/// </summary>
		[Entity(ColumnName = CNIsType, IsNotNull = true)]
		public System.Boolean IsType
		{
			get { return _IsType; }
			set
			{
				_IsType = value;
				base.SetFieldChanged(CNIsType) ;
			}
		}

		private System.Int32 _OrderNo;
		/// <summary>
		/// OrderNo
		/// </summary>
		[Entity(ColumnName = CNOrderNo, IsNotNull = true)]
		public System.Int32 OrderNo
		{
			get { return _OrderNo; }
			set
			{
				_OrderNo = value;
				base.SetFieldChanged(CNOrderNo) ;
			}
		}

		private System.Boolean _IsBalanceOut;
		/// <summary>
		/// 是否资金支出
		/// </summary>
		[Entity(ColumnName = CNIsBalanceOut, IsNotNull = true)]
		public System.Boolean IsBalanceOut
		{
			get { return _IsBalanceOut; }
			set
			{
				_IsBalanceOut = value;
				base.SetFieldChanged(CNIsBalanceOut) ;
			}
		}

		private System.Boolean _IsBalanceIn;
		/// <summary>
		/// 是否资金收入
		/// </summary>
		[Entity(ColumnName = CNIsBalanceIn, IsNotNull = true)]
		public System.Boolean IsBalanceIn
		{
			get { return _IsBalanceIn; }
			set
			{
				_IsBalanceIn = value;
				base.SetFieldChanged(CNIsBalanceIn) ;
			}
		}

		private System.DateTime _AddTime;
		/// <summary>
		/// AddTime
		/// </summary>
		[Entity(ColumnName = CNAddTime, IsNotNull = true)]
		public System.DateTime AddTime
		{
			get { return _AddTime; }
			set
			{
				_AddTime = value;
				base.SetFieldChanged(CNAddTime) ;
			}
		}

		private System.String _AddUserId;
		/// <summary>
		/// AddUserId
		/// </summary>
		[Entity(ColumnName = CNAddUserId)]
		public System.String AddUserId
		{
			get { return _AddUserId; }
			set
			{
				_AddUserId = value;
				base.SetFieldChanged(CNAddUserId) ;
			}
		}

		private System.String _AddHost;
		/// <summary>
		/// AddHost
		/// </summary>
		[Entity(ColumnName = CNAddHost)]
		public System.String AddHost
		{
			get { return _AddHost; }
			set
			{
				_AddHost = value;
				base.SetFieldChanged(CNAddHost) ;
			}
		}

		private System.DateTime? _LastTime;
		/// <summary>
		/// LastTime
		/// </summary>
		[Entity(ColumnName = CNLastTime)]
		public System.DateTime? LastTime
		{
			get { return _LastTime; }
			set
			{
				_LastTime = value;
				base.SetFieldChanged(CNLastTime) ;
			}
		}

		private System.String _LastHost;
		/// <summary>
		/// LastHost
		/// </summary>
		[Entity(ColumnName = CNLastHost)]
		public System.String LastHost
		{
			get { return _LastHost; }
			set
			{
				_LastHost = value;
				base.SetFieldChanged(CNLastHost) ;
			}
		}

		private System.String _LastUserId;
		/// <summary>
		/// LastUserId
		/// </summary>
		[Entity(ColumnName = CNLastUserId)]
		public System.String LastUserId
		{
			get { return _LastUserId; }
			set
			{
				_LastUserId = value;
				base.SetFieldChanged(CNLastUserId) ;
			}
		}

		#region 字段名的定义
		public const string CNId = "Id";
		public const string CNPId = "PId";
		public const string CNTypeName = "TypeName";
		public const string CNTypeNamePY = "TypeNamePY";
		public const string CNTypeNameWB = "TypeNameWB";
		public const string CNUseCount = "UseCount";
		public const string CNRemark = "Remark";
		public const string CNIsType = "IsType";
		public const string CNOrderNo = "OrderNo";
		public const string CNIsBalanceOut = "IsBalanceOut";
		public const string CNIsBalanceIn = "IsBalanceIn";
		public const string CNAddTime = "AddTime";
		public const string CNAddUserId = "AddUserId";
		public const string CNAddHost = "AddHost";
		public const string CNLastTime = "LastTime";
		public const string CNLastHost = "LastHost";
		public const string CNLastUserId = "LastUserId";
		#endregion

	}
}
