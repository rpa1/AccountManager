﻿/**************************************************************************
创建时间:	2020/5/20 14:04:59    
作	  者:	张存
邮 	  箱:	zhangcunliang@126.com

Copyright (c) zhcun.cn

描	述：
记	录：
***************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZhCun.DbCore.Entitys;
using ZhCun.Utils;

namespace ZhCun.AccountManager.DBModel
{
    partial class TConsumeType : ITreeModel
    {
        [Entity(IsNotField = true)]
        public string Caption { get { return TypeName; } }
    }
}
