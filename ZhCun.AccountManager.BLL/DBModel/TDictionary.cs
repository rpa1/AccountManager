using System;
using ZhCun.DbCore.Entitys;

namespace ZhCun.AccountManager.DBModel
{
	public partial class TDictionary : EntityBase
	{
		private System.Int32 _DictKey;
		/// <summary>
		/// DictKey
		/// </summary>
		[Entity(ColumnName = CNDictKey, IsPrimaryKey = true, IsNotNull = true)]
		public System.Int32 DictKey
		{
			get { return _DictKey; }
			set
			{
				_DictKey = value;
				base.SetFieldChanged(CNDictKey) ;
			}
		}

		private System.String _DictKeyPrompt;
		/// <summary>
		/// DictKeyPrompt
		/// </summary>
		[Entity(ColumnName = CNDictKeyPrompt)]
		public System.String DictKeyPrompt
		{
			get { return _DictKeyPrompt; }
			set
			{
				_DictKeyPrompt = value;
				base.SetFieldChanged(CNDictKeyPrompt) ;
			}
		}

		private System.Int32 _DictValue;
		/// <summary>
		/// DictValue
		/// </summary>
		[Entity(ColumnName = CNDictValue, IsPrimaryKey = true, IsNotNull = true)]
		public System.Int32 DictValue
		{
			get { return _DictValue; }
			set
			{
				_DictValue = value;
				base.SetFieldChanged(CNDictValue) ;
			}
		}

		private System.String _DictValuePrompt;
		/// <summary>
		/// DictValuePrompt
		/// </summary>
		[Entity(ColumnName = CNDictValuePrompt)]
		public System.String DictValuePrompt
		{
			get { return _DictValuePrompt; }
			set
			{
				_DictValuePrompt = value;
				base.SetFieldChanged(CNDictValuePrompt) ;
			}
		}

		private System.String _Remark;
		/// <summary>
		/// Remark
		/// </summary>
		[Entity(ColumnName = CNRemark)]
		public System.String Remark
		{
			get { return _Remark; }
			set
			{
				_Remark = value;
				base.SetFieldChanged(CNRemark) ;
			}
		}

		#region 字段名的定义
		public const string CNDictKey = "DictKey";
		public const string CNDictKeyPrompt = "DictKeyPrompt";
		public const string CNDictValue = "DictValue";
		public const string CNDictValuePrompt = "DictValuePrompt";
		public const string CNRemark = "Remark";
		#endregion

	}
}
