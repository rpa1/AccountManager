using System;
using ZhCun.DbCore.Entitys;

namespace ZhCun.AccountManager.DBModel
{
	public partial class TUser : EntityBase
	{
		private System.String _Id;
		/// <summary>
		/// Id
		/// </summary>
		[Entity(ColumnName = CNId, IsPrimaryKey = true, IsNotNull = true)]
		public System.String Id
		{
			get { return _Id; }
			set
			{
				_Id = value;
				base.SetFieldChanged(CNId) ;
			}
		}

		private System.String _UserName;
		/// <summary>
		/// UserName
		/// </summary>
		[Entity(ColumnName = CNUserName, IsNotNull = true)]
		public System.String UserName
		{
			get { return _UserName; }
			set
			{
				_UserName = value;
				base.SetFieldChanged(CNUserName) ;
			}
		}

		private System.String _LoginName;
		/// <summary>
		/// LoginName
		/// </summary>
		[Entity(ColumnName = CNLoginName, IsNotNull = true)]
		public System.String LoginName
		{
			get { return _LoginName; }
			set
			{
				_LoginName = value;
				base.SetFieldChanged(CNLoginName) ;
			}
		}

		private System.String _LoginPwd;
		/// <summary>
		/// LoginPwd
		/// </summary>
		[Entity(ColumnName = CNLoginPwd, IsNotNull = true)]
		public System.String LoginPwd
		{
			get { return _LoginPwd; }
			set
			{
				_LoginPwd = value;
				base.SetFieldChanged(CNLoginPwd) ;
			}
		}

		private System.String _RoleId;
		/// <summary>
		/// RoleId
		/// </summary>
		[Entity(ColumnName = CNRoleId, IsNotNull = true)]
		public System.String RoleId
		{
			get { return _RoleId; }
			set
			{
				_RoleId = value;
				base.SetFieldChanged(CNRoleId) ;
			}
		}

		private System.Int32 _LoginCount;
		/// <summary>
		/// LoginCount
		/// </summary>
		[Entity(ColumnName = CNLoginCount, IsNotNull = true)]
		public System.Int32 LoginCount
		{
			get { return _LoginCount; }
			set
			{
				_LoginCount = value;
				base.SetFieldChanged(CNLoginCount) ;
			}
		}

		private System.DateTime? _LastLoginTime;
		/// <summary>
		/// LastLoginTime
		/// </summary>
		[Entity(ColumnName = CNLastLoginTime)]
		public System.DateTime? LastLoginTime
		{
			get { return _LastLoginTime; }
			set
			{
				_LastLoginTime = value;
				base.SetFieldChanged(CNLastLoginTime) ;
			}
		}

		private System.Boolean _IsUse;
		/// <summary>
		/// IsUse
		/// </summary>
		[Entity(ColumnName = CNIsUse, IsNotNull = true)]
		public System.Boolean IsUse
		{
			get { return _IsUse; }
			set
			{
				_IsUse = value;
				base.SetFieldChanged(CNIsUse) ;
			}
		}

		private System.Boolean _IsManager;
		/// <summary>
		/// 是否管理员
		/// </summary>
		[Entity(ColumnName = CNIsManager, IsNotNull = true)]
		public System.Boolean IsManager
		{
			get { return _IsManager; }
			set
			{
				_IsManager = value;
				base.SetFieldChanged(CNIsManager) ;
			}
		}

		private System.DateTime _AddTime;
		/// <summary>
		/// AddTime
		/// </summary>
		[Entity(ColumnName = CNAddTime, IsNotNull = true)]
		public System.DateTime AddTime
		{
			get { return _AddTime; }
			set
			{
				_AddTime = value;
				base.SetFieldChanged(CNAddTime) ;
			}
		}

		private System.DateTime? _LastTime;
		/// <summary>
		/// LastTime
		/// </summary>
		[Entity(ColumnName = CNLastTime)]
		public System.DateTime? LastTime
		{
			get { return _LastTime; }
			set
			{
				_LastTime = value;
				base.SetFieldChanged(CNLastTime) ;
			}
		}

		#region 字段名的定义
		public const string CNId = "Id";
		public const string CNUserName = "UserName";
		public const string CNLoginName = "LoginName";
		public const string CNLoginPwd = "LoginPwd";
		public const string CNRoleId = "RoleId";
		public const string CNLoginCount = "LoginCount";
		public const string CNLastLoginTime = "LastLoginTime";
		public const string CNIsUse = "IsUse";
		public const string CNIsManager = "IsManager";
		public const string CNAddTime = "AddTime";
		public const string CNLastTime = "LastTime";
		#endregion

	}
}
