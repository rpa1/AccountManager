﻿/**************************************************************************
创建时间:	2020/5/20 8:49:51    
作	  者:	张存
邮 	  箱:	zhangcunliang@126.com

Copyright (c) zhcun.cn

描	述：
记	录：
***************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZhCun.AccountManager.BLL
{
    /// <summary>
    /// 标记数据权限将被禁用，不进行sql注入相关操作
    /// </summary>

    public class IgnoreDataRightAttribute : Attribute
    { }
}