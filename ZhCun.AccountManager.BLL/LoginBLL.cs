﻿/**************************************************************************
创建时间:	2020/5/17 20:27:24    
作	  者:	张存
邮 	  箱:	zhangcunliang@126.com

Copyright (c) zhcun.cn

描	述：
记	录：
***************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZhCun.AccountManager.DBModel;
using ZhCun.Utils;

namespace ZhCun.AccountManager.BLL
{
    public class LoginBLL : BLLBase
    {
        public ApiResult<TUser> LoginVerify(string loginName, string loginPwd)
        {
            var user = DB.Query<TUser>(s => s.LoginName == loginName && s.LoginPwd == loginPwd).ToEntity();
            if (user == null)
            {
                return RetErr<TUser>("用户名或密码错误");
            }
            else
            {
                CurrUser = user;
                user.LoginCount++;
                user.LastLoginTime = DateTime.Now;
                DB.Update(user);
                
                return RetOK(user);
            }
        }
    }
}
