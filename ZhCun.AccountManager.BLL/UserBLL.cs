﻿/**************************************************************************
创建时间:	2020/5/20 10:42:24    
作	  者:	张存
邮 	  箱:	zhangcunliang@126.com

Copyright (c) zhcun.cn

描	述：
记	录：
***************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZhCun.AccountManager.DBModel;
using ZhCun.DbCore.Cores;
using ZhCun.Utils;
using ZhCun.Utils.Helpers;

namespace ZhCun.AccountManager.BLL
{
    public class UserBLL : BLLBaseCrud<TUser>
    {
        public override ApiResult Del(TUser model)
        {
            if (model.LastTime != null)
            {
                return RetErr("账户已经使用不允许删除");
            }

            return base.Del(model);
        }

        protected override void SetQuery(QueryCondition<TUser> query, string searchVal)
        {
            if (!searchVal.IsEmpty())
            {
                string likeStr = $"%{searchVal}%";
                query.WhereAnd(s =>
                    s.WEx_Like(s.UserName, likeStr) ||
                    s.WEx_Like(s.LoginName, likeStr)
                    );
            }
            query.OrderBy(s => s.AddTime);
        }

        protected override ApiResult<string> VerifyModel(TUser model, bool isAdd)
        {
            if (model.UserName.IsEmpty())
            {
                return RetErr(nameof(model.UserName), "用户名称不能为空");
            }
            if (model.LoginName.IsEmpty())
            {
                return RetErr(nameof(model.LoginName), "登陆名不能为空");
            }
            if (DB.QueryExist<TUser>(s => s.UserName == model.UserName && s.Id != model.Id))
            {
                return RetErr(nameof(model.UserName), "用户名已存在");
            }
            if (DB.QueryExist<TUser>(s => s.LoginName == model.LoginName && s.Id != model.Id))
            {
                return RetErr(nameof(model.LoginName), "登陆名已存在");
            }
            if (isAdd)
            {
                model.LoginPwd = Config.Settings.DefaultUserPwd;
                model.RoleId = Config.Settings.UserRoleId;
                Task.Factory.StartNew(AddUserData, model);
            }
            else
            {
                model.ClearChangedState();
                model.SetFieldChanged(TUser.CNLoginName, TUser.CNUserName);
            }
            return RetOK<string>(string.Empty, isAdd ? $"新增用户成功，默认密码：{Config.Settings.DefaultUserPwd}" : "更新用户成功");
        }

        static void AddUserData(object state)
        {
            var db = new DBHelper(null);
            TUser user = state as TUser;
            var managerUserId = Config.Settings.ManageUserId;

            var accountList = db.Query<TAccount>(s => s.AddUserId == managerUserId).ToList(true);
            TAccount account = new TAccount();
            foreach (var item in accountList)
            {
                account.Id = GuidHelper.NewId();
                account.AccountName = item.AccountName;
                account.AccountNameCode = item.AccountNameCode;
                account.AddUserId = user.Id;
                db.Insert(account, false, false);
            }
            var consumeTypeLst = db.Query<TConsumeType>(s => s.AddUserId == managerUserId).ToList(true);
            TConsumeType consumeType = new TConsumeType();
            foreach (var item in consumeTypeLst)
            {
                consumeType.Id = GuidHelper.NewId();
                consumeType.TypeName = item.TypeName;
                consumeType.TypeNamePY = item.TypeNamePY;
                consumeType.TypeNameWB = item.TypeNamePY;
                consumeType.IsBalanceIn = item.IsBalanceIn;
                consumeType.IsBalanceOut = item.IsBalanceOut;
                consumeType.AddUserId = user.Id;
                consumeType.IsType = true;
                db.Insert(consumeType, false, false);
            }
        }
    }
}