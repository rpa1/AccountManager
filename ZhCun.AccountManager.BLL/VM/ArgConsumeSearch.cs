﻿/**************************************************************************
创建时间:	2020/5/21 21:24:36    
作	  者:	张存
邮 	  箱:	zhangcunliang@126.com

Copyright (c) zhcun.cn

描	述：
记	录：
***************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZhCun.AccountManager.BLL.VM
{
    /// <summary>
    /// 消费查询的条件对象
    /// </summary>
    public class ArgConsumeSearch: ArgSearchBase
    {
        public DateRange DataRange { set; get; }

        public DateTime? StartDate { set; get; }

        public DateTime? EndDate { set; get; }

        /// <summary>
        /// 收支差金额
        /// </summary>
        public decimal AmountSum { set; get; }
        /// <summary>
        /// 支出金额
        /// </summary>
        public decimal AmountSumOut { set; get; }
        /// <summary>
        /// 收入金额
        /// </summary>
        public decimal AmountSumIn { set; get; }
    }
}