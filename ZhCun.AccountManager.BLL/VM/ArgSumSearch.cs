﻿/**************************************************************************
创建时间:	2020/5/27 21:57:57    
作	  者:	张存
邮 	  箱:	zhangcunliang@126.com

Copyright (c) zhcun.cn

描	述：
记	录：
***************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZhCun.AccountManager.BLL.VM
{
    public class ArgSumSearch
    {        
        public DateTime StartTime { set; get; }

        public DateTime EndTime { set; get; }
        /// <summary>
        /// 0: 记录时间，1：消费时间
        /// </summary>
        public int DateType { set; get; }
        /// <summary>
        /// 支出总额 输出
        /// </summary>
        public decimal SumAmountOut { get; internal set; }
        /// <summary>
        /// 收入总额 输出
        /// </summary>
        public decimal SumAmountIn { get; internal set; }
    }
}