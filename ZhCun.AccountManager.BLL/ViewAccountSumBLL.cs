﻿/**************************************************************************
创建时间:	2020/5/23 17:53:44    
作	  者:	张存
邮 	  箱:	zhangcunliang@126.com

Copyright (c) zhcun.cn

描	述：账户的月统计，显示指定月份的每一天账户情况
记	录：
***************************************************************************/
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using ZhCun.AccountManager.BLL.VM;
using ZhCun.AccountManager.DBModel;
using ZhCun.DbCore.BuildSQLText;

namespace ZhCun.AccountManager.BLL
{
    public class ViewAccountSumBLL : ViewDateSumBase
    {
        List<TAccount> _AccountList;

        public List<TAccount> AccountList
        {
            get
            {
                if (_AccountList == null)
                {
                    var query = DB.CreateQuery<TAccount>();
                    query.OrderBy(s => s.UseCount, DbCore.EmQueryOrder.Desc);
                    _AccountList = DB.Query<TAccount>(query).ToList(true);
                }
                return _AccountList;
            }
        }

        ///// <summary>
        ///// 获取账户日报表统计（显示每一天）
        ///// </summary>
        //DataTable GetAccountDay(int year, int month, int dateType)
        //{
        //    DateTime startTime = new DateTime(year, month, 1);
        //    DateTime endTime = startTime.AddMonths(1);
        //    var query = DB.CreateQuery<TConsume>();
        //    if (dateType == 1)
        //    {
        //        query.WhereAnd(s => s.AddTime >= startTime && s.AddTime < endTime);
        //    }
        //    else
        //    {
        //        query.WhereAnd(s => s.ConsumeDate >= startTime && s.ConsumeDate < endTime);
        //    }
        //    List<TConsume> rData = DB.Query<TConsume>(query).ToList(true);

        //    var account = AccountList;

        //    //IEnumerable<IGrouping<DateTime, TConsume>> groupTime =
        //    //    dateType == 1 ? rData.GroupBy(s => s.ConsumeDate) : rData.GroupBy(s => s.AddTime.Date);

        //    DataTable dt = new DataTable();
        //    dt.Columns.Add("日期");
        //    foreach (var item in account)
        //    {
        //        dt.Columns.Add(item.AccountName);
        //    }
        //    decimal accountAmount;
        //    for (int i = 1; i <= endTime.AddDays(-1).Day; i++)
        //    {
        //        var dr = dt.NewRow();
        //        dr[0] = i;

        //        foreach (var itemAccount in account)
        //        {
        //            accountAmount =
        //                dateType == 1 ?
        //                rData.Where(s => s.AccountId == itemAccount.Id && s.ConsumeDate.Day == i).Sum(s => s.Amount)
        //              : rData.Where(s => s.AccountId == itemAccount.Id && s.AddTime.Day == i).Sum(s => s.Amount);

        //            dr[itemAccount.AccountName] = accountAmount;
        //        }
        //        dt.Rows.Add(dr);
        //    }
        //    return dt;
        //}


        /// <summary>
        /// 获取账户日报表统计（显示每一天）
        /// </summary>
        public DataTable GetViewData(ArgSumSearch arg)
        {
            var detail = GetViewDetail(arg);
            decimal inAmount, outAmount;
            DataTable dt = new DataTable();
            dt.Columns.Add("AccountName");
            dt.Columns.Add("AmountIn");
            dt.Columns.Add("AmountOut");
            dt.Columns.Add("Amount");
            dt.Columns.Add("UseCount");
            foreach (var item in AccountList)
            {
                var dr = dt.NewRow();
                dr["AccountName"] = item.AccountName;

                var accountData = detail.Where(s => s.AccountId == item.Id);
                outAmount = accountData.Where(s => s.Amount < 0).Sum(s => s.Amount);
                inAmount = accountData.Where(s => s.Amount > 0).Sum(s => s.Amount);

                dr["AmountIn"] = inAmount;
                dr["AmountOut"] = outAmount;
                dr["Amount"] = inAmount + outAmount;
                dr["UseCount"] = accountData.Count();
                dt.Rows.Add(dr);
            }
            return dt;
        }
    }
}