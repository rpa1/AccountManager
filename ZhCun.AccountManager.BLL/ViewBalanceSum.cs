﻿/**************************************************************************
创建时间:	2020/5/23 22:42:15    
作	  者:	张存
邮 	  箱:	zhangcunliang@126.com

Copyright (c) zhcun.cn

描	述：
记	录：
***************************************************************************/
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using ZhCun.AccountManager.BLL.VM;
using ZhCun.AccountManager.DBModel;
using ZhCun.Utils.Helpers;

namespace ZhCun.AccountManager.BLL
{
    public class ViewBalanceSum : ViewDateSumBase
    {
        /// <summary>
        /// 获取账户日报表统计（显示每一天）
        /// </summary>
        public DataTable GetViewData(ArgSumSearch arg)
        {
            var detail = GetViewDetail(arg);
            DataTable dt = new DataTable();
            dt.Columns.Add("BalanceType");
            dt.Columns.Add("Amount");
            dt.Columns.Add("UseCount");

            var typeNames = Enum.GetNames(typeof(BalanceType));

            foreach (var item in typeNames)
            {
                var dr = dt.NewRow();
                dr["BalanceType"] = item.ToString();
                EnumHelper.StringToEnum(item, out BalanceType btype);
                var groupData = detail.Where(s => btype.EqualValue(s.BalanceTypeId));
                dr["Amount"] = groupData.Sum(s => s.Amount);
                dr["UseCount"] = groupData.Count();
                dt.Rows.Add(dr);
            }
            return dt;
        }
    }
}