﻿/**************************************************************************
创建时间:	2020/5/21
作	  者:	张存
邮 	  箱:	zhangcunliang@126.com

Copyright (c) zhcun.cn

描	述：
记	录：
***************************************************************************/
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using ZhCun.AccountManager.BLL.VM;
using ZhCun.AccountManager.DBModel;
using ZhCun.DbCore;
using ZhCun.Utils.Helpers;

namespace ZhCun.AccountManager.BLL
{
    public class ViewDetailBLL : BLLBase
    {
        public DataTable GetData(ArgDetailViewSearch arg)
        {
            var query = DB.CreateQuery<TConsume>();
            query.PageNo = arg.PageNo;
            query.PageSize = arg.PageSize;
            if (arg.DateType == 1)
            {
                query.WhereAnd(s => s.ConsumeDate >= arg.StartTime.Date && s.ConsumeDate < arg.EndTime.Date.AddDays(1));
                query.OrderBy(s => s.ConsumeDate, EmQueryOrder.Desc);
            }
            else
            {
                query.WhereAnd(s => s.AddTime >= arg.StartTime.Date && s.AddTime <= arg.EndTime.Date.AddDays(1));
                query.OrderBy(s => s.AddTime, EmQueryOrder.Desc);
            }
            if (!arg.SearchVal.IsEmpty())
            {
                string likeStr = $"%{arg.SearchVal}%";
                query.WhereAnd(s =>
                    s.WEx_Like(s.ConsumeName, likeStr) ||
                    s.WEx_Like(s.ConsumeTypeName, likeStr));
            }
            var dt = DB.Query<TConsume>(query, ADJoinSql).ToDataTable();
            arg.OutRowCount = query.Total;
            //arg.AmountSumOut = DB.QuerySum<TConsume>(s => s.Amount, query, ADJoinSql).ToObject<decimal>();

            query.WhereAnd(s => s.WEx_In(s.BalanceTypeId, 1001, 1003, 2001, 2003));
            //收支差额
            arg.AmountSum = DB.QuerySum<TConsume>(s => s.Amount, query, ADJoinSql).ToObject<decimal>();
            //支出总额
            query.WhereAnd(s => s.BalanceTypeId < 2000);
            arg.AmountSumOut = DB.QuerySum<TConsume>(s => s.Amount, query, ADJoinSql).ToObject<decimal>();
            arg.AmountSumIn = arg.AmountSum - arg.AmountSumOut;

            return dt;
        }
    }
}