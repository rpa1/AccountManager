﻿/**************************************************************************
创建时间:	2020/5/23 22:58:54    
作	  者:	张存
邮 	  箱:	zhangcunliang@126.com

Copyright (c) zhcun.cn

描	述：
记	录：
***************************************************************************/
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using ZhCun.AccountManager.BLL.VM;
using ZhCun.AccountManager.DBModel;

namespace ZhCun.AccountManager.BLL
{
    public class ViewMonthBLL : ViewDateSumBase
    {
        //月报表不能查明细后统计了,数据量大会影响查询速度
        public DataTable GetViewData(ArgSumSearch arg)
        {
            var detail = GetViewDetail(arg);

            decimal inAmount, outAmount;

            DataTable dt = new DataTable();
            dt.Columns.Add("Date");
            dt.Columns.Add("AmountIn");
            dt.Columns.Add("AmountOut");
            dt.Columns.Add("Amount");
            dt.Columns.Add("UseCount");

            DateTime date = arg.StartTime;
            while (date <= arg.EndTime)
            {
                var dr = dt.NewRow();
                dr["Date"] = date;
                var groupData = arg.DateType == 1
                   ? detail.Where(s => s.ConsumeDate >= date.Date && s.ConsumeDate < date.AddDays(1))
                   : detail.Where(s => s.AddTime.Date >= date.Date && s.AddTime < date.AddDays(1));

                outAmount = groupData.Where(s => s.Amount < 0).Sum(s => s.Amount);
                inAmount = groupData.Where(s => s.Amount > 0).Sum(s => s.Amount);
                dr["AmountIn"] = inAmount;
                dr["AmountOut"] = outAmount;
                dr["Amount"] = inAmount + outAmount;
                dr["UseCount"] = groupData.Count();

                dt.Rows.Add(dr);
                date = date.AddDays(1);
            }
            return dt;
        }
    }
}