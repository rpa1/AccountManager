﻿/**************************************************************************
创建时间:	2020/5/23 22:28:29    
作	  者:	张存
邮 	  箱:	zhangcunliang@126.com

Copyright (c) zhcun.cn

描	述：
记	录：
***************************************************************************/
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using ZhCun.AccountManager.BLL.VM;
using ZhCun.AccountManager.DBModel;

namespace ZhCun.AccountManager.BLL
{
    public class ViewTypeSumBLL : ViewDateSumBase
    {
        List<TConsumeType> _TypeList;

        public List<TConsumeType> TypeList
        {
            get
            {
                if (_TypeList == null)
                {
                    var query = DB.CreateQuery<TConsumeType>();
                    query.OrderBy(s => s.UseCount, DbCore.EmQueryOrder.Desc);
                    _TypeList = DB.Query<TConsumeType>(query).ToList(true);
                }
                return _TypeList;
            }
        }

        /// <summary>
        /// 类型统计
        /// </summary>
        public DataTable GetViewData(ArgSumSearch arg)
        {
            var detail = GetViewDetail(arg);

            decimal inAmount, outAmount;

            DataTable dt = new DataTable();
            dt.Columns.Add("TypeName");
            dt.Columns.Add("AmountIn");
            dt.Columns.Add("AmountOut");
            dt.Columns.Add("Amount");
            dt.Columns.Add("UseCount");
            foreach (var item in TypeList)
            {
                var dr = dt.NewRow();
                dr["TypeName"] = item.TypeName;

                var accountData = detail.Where(s => s.ConsumeTypeId == item.Id);
                outAmount = accountData.Where(s => s.Amount < 0).Sum(s => s.Amount);
                inAmount = accountData.Where(s => s.Amount > 0).Sum(s => s.Amount);
                dr["AmountIn"] = inAmount;
                dr["AmountOut"] = outAmount;
                dr["Amount"] = inAmount + outAmount;
                dr["UseCount"] = accountData.Count();
                dt.Rows.Add(dr);
            }
            return dt;
        }
    }
}
