﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ZhCun.WebUtils;

namespace ZhCun.AccountManager.WebServer.ApiControllers
{
    //[ApiExplorerSettings(GroupName = "v1")]
    public class ApiTestController: BaseApiController
    {
        [HttpGet]
        [AllowAnonymous]
        public string GetNow()
        {
            return $"当前时间: {DateTime.Now}";
        }
    }
}