using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using ZhCun.Utils;
using ZhCun.WebUtils;

namespace ZhCun.AccountManager.WebServer
{
    public class Program
    {
        public static void Main(string[] args)
        {
            LogHelper.LogObj.Info("app runing");
            HostHelper.CreateWebHostBuilder(args)
                .UseStartup<Startup>()
                .Build()
                .Run();
        }
    }
}
