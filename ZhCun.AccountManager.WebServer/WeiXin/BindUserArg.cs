﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ZhCun.AccountManager.WebServer.WeiXin
{
    public class BindUserArg
    {
        public string BindId { set; get; }

        public string LoginName { set; get; }
        
        public string LoginPwd { set; get; }
    }
}
