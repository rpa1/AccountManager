﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ZhCun.AccountManager.WebServer.WeiXin
{
    //参考文档: https://developers.weixin.qq.com/ebook?action=get_post_info&docid=000cc48f96c5989b0086ddc7e56c0a

    /// <summary>
    /// 微信的用户信息
    /// </summary>
    public class UserSession
    {
        /// <summary>
        /// 微信用户的唯一标识
        /// </summary>
        public string openid { set; get; }
        /// <summary>
        /// 会话密钥
        /// </summary>
        public string session_key { set; get; }
        /// <summary>
        /// 用户在微信开放平台的唯一标识符。本字段在满足一定条件的情况下才返回。
        /// </summary>
        public string unionid { set; get; }

        public int errcode { set; get; }

        public string errmsg { set; get; }
    }
}