﻿/**************************************************************************
创建时间:	2020/5/21
作	  者:	张存
邮 	  箱:	zhangcunliang@126.com

Copyright (c) zhcun.cn

描	述：
记	录：
***************************************************************************/
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using ZhCun.Win;

namespace ZhCun.AccountManager
{
    static class FormExtend
    {
        static Font LargeFont = new Font("微软雅黑", 14F, FontStyle.Regular, GraphicsUnit.Point, 134);

        public static void InitLargeFont(this FrmBase frm)
        {
            frm.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            frm.Font = LargeFont;
        }
    }
}
