﻿/**************************************************************************
创建时间:	2020/5/19 14:48:57    
作	  者:	张存
邮 	  箱:	zhangcunliang@126.com

Copyright (c) zhcun.cn

描	述：
记	录：
***************************************************************************/
using ZhCun.AccountManager.BLL;
using ZhCun.AccountManager.DBModel;
using ZhCun.Win;

namespace ZhCun.AccountManager
{
    public partial class FrmAccount : FrmBaseCrud<TAccount, TAccount>
    {
        public FrmAccount()
        {
            InitializeComponent();
        }

        readonly AccountBLL _BLLObj = new AccountBLL();

        protected override BLLBaseCrud<TAccount, TAccount> BLLObj => _BLLObj;

        /// <summary>
        /// 创建新增窗体的方法
        /// </summary>
        protected override FrmBaseEdit CreateAddForm()
        {
            var frm = new FrmAccountEdit();
            return frm;
        }

        private void FrmAccount_Load(object sender, System.EventArgs e)
        {

        }
    }
}