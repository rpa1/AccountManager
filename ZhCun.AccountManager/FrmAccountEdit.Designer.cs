﻿namespace ZhCun.AccountManager
{
    partial class FrmAccountEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.editText1 = new ZhCun.Win.Controls.EditText();
            this.label2 = new System.Windows.Forms.Label();
            this.txtPYCode = new ZhCun.Win.Controls.EditText();
            this.label3 = new System.Windows.Forms.Label();
            this.editText3 = new ZhCun.Win.Controls.EditText();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 180);
            this.panel1.Size = new System.Drawing.Size(400, 42);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(224, 0);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(310, 0);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 20);
            this.label1.TabIndex = 3;
            this.label1.Text = "账户名称：";
            // 
            // editText1
            // 
            this.editText1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.editText1.CodePYTextBox = this.txtPYCode;
            this.editText1.CodeWBTextBox = null;
            this.editText1.DisplayItem = "";
            this.dcm.SetDisplayMember(this.editText1, "AccountName");
            this.editText1.EmptyTextTip = null;
            this.editText1.EmptyTextTipColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.dcm.SetEnterIsTab(this.editText1, true);
            this.dcm.SetIsUse(this.editText1, true);
            this.editText1.Location = new System.Drawing.Point(105, 20);
            this.editText1.MaxLength = 20;
            this.editText1.Name = "editText1";
            this.editText1.RealValue = "";
            this.editText1.Size = new System.Drawing.Size(248, 26);
            this.editText1.TabIndex = 1;
            this.editText1.ValueItem = null;
            this.dcm.SetValueMember(this.editText1, "Id");
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(37, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 20);
            this.label2.TabIndex = 3;
            this.label2.Text = "助记码：";
            // 
            // txtPYCode
            // 
            this.txtPYCode.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPYCode.CodePYTextBox = null;
            this.txtPYCode.CodeWBTextBox = null;
            this.txtPYCode.DisplayItem = "";
            this.dcm.SetDisplayMember(this.txtPYCode, "AccountNameCode");
            this.txtPYCode.EmptyTextTip = null;
            this.txtPYCode.EmptyTextTipColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.dcm.SetEnterIsTab(this.txtPYCode, true);
            this.dcm.SetIsUse(this.txtPYCode, true);
            this.txtPYCode.Location = new System.Drawing.Point(105, 55);
            this.txtPYCode.MaxLength = 20;
            this.txtPYCode.Name = "txtPYCode";
            this.txtPYCode.RealValue = "";
            this.txtPYCode.Size = new System.Drawing.Size(248, 26);
            this.txtPYCode.TabIndex = 2;
            this.txtPYCode.ValueItem = null;
            this.dcm.SetValueMember(this.txtPYCode, "");
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(51, 94);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 20);
            this.label3.TabIndex = 3;
            this.label3.Text = "备注：";
            // 
            // editText3
            // 
            this.editText3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.editText3.CodePYTextBox = null;
            this.editText3.CodeWBTextBox = null;
            this.editText3.DisplayItem = "";
            this.dcm.SetDisplayMember(this.editText3, "Remark");
            this.editText3.EmptyTextTip = null;
            this.editText3.EmptyTextTipColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.dcm.SetEnterIsTab(this.editText3, true);
            this.dcm.SetIsUse(this.editText3, true);
            this.editText3.Location = new System.Drawing.Point(105, 90);
            this.editText3.MaxLength = 100;
            this.editText3.Multiline = true;
            this.editText3.Name = "editText3";
            this.editText3.RealValue = "";
            this.editText3.Size = new System.Drawing.Size(248, 66);
            this.editText3.TabIndex = 3;
            this.editText3.ValueItem = null;
            this.dcm.SetValueMember(this.editText3, "");
            // 
            // FrmAccountEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(400, 222);
            this.Controls.Add(this.editText3);
            this.Controls.Add(this.txtPYCode);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.editText1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.KeepFormSize = true;
            this.Name = "FrmAccountEdit";
            this.Text = "账户管理";
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.editText1, 0);
            this.Controls.SetChildIndex(this.label3, 0);
            this.Controls.SetChildIndex(this.txtPYCode, 0);
            this.Controls.SetChildIndex(this.editText3, 0);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private Win.Controls.EditText editText1;
        private System.Windows.Forms.Label label2;
        private Win.Controls.EditText txtPYCode;
        private System.Windows.Forms.Label label3;
        private Win.Controls.EditText editText3;
    }
}