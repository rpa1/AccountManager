﻿/**************************************************************************
创建时间:	2020/5/19 15:56:00    
作	  者:	张存
邮 	  箱:	zhangcunliang@126.com

Copyright (c) zhcun.cn

描	述：
记	录：
***************************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ZhCun.AccountManager.BLL;
using ZhCun.DbCore.Entitys;
using ZhCun.Utils;
using ZhCun.Win;

namespace ZhCun.AccountManager
{
    public partial class FrmBaseCrud<TSaveModel, TViewModel> : FrmCrud
         where TSaveModel : EntityBase, new()
         where TViewModel : EntityBase, new()
    {
        public FrmBaseCrud()
        {
            InitializeComponent();
        }

        protected virtual BLLBaseCrud<TSaveModel, TViewModel> BLLObj { get; }

        #region CRUD 重写

        /// <summary>
        /// 创建新增窗体的方法
        /// </summary>
        protected override FrmBaseEdit CreateAddForm()
        {
            throw new NotImplementedException("未实现 CreateAddForm");
        }
        /// <summary>
        /// 获取数据源的方法 （必须重写）
        /// </summary>
        protected override DataTable GetDataSource(int pageNo, int pageSize, string searchVal, out int rowCount)
        {
            return BLLObj.GetData(pageNo, pageSize, searchVal, out rowCount);
        }
        /// <summary>
        /// 新增处理
        /// </summary>
        protected override ApiResult<string> AddSaveHandle(object m)
        {
            return BLLObj.Add(m as TSaveModel);
        }
        /// <summary>
        /// 更新处理
        /// </summary>
        protected override ApiResult<string> EditSaveHandle(object m)
        {
            return BLLObj.Edit(m as TSaveModel);
        }

        public override object GetGridSelected()
        {
            return dgv.GetSelectedClassData<TViewModel>();
        }
        /// <summary>
        /// 删除处理
        /// </summary>
        protected override ApiResult DelHandle(object m)
        {
            return BLLObj.Del(m as TViewModel);
        }
        /// <summary>
        /// 创建保存实体对象
        /// </summary>
        public override object CreateSaveModel()
        {
            return new TSaveModel();
        }

        protected override void AdSearch_Click(object sender, EventArgs e)
        {
            ADSearch(BLLObj.ADJoinSql, BindDataSource);
        }


        #endregion
    }

}
