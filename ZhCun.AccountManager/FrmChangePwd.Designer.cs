﻿namespace ZhCun.AccountManager
{
    partial class FrmChangePwd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtOldPwd = new ZhCun.Win.Controls.EditText();
            this.txtNewPwd1 = new ZhCun.Win.Controls.EditText();
            this.txtNewPwd2 = new ZhCun.Win.Controls.EditText();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(52, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "原密码：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(52, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 20);
            this.label2.TabIndex = 0;
            this.label2.Text = "新密码：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(24, 101);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(93, 20);
            this.label3.TabIndex = 0;
            this.label3.Text = "重复新密码：";
            // 
            // txtOldPwd
            // 
            this.txtOldPwd.DisplayItem = "";
            this.txtOldPwd.EmptyTextTip = null;
            this.txtOldPwd.EmptyTextTipColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.txtOldPwd.Location = new System.Drawing.Point(123, 18);
            this.txtOldPwd.Name = "txtOldPwd";
            this.txtOldPwd.PasswordChar = '*';
            this.txtOldPwd.RealValue = "";
            this.txtOldPwd.Size = new System.Drawing.Size(165, 26);
            this.txtOldPwd.TabIndex = 1;
            this.txtOldPwd.ValueItem = null;
            this.txtOldPwd.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtOldPwd_KeyDown);
            // 
            // txtNewPwd1
            // 
            this.txtNewPwd1.DisplayItem = "";
            this.txtNewPwd1.EmptyTextTip = null;
            this.txtNewPwd1.EmptyTextTipColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.txtNewPwd1.Location = new System.Drawing.Point(123, 58);
            this.txtNewPwd1.Name = "txtNewPwd1";
            this.txtNewPwd1.PasswordChar = '*';
            this.txtNewPwd1.RealValue = "";
            this.txtNewPwd1.Size = new System.Drawing.Size(165, 26);
            this.txtNewPwd1.TabIndex = 2;
            this.txtNewPwd1.ValueItem = null;
            this.txtNewPwd1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNewPwd1_KeyDown);
            // 
            // txtNewPwd2
            // 
            this.txtNewPwd2.DisplayItem = "";
            this.txtNewPwd2.EmptyTextTip = null;
            this.txtNewPwd2.EmptyTextTipColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.txtNewPwd2.Location = new System.Drawing.Point(123, 98);
            this.txtNewPwd2.Name = "txtNewPwd2";
            this.txtNewPwd2.PasswordChar = '*';
            this.txtNewPwd2.RealValue = "";
            this.txtNewPwd2.Size = new System.Drawing.Size(165, 26);
            this.txtNewPwd2.TabIndex = 3;
            this.txtNewPwd2.ValueItem = null;
            this.txtNewPwd2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNewPwd2_KeyDown);
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(65, 142);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 32);
            this.btnOK.TabIndex = 4;
            this.btnOK.Text = "确定";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(230, 142);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 32);
            this.btnClose.TabIndex = 5;
            this.btnClose.Text = "取消";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // FrmChangePwd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(353, 192);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.txtNewPwd2);
            this.Controls.Add(this.txtNewPwd1);
            this.Controls.Add(this.txtOldPwd);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "FrmChangePwd";
            this.Text = "修改密码";
            this.Load += new System.EventHandler(this.FrmChangePwd_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private Win.Controls.EditText txtOldPwd;
        private Win.Controls.EditText txtNewPwd1;
        private Win.Controls.EditText txtNewPwd2;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnClose;
    }
}