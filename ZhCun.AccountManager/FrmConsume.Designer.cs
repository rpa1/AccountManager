﻿namespace ZhCun.AccountManager
{
    partial class FrmConsume
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            this.toolStripTop1 = new System.Windows.Forms.ToolStrip();
            this.tsBtnOut = new System.Windows.Forms.ToolStripButton();
            this.tsBtnIn = new System.Windows.Forms.ToolStripButton();
            this.tsBtnClose = new System.Windows.Forms.ToolStripButton();
            this.tsBtnCopy = new System.Windows.Forms.ToolStripButton();
            this.tsBtnUndo = new System.Windows.Forms.ToolStripButton();
            this.tsBtnCancel = new System.Windows.Forms.ToolStripButton();
            this.btnEdit = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsBtnCheckAccount = new System.Windows.Forms.ToolStripButton();
            this.tsBtnTransferAccount = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.tsAdSearch = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tsDateRange = new System.Windows.Forms.ToolStripComboBox();
            this.tsDateType = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.tsTxtSearch = new System.Windows.Forms.ToolStripTextBox();
            this.tsBtnSearch = new System.Windows.Forms.ToolStripButton();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.ucPage = new ZhCun.Win.Controls.UcPageNav();
            this.dgv = new ZhCun.Win.Controls.GridView();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tslbAmountOut = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel4 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tslbAmountIn = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel6 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tslbAmount = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripTop1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStripTop1
            // 
            this.toolStripTop1.AutoSize = false;
            this.toolStripTop1.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.toolStripTop1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsBtnOut,
            this.tsBtnIn,
            this.tsBtnClose,
            this.tsBtnCopy,
            this.tsBtnUndo,
            this.tsBtnCancel,
            this.btnEdit,
            this.toolStripSeparator1,
            this.tsBtnCheckAccount,
            this.tsBtnTransferAccount,
            this.toolStripSeparator4,
            this.tsAdSearch,
            this.toolStripSeparator2,
            this.tsDateRange,
            this.tsDateType,
            this.toolStripSeparator3,
            this.tsTxtSearch,
            this.tsBtnSearch});
            this.toolStripTop1.Location = new System.Drawing.Point(0, 0);
            this.toolStripTop1.Name = "toolStripTop1";
            this.toolStripTop1.Size = new System.Drawing.Size(1311, 38);
            this.toolStripTop1.TabIndex = 20;
            this.toolStripTop1.Text = "toolStrip2";
            // 
            // tsBtnOut
            // 
            this.tsBtnOut.BackColor = System.Drawing.Color.Transparent;
            this.tsBtnOut.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.tsBtnOut.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.tsBtnOut.Image = global::ZhCun.AccountManager.Properties.Resources.zhichu;
            this.tsBtnOut.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsBtnOut.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnOut.Name = "tsBtnOut";
            this.tsBtnOut.Size = new System.Drawing.Size(73, 35);
            this.tsBtnOut.Text = "支出";
            this.tsBtnOut.Click += new System.EventHandler(this.tsBtnOut_Click);
            // 
            // tsBtnIn
            // 
            this.tsBtnIn.BackColor = System.Drawing.Color.Transparent;
            this.tsBtnIn.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.tsBtnIn.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.tsBtnIn.Image = global::ZhCun.AccountManager.Properties.Resources.shouru;
            this.tsBtnIn.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsBtnIn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnIn.Name = "tsBtnIn";
            this.tsBtnIn.Size = new System.Drawing.Size(73, 35);
            this.tsBtnIn.Text = "收入";
            this.tsBtnIn.Click += new System.EventHandler(this.tsBtnIn_Click);
            // 
            // tsBtnClose
            // 
            this.tsBtnClose.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tsBtnClose.BackColor = System.Drawing.Color.Transparent;
            this.tsBtnClose.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.tsBtnClose.Image = global::ZhCun.AccountManager.Properties.Resources.exit;
            this.tsBtnClose.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsBtnClose.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnClose.Margin = new System.Windows.Forms.Padding(0, 1, 1, 2);
            this.tsBtnClose.Name = "tsBtnClose";
            this.tsBtnClose.Padding = new System.Windows.Forms.Padding(0, 0, 1, 0);
            this.tsBtnClose.Size = new System.Drawing.Size(74, 35);
            this.tsBtnClose.Text = "退出";
            this.tsBtnClose.Click += new System.EventHandler(this.tsBtnClose_Click);
            // 
            // tsBtnCopy
            // 
            this.tsBtnCopy.BackColor = System.Drawing.Color.Transparent;
            this.tsBtnCopy.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.tsBtnCopy.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.tsBtnCopy.Image = global::ZhCun.AccountManager.Properties.Resources.copy;
            this.tsBtnCopy.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsBtnCopy.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnCopy.Name = "tsBtnCopy";
            this.tsBtnCopy.Size = new System.Drawing.Size(73, 35);
            this.tsBtnCopy.Text = "复制";
            this.tsBtnCopy.Click += new System.EventHandler(this.tsBtnCopy_Click);
            // 
            // tsBtnUndo
            // 
            this.tsBtnUndo.BackColor = System.Drawing.Color.Transparent;
            this.tsBtnUndo.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.tsBtnUndo.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.tsBtnUndo.Image = global::ZhCun.AccountManager.Properties.Resources.Undo;
            this.tsBtnUndo.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsBtnUndo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnUndo.Name = "tsBtnUndo";
            this.tsBtnUndo.Size = new System.Drawing.Size(73, 35);
            this.tsBtnUndo.Text = "撤销";
            this.tsBtnUndo.ToolTipText = "撤销当天最后一笔记账记录";
            this.tsBtnUndo.Click += new System.EventHandler(this.tsBtnUndo_Click);
            // 
            // tsBtnCancel
            // 
            this.tsBtnCancel.BackColor = System.Drawing.Color.Transparent;
            this.tsBtnCancel.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.tsBtnCancel.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.tsBtnCancel.Image = global::ZhCun.AccountManager.Properties.Resources.fuzhijizhang;
            this.tsBtnCancel.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsBtnCancel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnCancel.Name = "tsBtnCancel";
            this.tsBtnCancel.Size = new System.Drawing.Size(73, 35);
            this.tsBtnCancel.Text = "冲账";
            this.tsBtnCancel.Visible = false;
            this.tsBtnCancel.Click += new System.EventHandler(this.tsBtnCancel_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.BackColor = System.Drawing.Color.Transparent;
            this.btnEdit.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnEdit.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btnEdit.Image = global::ZhCun.AccountManager.Properties.Resources.edit;
            this.btnEdit.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnEdit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(68, 35);
            this.btnEdit.Text = "编辑";
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 38);
            // 
            // tsBtnCheckAccount
            // 
            this.tsBtnCheckAccount.BackColor = System.Drawing.Color.Transparent;
            this.tsBtnCheckAccount.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tsBtnCheckAccount.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.tsBtnCheckAccount.Image = global::ZhCun.AccountManager.Properties.Resources.book;
            this.tsBtnCheckAccount.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsBtnCheckAccount.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnCheckAccount.Name = "tsBtnCheckAccount";
            this.tsBtnCheckAccount.Size = new System.Drawing.Size(73, 35);
            this.tsBtnCheckAccount.Text = "对账";
            this.tsBtnCheckAccount.Click += new System.EventHandler(this.tsBtnCheckAccount_Click);
            // 
            // tsBtnTransferAccount
            // 
            this.tsBtnTransferAccount.BackColor = System.Drawing.Color.Transparent;
            this.tsBtnTransferAccount.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tsBtnTransferAccount.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.tsBtnTransferAccount.Image = global::ZhCun.AccountManager.Properties.Resources.chongzhang;
            this.tsBtnTransferAccount.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsBtnTransferAccount.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnTransferAccount.Name = "tsBtnTransferAccount";
            this.tsBtnTransferAccount.Size = new System.Drawing.Size(73, 35);
            this.tsBtnTransferAccount.Text = "转账";
            this.tsBtnTransferAccount.Click += new System.EventHandler(this.tsBtnTransferAccount_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 38);
            // 
            // tsAdSearch
            // 
            this.tsAdSearch.BackColor = System.Drawing.Color.Transparent;
            this.tsAdSearch.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tsAdSearch.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.tsAdSearch.Image = global::ZhCun.AccountManager.Properties.Resources.search;
            this.tsAdSearch.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsAdSearch.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsAdSearch.Name = "tsAdSearch";
            this.tsAdSearch.Size = new System.Drawing.Size(101, 35);
            this.tsAdSearch.Text = "高级搜索";
            this.tsAdSearch.Click += new System.EventHandler(this.tsAdSearch_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 38);
            // 
            // tsDateRange
            // 
            this.tsDateRange.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.tsDateRange.Items.AddRange(new object[] {
            "一周内",
            "一月内"});
            this.tsDateRange.Name = "tsDateRange";
            this.tsDateRange.Size = new System.Drawing.Size(100, 38);
            this.tsDateRange.SelectedIndexChanged += new System.EventHandler(this.tsConsumeDate_SelectedIndexChanged);
            // 
            // tsDateType
            // 
            this.tsDateType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.tsDateType.Items.AddRange(new object[] {
            "记录时间",
            "发生时间"});
            this.tsDateType.Name = "tsDateType";
            this.tsDateType.Size = new System.Drawing.Size(100, 38);
            this.tsDateType.ToolTipText = "选择是按记录时间筛选记录,还是发生时间筛选";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 38);
            // 
            // tsTxtSearch
            // 
            this.tsTxtSearch.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tsTxtSearch.Margin = new System.Windows.Forms.Padding(5);
            this.tsTxtSearch.Name = "tsTxtSearch";
            this.tsTxtSearch.Size = new System.Drawing.Size(120, 28);
            this.tsTxtSearch.ToolTipText = "输入回车进行快速检索";
            this.tsTxtSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tsTxtSearch_KeyDown);
            // 
            // tsBtnSearch
            // 
            this.tsBtnSearch.Checked = true;
            this.tsBtnSearch.CheckState = System.Windows.Forms.CheckState.Checked;
            this.tsBtnSearch.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsBtnSearch.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tsBtnSearch.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.tsBtnSearch.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnSearch.Margin = new System.Windows.Forms.Padding(0, 1, 5, 2);
            this.tsBtnSearch.Name = "tsBtnSearch";
            this.tsBtnSearch.Padding = new System.Windows.Forms.Padding(0, 0, 20, 0);
            this.tsBtnSearch.Size = new System.Drawing.Size(79, 35);
            this.tsBtnSearch.Text = "检索(&S)";
            this.tsBtnSearch.Click += new System.EventHandler(this.tsBtnSearch_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel2,
            this.tslbAmountOut,
            this.toolStripStatusLabel4,
            this.tslbAmountIn,
            this.toolStripStatusLabel6,
            this.tslbAmount});
            this.statusStrip1.Location = new System.Drawing.Point(0, 636);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1311, 22);
            this.statusStrip1.TabIndex = 21;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // ucPage
            // 
            this.ucPage.AutoSize = true;
            this.ucPage.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ucPage.Font = new System.Drawing.Font("宋体", 10.5F);
            this.ucPage.Location = new System.Drawing.Point(0, 594);
            this.ucPage.Name = "ucPage";
            this.ucPage.Size = new System.Drawing.Size(1311, 42);
            this.ucPage.TabIndex = 23;
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle9;
            this.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgv.BackgroundColor = System.Drawing.Color.Snow;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.DisplayRowNo = false;
            this.dgv.DisplayRowNoStyle = ZhCun.Win.Controls.GridView.RowNoStyle.CustomColumn;
            this.dgv.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv.Location = new System.Drawing.Point(0, 38);
            this.dgv.MultiSelect = false;
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dgv.RowsDefaultCellStyle = dataGridViewCellStyle10;
            this.dgv.RowTemplate.Height = 23;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.Size = new System.Drawing.Size(1311, 556);
            this.dgv.TabIndex = 24;
            this.dgv.UseControlStyle = true;
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.BackColor = System.Drawing.Color.Transparent;
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(44, 17);
            this.toolStripStatusLabel2.Text = "支出：";
            // 
            // tslbAmountOut
            // 
            this.tslbAmountOut.BackColor = System.Drawing.Color.Transparent;
            this.tslbAmountOut.ForeColor = System.Drawing.Color.Red;
            this.tslbAmountOut.Name = "tslbAmountOut";
            this.tslbAmountOut.Size = new System.Drawing.Size(32, 17);
            this.tslbAmountOut.Text = "0.00";
            // 
            // toolStripStatusLabel4
            // 
            this.toolStripStatusLabel4.BackColor = System.Drawing.Color.Transparent;
            this.toolStripStatusLabel4.Name = "toolStripStatusLabel4";
            this.toolStripStatusLabel4.Size = new System.Drawing.Size(44, 17);
            this.toolStripStatusLabel4.Text = "收入：";
            // 
            // tslbAmountIn
            // 
            this.tslbAmountIn.BackColor = System.Drawing.Color.Transparent;
            this.tslbAmountIn.ForeColor = System.Drawing.Color.Red;
            this.tslbAmountIn.Name = "tslbAmountIn";
            this.tslbAmountIn.Size = new System.Drawing.Size(32, 17);
            this.tslbAmountIn.Text = "0.00";
            // 
            // toolStripStatusLabel6
            // 
            this.toolStripStatusLabel6.BackColor = System.Drawing.Color.Transparent;
            this.toolStripStatusLabel6.Name = "toolStripStatusLabel6";
            this.toolStripStatusLabel6.Size = new System.Drawing.Size(56, 17);
            this.toolStripStatusLabel6.Text = "收支差：";
            // 
            // tslbAmount
            // 
            this.tslbAmount.BackColor = System.Drawing.Color.Transparent;
            this.tslbAmount.ForeColor = System.Drawing.Color.Red;
            this.tslbAmount.Name = "tslbAmount";
            this.tslbAmount.Size = new System.Drawing.Size(32, 17);
            this.tslbAmount.Text = "0.00";
            // 
            // FrmConsume
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1311, 658);
            this.Controls.Add(this.dgv);
            this.Controls.Add(this.ucPage);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.toolStripTop1);
            this.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "FrmConsume";
            this.Text = "记账管理";
            this.Load += new System.EventHandler(this.FrmConsume_Load);
            this.toolStripTop1.ResumeLayout(false);
            this.toolStripTop1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.ToolStrip toolStripTop1;
        public System.Windows.Forms.ToolStripButton tsBtnOut;
        public System.Windows.Forms.ToolStripButton tsBtnIn;
        public System.Windows.Forms.ToolStripButton tsBtnClose;
        public System.Windows.Forms.ToolStripButton tsBtnCopy;
        public System.Windows.Forms.ToolStripButton tsBtnUndo;
        public System.Windows.Forms.ToolStripButton tsBtnCancel;
        public System.Windows.Forms.ToolStripButton btnEdit;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        public System.Windows.Forms.ToolStripButton tsAdSearch;
        public System.Windows.Forms.ToolStripTextBox tsTxtSearch;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private Win.Controls.UcPageNav ucPage;
        private Win.Controls.GridView dgv;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        public System.Windows.Forms.ToolStripButton tsBtnSearch;
        private System.Windows.Forms.ToolStripComboBox tsDateRange;
        public System.Windows.Forms.ToolStripButton tsBtnCheckAccount;
        public System.Windows.Forms.ToolStripButton tsBtnTransferAccount;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripComboBox tsDateType;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripStatusLabel tslbAmountOut;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel4;
        private System.Windows.Forms.ToolStripStatusLabel tslbAmountIn;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel6;
        private System.Windows.Forms.ToolStripStatusLabel tslbAmount;
    }
}