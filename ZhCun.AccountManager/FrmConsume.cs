﻿/**************************************************************************
创建时间:	2020/5/19 15:55:13    
作	  者:	张存
邮 	  箱:	zhangcunliang@126.com

Copyright (c) zhcun.cn

描	述：
记	录：
***************************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ZhCun.AccountManager.BLL;
using ZhCun.AccountManager.BLL.VM;
using ZhCun.AccountManager.DBModel;
using ZhCun.Utils;
using ZhCun.Win;

namespace ZhCun.AccountManager
{
    public partial class FrmConsume : FrmBase
    {
        public FrmConsume()
        {
            InitializeComponent();
        }

        readonly ConsumeBLL BLLObj = new ConsumeBLL();

        readonly ArgConsumeSearch _SearchArg = new ArgConsumeSearch();

        void RefreshData()
        {
            RefreshData(1, ucPage.OnePageCount);
        }

        int RefreshData(int pageNo, int pageSize)
        {
            _SearchArg.PageNo = pageNo;
            _SearchArg.PageSize = pageSize;
            _SearchArg.SearchVal = tsTxtSearch.Text;
            _SearchArg.DataRange = tsDateRange.SelectedItem as DateRange;
            _SearchArg.DateType = tsDateType.SelectedIndex;
            var rData = BLLObj.GetData(_SearchArg);
            dgv.DataSource = rData;
            ucPage.InitiControl(_SearchArg.OutRowCount);
            //tslbSumAmount.Text = _SearchArg.AmountSum.ToString();
            
            tslbAmount.Text = _SearchArg.AmountSum.ToString();
            tslbAmountIn.Text = _SearchArg.AmountSumIn.ToString();
            tslbAmountOut.Text = _SearchArg.AmountSumOut.ToString();
            return _SearchArg.OutRowCount;
        }

        private void FrmConsume_Load(object sender, EventArgs e)
        {
            tsDateRange.Items.Clear();
            foreach (var item in BLLObj.SearchDateList)
            {
                tsDateRange.Items.Add(item);
            }

            tsDateRange.SelectedIndex = 0;
            tsDateType.SelectedIndex = 1;

            RefreshData();
            ucPage.PageChangedEvent += UcPage_PageChangedEvent;
            dgv.SetDoubleBuffered(true);
        }

        private void UcPage_PageChangedEvent(int pageNo, int pageSize, out int recordCount)
        {
            recordCount = RefreshData(pageNo, pageSize);
        }

        private void tsBtnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        void ShowEditForm(BalanceType bType, TConsume setModel = null, bool isEdit = false)
        {
            FrmConsumeEdit frm = new FrmConsumeEdit();
            frm.GetAccountHandel = BLLObj.GetAccountData;
            frm.GetConsumeTypeHandel = (serVal) => BLLObj.GetConsumeTypeData(serVal, bType);

            TConsume saveModel = new TConsume();
            saveModel.BalanceTypeId = (int)bType;
            saveModel.BalanceTypeName = bType.ToString();

            var editArg = new EditFormArgs
            {
                ClearId = !isEdit,
                SaveHandle = Save,
                SaveModel = saveModel,
                SetModel = setModel
            };
            if (isEdit)
            {
                frm.IsEditConsume = true;
            }

            frm.ShowDialog(editArg);

            ApiResult<string> Save(object m)
            {
                ApiResult r;
                if (isEdit)
                {
                    r = BLLObj.EditConsume(m as TConsume);
                }
                else
                {
                    r = BLLObj.SaveConsume(m as TConsume);
                }
                if (r)
                {
                    ShowMessage(r.msg);
                    RefreshData();
                    return ApiResult.RetOK(string.Empty, r.msg);
                }
                else
                {
                    return ApiResult.RetErr(string.Empty, r.msg);
                }
            }
        }

        private void tsBtnOut_Click(object sender, EventArgs e)
        {
            ShowEditForm(BalanceType.支出);
        }

        private void tsBtnIn_Click(object sender, EventArgs e)
        {
            ShowEditForm(BalanceType.收入);
        }

        private void tsBtnCopy_Click(object sender, EventArgs e)
        {
            var selModel = dgv.GetSelectedClassData<TConsume>();
            if (selModel == null)
            {
                ShowMessage("没有选中任何记录");
                return;
            }
            BalanceType bType = (BalanceType)selModel.BalanceTypeId;
            if (bType != BalanceType.支出 && bType != BalanceType.收入)
            {
                ShowMessage("只能对支出或收入进行复制");
                return;
            }
            ShowEditForm(bType, selModel);
        }

        private void tsBtnUndo_Click(object sender, EventArgs e)
        {
            if (!ShowQuestion("确实要移除24小时内最后一条记录吗?", "移除确认"))
            {
                return;
            }

            var r = BLLObj.DelLastConsume();
            if (r)
            {
                RefreshData();
            }
            ShowMessage(r.msg);
        }

        private void tsBtnCancel_Click(object sender, EventArgs e)
        {
            var selModel = dgv.GetSelectedClassData<TConsume>();
            if (selModel == null)
            {
                ShowMessage("没有选中任何记录");
                return;
            }
            if (selModel.IsCancel)
            {
                ShowMessage("当前记录已冲账");
                return;
            }
            BalanceType bType = (BalanceType)selModel.BalanceTypeId;
            if (bType != BalanceType.支出 && bType != BalanceType.收入)
            {
                ShowMessage("只能对支出或收入进行编辑");
                return;
            }
            //if (!ShowQuestion("确实要对指定记录进行冲账吗?", "冲账确认"))
            //{
            //    return;
            //}
            if (ShowInput(SaveCancel, "请输入冲账原因"))
            {
                RefreshData();
            }

            ApiResult SaveCancel(string input)
            {
                selModel.CancelRemark = input;
                var ret = BLLObj.CancelConsume(selModel);
                if (ret)
                {
                    RefreshData();
                    ShowMessage(ret.msg);
                }
                return ret;
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            var selModel = dgv.GetSelectedClassData<TConsume>();
            if (selModel == null)
            {
                ShowMessage("没有选中任何记录");
                return;
            }
            if (selModel.IsCancel)
            {
                ShowMessage("冲账记录不能编辑");
                return;
            }
            BalanceType bType = (BalanceType)selModel.BalanceTypeId;
            //if (bType != BalanceType.支出 && bType != BalanceType.收入)
            //{
            //    ShowMessage("只能对支出或收入进行编辑");
            //    return;
            //}
            ShowEditForm(bType, selModel, true);
        }

        private void tsBtnSearch_Click(object sender, EventArgs e)
        {
            BLLObj.ClearADSql();
            RefreshData();
        }

        private void tsTxtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                tsBtnSearch_Click(sender, e);
            }
        }

        private void tsConsumeDate_SelectedIndexChanged(object sender, EventArgs e)
        {
            tsBtnSearch_Click(sender, e);
        }

        private void tsAdSearch_Click(object sender, EventArgs e)
        {
            //tsConsumeDate.SelectedIndex = -1;
            ADSearch(BLLObj.ADJoinSql, RefreshData);
        }

        private void tsBtnCheckAccount_Click(object sender, EventArgs e)
        {
            var frm = new FrmConsumeCheckAccount();
            frm.CheckSave = BLLObj.SaveCheckAccount;
            frm.GetAccountHandel = BLLObj.GetAccountData;
            if (frm.ShowDialog() == DialogResult.OK)
            {
                ShowMessage("对账完成");
                RefreshData();
            }
        }

        private void tsBtnTransferAccount_Click(object sender, EventArgs e)
        {
            var frm = new FrmConsumeTransferAccount();
            frm.TransferSave = BLLObj.SaveTransferAccount;
            frm.GetAccountHandel = BLLObj.GetAccountData;
            if (frm.ShowDialog() == DialogResult.OK)
            {
                ShowMessage("转账完成");
                RefreshData();
            }
        }
    }
}