﻿namespace ZhCun.AccountManager
{
    partial class FrmConsumeCheckAccount
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtAccount = new ZhCun.Win.Controls.EditText();
            this.label2 = new System.Windows.Forms.Label();
            this.txtRealAmount = new ZhCun.Win.Controls.EditNumber();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtBalance = new ZhCun.Win.Controls.EditNumber();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtRemark = new ZhCun.Win.Controls.EditText();
            this.SuspendLayout();
            // 
            // txtAccount
            // 
            this.txtAccount.DisplayItem = "";
            this.txtAccount.EmptyTextTip = "输入助记码检索";
            this.txtAccount.EmptyTextTipColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.txtAccount.Location = new System.Drawing.Point(153, 34);
            this.txtAccount.Margin = new System.Windows.Forms.Padding(4);
            this.txtAccount.Name = "txtAccount";
            this.txtAccount.RealValue = "";
            this.txtAccount.Size = new System.Drawing.Size(263, 34);
            this.txtAccount.TabIndex = 1;
            this.txtAccount.ValueItem = null;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Blue;
            this.label2.Location = new System.Drawing.Point(78, 37);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 27);
            this.label2.TabIndex = 5;
            this.label2.Text = "账户：";
            // 
            // txtRealAmount
            // 
            this.txtRealAmount.DisplayItem = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.txtRealAmount.EmptyTextTip = null;
            this.txtRealAmount.EmptyTextTipColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.txtRealAmount.FormatString = "#,##0.00";
            this.txtRealAmount.Location = new System.Drawing.Point(153, 123);
            this.txtRealAmount.Margin = new System.Windows.Forms.Padding(4);
            this.txtRealAmount.Name = "txtRealAmount";
            this.txtRealAmount.PrefixSign = "￥";
            this.txtRealAmount.RealValue = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.txtRealAmount.Size = new System.Drawing.Size(263, 34);
            this.txtRealAmount.TabIndex = 2;
            this.txtRealAmount.Text = "￥0.00";
            this.txtRealAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtRealAmount.Value = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.txtRealAmount.ValueItem = null;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.Blue;
            this.label5.Location = new System.Drawing.Point(38, 126);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(112, 27);
            this.label5.TabIndex = 6;
            this.label5.Text = "实际余额：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Blue;
            this.label3.Location = new System.Drawing.Point(38, 80);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(112, 27);
            this.label3.TabIndex = 6;
            this.label3.Text = "当前余额：";
            // 
            // txtBalance
            // 
            this.txtBalance.DisplayItem = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.txtBalance.EmptyTextTip = null;
            this.txtBalance.EmptyTextTipColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.txtBalance.Enabled = false;
            this.txtBalance.FormatString = "#,##0.00";
            this.txtBalance.Location = new System.Drawing.Point(153, 78);
            this.txtBalance.Margin = new System.Windows.Forms.Padding(4);
            this.txtBalance.Name = "txtBalance";
            this.txtBalance.PrefixSign = "￥";
            this.txtBalance.ReadOnly = true;
            this.txtBalance.RealValue = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.txtBalance.Size = new System.Drawing.Size(263, 34);
            this.txtBalance.TabIndex = 7;
            this.txtBalance.Text = "￥0.00";
            this.txtBalance.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBalance.Value = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.txtBalance.ValueItem = null;
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(109, 254);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(109, 39);
            this.btnOK.TabIndex = 3;
            this.btnOK.Text = "确定";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(335, 254);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(109, 39);
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "取消";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(78, 171);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 27);
            this.label1.TabIndex = 8;
            this.label1.Text = "备注：";
            // 
            // txtRemark
            // 
            this.txtRemark.DisplayItem = "";
            this.txtRemark.EmptyTextTip = null;
            this.txtRemark.EmptyTextTipColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.txtRemark.Location = new System.Drawing.Point(153, 168);
            this.txtRemark.Multiline = true;
            this.txtRemark.Name = "txtRemark";
            this.txtRemark.RealValue = "";
            this.txtRemark.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtRemark.Size = new System.Drawing.Size(263, 63);
            this.txtRemark.TabIndex = 9;
            this.txtRemark.ValueItem = null;
            // 
            // FrmConsumeCheckAccount
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(482, 316);
            this.Controls.Add(this.txtRemark);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.txtBalance);
            this.Controls.Add(this.txtRealAmount);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtAccount);
            this.Controls.Add(this.label2);
            this.Font = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Margin = new System.Windows.Forms.Padding(6, 7, 6, 7);
            this.Name = "FrmConsumeCheckAccount";
            this.Text = "账户对账";
            this.Load += new System.EventHandler(this.FrmConsumeCheckAccount_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Win.Controls.EditText txtAccount;
        private System.Windows.Forms.Label label2;
        private Win.Controls.EditNumber txtRealAmount;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private Win.Controls.EditNumber txtBalance;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label label1;
        private Win.Controls.EditText txtRemark;
    }
}