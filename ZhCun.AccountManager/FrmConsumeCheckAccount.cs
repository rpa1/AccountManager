﻿/**************************************************************************
创建时间:	2020/5/21 12:01:34    
作	  者:	张存
邮 	  箱:	zhangcunliang@126.com

Copyright (c) zhcun.cn

描	述：
记	录：
***************************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ZhCun.AccountManager.DBModel;
using ZhCun.Utils;
using ZhCun.Utils.Helpers;
using ZhCun.Win;
using ZhCun.Win.ExtendGridCombox;

namespace ZhCun.AccountManager
{
    public partial class FrmConsumeCheckAccount : FrmBaseModal
    {
        public FrmConsumeCheckAccount()
        {
            InitializeComponent();
        }

        public Func<string, decimal, string, ApiResult> CheckSave;

        public Func<string, object> GetAccountHandel;

        private void FrmConsumeCheckAccount_Load(object sender, EventArgs e)
        {
            this.InitLargeFont();
            InitCoBoxGrid(new ArgInitCoBoxGrid
            {
                Txt = txtAccount,
                GetDataSource = GetAccountHandel
            });

            //CoBoxGrid.OnBeforeShowGridView += CoBoxGrid_OnBeforeShowGridView;
            CoBoxGrid.OnSelectedRowItem += CoBoxGrid_OnSelectedRowItem;
        }

        private void CoBoxGrid_OnSelectedRowItem(TextBox txt, object data)
        {
            TAccount account;
            if (data is DataRow dr)
            {
                account = ReflectionHelper.SetPropertyValue<TAccount>(dr);
            }
            else
            {
                account = data as TAccount;
            }
            txtBalance.Value = account.Balance;
            //txtAccount.Focus();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            var ret = CheckSave(
                txtAccount.ValueItem?.ToString(),
                txtRealAmount.Value,
                ""
                );

            if (!ret)
            {
                ShowMessage(ret.msg);
                return;
            }
            this.DialogResult = DialogResult.OK;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
