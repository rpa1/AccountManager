﻿namespace ZhCun.AccountManager
{
    partial class FrmConsumeEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.editText1 = new ZhCun.Win.Controls.EditText();
            this.label2 = new System.Windows.Forms.Label();
            this.txtAccount = new ZhCun.Win.Controls.EditText();
            this.dtpConsumeDate = new ZhCun.Win.Controls.EditDateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtConsumeType = new ZhCun.Win.Controls.EditText();
            this.label5 = new System.Windows.Forms.Label();
            this.txtAmount = new ZhCun.Win.Controls.EditNumber();
            this.label6 = new System.Windows.Forms.Label();
            this.editText4 = new ZhCun.Win.Controls.EditText();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 404);
            this.panel1.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.panel1.Size = new System.Drawing.Size(604, 43);
            // 
            // btnSave
            // 
            this.btnSave.Font = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnSave.Location = new System.Drawing.Point(345, 0);
            this.btnSave.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.btnSave.Size = new System.Drawing.Size(115, 39);
            // 
            // btnCancel
            // 
            this.btnCancel.Font = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnCancel.Location = new System.Drawing.Point(460, 0);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.btnCancel.Size = new System.Drawing.Size(140, 39);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Blue;
            this.label1.Location = new System.Drawing.Point(48, 88);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 25);
            this.label1.TabIndex = 3;
            this.label1.Text = "账目说明：";
            // 
            // editText1
            // 
            this.editText1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.editText1.DisplayItem = "";
            this.dcm.SetDisplayMember(this.editText1, "ConsumeName");
            this.editText1.EmptyTextTip = null;
            this.editText1.EmptyTextTipColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.dcm.SetEnterIsTab(this.editText1, true);
            this.dcm.SetIsUse(this.editText1, true);
            this.editText1.Location = new System.Drawing.Point(167, 85);
            this.editText1.Name = "editText1";
            this.editText1.RealValue = "";
            this.editText1.Size = new System.Drawing.Size(353, 33);
            this.editText1.TabIndex = 2;
            this.editText1.ValueItem = null;
            this.dcm.SetValueMember(this.editText1, "Id");
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Blue;
            this.label2.Location = new System.Drawing.Point(48, 135);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 25);
            this.label2.TabIndex = 3;
            this.label2.Text = "发生账户：";
            // 
            // txtAccount
            // 
            this.txtAccount.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccount.DisplayItem = "";
            this.dcm.SetDisplayMember(this.txtAccount, "AccountName");
            this.txtAccount.EmptyTextTip = "输入助记码后回车检索";
            this.txtAccount.EmptyTextTipColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.dcm.SetEnterIsTab(this.txtAccount, false);
            this.dcm.SetIsUse(this.txtAccount, true);
            this.txtAccount.Location = new System.Drawing.Point(167, 131);
            this.txtAccount.Name = "txtAccount";
            this.txtAccount.RealValue = "";
            this.txtAccount.Size = new System.Drawing.Size(353, 33);
            this.txtAccount.TabIndex = 3;
            this.txtAccount.ValueItem = null;
            this.dcm.SetValueMember(this.txtAccount, "AccountId");
            // 
            // dtpConsumeDate
            // 
            this.dtpConsumeDate.DisplayItem = new System.DateTime(2020, 5, 20, 15, 50, 57, 994);
            this.dcm.SetDisplayMember(this.dtpConsumeDate, "ConsumeDate");
            this.dcm.SetEnterIsTab(this.dtpConsumeDate, true);
            this.dcm.SetIsUse(this.dtpConsumeDate, true);
            this.dtpConsumeDate.Location = new System.Drawing.Point(167, 39);
            this.dtpConsumeDate.Name = "dtpConsumeDate";
            this.dtpConsumeDate.Size = new System.Drawing.Size(352, 33);
            this.dtpConsumeDate.TabIndex = 1;
            this.dtpConsumeDate.TabStop = false;
            this.dtpConsumeDate.Value = new System.DateTime(2020, 5, 20, 15, 50, 57, 994);
            this.dtpConsumeDate.ValueItem = true;
            this.dcm.SetValueMember(this.dtpConsumeDate, "");
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Blue;
            this.label3.Location = new System.Drawing.Point(48, 42);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(107, 25);
            this.label3.TabIndex = 3;
            this.label3.Text = "发生日期：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Blue;
            this.label4.Location = new System.Drawing.Point(48, 180);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(107, 25);
            this.label4.TabIndex = 3;
            this.label4.Text = "账目类型：";
            // 
            // txtConsumeType
            // 
            this.txtConsumeType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtConsumeType.DisplayItem = "";
            this.dcm.SetDisplayMember(this.txtConsumeType, "ConsumeTypeName");
            this.txtConsumeType.EmptyTextTip = "输入助记码后回车检索";
            this.txtConsumeType.EmptyTextTipColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.dcm.SetEnterIsTab(this.txtConsumeType, false);
            this.dcm.SetIsUse(this.txtConsumeType, true);
            this.txtConsumeType.Location = new System.Drawing.Point(167, 177);
            this.txtConsumeType.Name = "txtConsumeType";
            this.txtConsumeType.RealValue = "";
            this.txtConsumeType.Size = new System.Drawing.Size(353, 33);
            this.txtConsumeType.TabIndex = 4;
            this.txtConsumeType.ValueItem = null;
            this.dcm.SetValueMember(this.txtConsumeType, "ConsumeTypeId");
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.Blue;
            this.label5.Location = new System.Drawing.Point(69, 226);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(88, 25);
            this.label5.TabIndex = 3;
            this.label5.Text = "发生额：";
            // 
            // txtAmount
            // 
            this.txtAmount.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAmount.DisplayItem = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.dcm.SetDisplayMember(this.txtAmount, "Amount");
            this.txtAmount.EmptyTextTip = null;
            this.txtAmount.EmptyTextTipColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.dcm.SetEnterIsTab(this.txtAmount, true);
            this.txtAmount.FormatString = "#,##0.00";
            this.dcm.SetIsUse(this.txtAmount, true);
            this.txtAmount.Location = new System.Drawing.Point(167, 223);
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.PrefixSign = "￥";
            this.txtAmount.RealValue = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.txtAmount.Size = new System.Drawing.Size(353, 33);
            this.txtAmount.TabIndex = 5;
            this.txtAmount.Text = "￥0.00";
            this.txtAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtAmount.Value = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.txtAmount.ValueItem = null;
            this.dcm.SetValueMember(this.txtAmount, "");
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.Blue;
            this.label6.Location = new System.Drawing.Point(90, 272);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(69, 25);
            this.label6.TabIndex = 3;
            this.label6.Text = "备注：";
            // 
            // editText4
            // 
            this.editText4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.editText4.DisplayItem = "";
            this.dcm.SetDisplayMember(this.editText4, "Remark");
            this.editText4.EmptyTextTip = null;
            this.editText4.EmptyTextTipColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.dcm.SetEnterIsTab(this.editText4, true);
            this.dcm.SetIsUse(this.editText4, true);
            this.editText4.Location = new System.Drawing.Point(167, 269);
            this.editText4.Multiline = true;
            this.editText4.Name = "editText4";
            this.editText4.RealValue = "";
            this.editText4.Size = new System.Drawing.Size(353, 96);
            this.editText4.TabIndex = 6;
            this.editText4.ValueItem = null;
            this.dcm.SetValueMember(this.editText4, "");
            // 
            // FrmConsumeEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(604, 447);
            this.Controls.Add(this.editText4);
            this.Controls.Add(this.txtAmount);
            this.Controls.Add(this.dtpConsumeDate);
            this.Controls.Add(this.txtConsumeType);
            this.Controls.Add(this.txtAccount);
            this.Controls.Add(this.editText1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("微软雅黑", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.KeepFormSize = true;
            this.Name = "FrmConsumeEdit";
            this.Text = "记账";
            this.Load += new System.EventHandler(this.FrmConsumeEdit_Load);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.label3, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.label4, 0);
            this.Controls.SetChildIndex(this.label5, 0);
            this.Controls.SetChildIndex(this.label6, 0);
            this.Controls.SetChildIndex(this.editText1, 0);
            this.Controls.SetChildIndex(this.txtAccount, 0);
            this.Controls.SetChildIndex(this.txtConsumeType, 0);
            this.Controls.SetChildIndex(this.dtpConsumeDate, 0);
            this.Controls.SetChildIndex(this.txtAmount, 0);
            this.Controls.SetChildIndex(this.editText4, 0);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private Win.Controls.EditText editText1;
        private System.Windows.Forms.Label label2;
        private Win.Controls.EditText txtAccount;
        private Win.Controls.EditDateTimePicker dtpConsumeDate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private Win.Controls.EditText txtConsumeType;
        private System.Windows.Forms.Label label5;
        private Win.Controls.EditNumber txtAmount;
        private System.Windows.Forms.Label label6;
        private Win.Controls.EditText editText4;
    }
}