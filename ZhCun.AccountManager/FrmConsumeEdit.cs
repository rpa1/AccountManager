﻿/**************************************************************************
创建时间:	2020/5/20 15:46:02    
作	  者:	张存
邮 	  箱:	zhangcunliang@126.com

Copyright (c) zhcun.cn

描	述：
记	录：
***************************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ZhCun.Win;
using ZhCun.Win.ExtendGridCombox;

namespace ZhCun.AccountManager
{
    public partial class FrmConsumeEdit : FrmBaseEdit
    {
        public FrmConsumeEdit()
        {
            InitializeComponent();            
        }

        public Func<string, object> GetAccountHandel;

        public Func<string, object> GetConsumeTypeHandel;

        /// <summary>
        /// 是否编辑消费,不可以修改账户及金额
        /// </summary>
        public bool IsEditConsume { set; get; }

        private void FrmConsumeEdit_Load(object sender, EventArgs e)
        {
            var args = new ArgInitCoBoxGrid[]
            {
                new ArgInitCoBoxGrid
                {
                    Txt = txtAccount,
                    GetDataSource = GetAccountHandel
                },
                new ArgInitCoBoxGrid{
                    Txt = txtConsumeType,
                    GetDataSource = GetConsumeTypeHandel
                },
            };
            InitCoBoxGrid(args);
            
            CoBoxGrid.OnBeforeShowGridView += CoBoxGrid_OnBeforeShowGridView;
            CoBoxGrid.OnSelectedRowItem += CoBoxGrid_OnSelectedRowItem;
            if (IsEditConsume)
            {
                txtAccount.Enabled = false;
                txtAmount.Enabled = false;
            }
            if (!IsEditConsume)
            {
                dtpConsumeDate.Value = DateTime.Now;
            }
            this.InitLargeFont();
        }

        private void CoBoxGrid_OnBeforeShowGridView(Win.Extend.ComboxGridArgs arg)
        {

        }

        private void CoBoxGrid_OnSelectedRowItem(TextBox arg1, object data)
        {

        }
    }
}