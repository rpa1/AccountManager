﻿/**************************************************************************
创建时间:	2020/5/21 12:02:35    
作	  者:	张存
邮 	  箱:	zhangcunliang@126.com

Copyright (c) zhcun.cn

描	述：
记	录：
***************************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ZhCun.AccountManager.DBModel;
using ZhCun.Utils;
using ZhCun.Utils.Helpers;
using ZhCun.Win;
using ZhCun.Win.ExtendGridCombox;

namespace ZhCun.AccountManager
{
    public partial class FrmConsumeTransferAccount : FrmBaseModal
    {
        public FrmConsumeTransferAccount()
        {
            InitializeComponent();
        }

        public Func<string, string, decimal, ApiResult> TransferSave;

        public Func<string, object> GetAccountHandel;

        private void btnOK_Click(object sender, EventArgs e)
        {
            var ret =
            TransferSave(
                txtAccountOut.ValueItem?.ToString(),
                txtAccountIn.ValueItem?.ToString(),
                txtAmount.Value
                );

            if (!ret)
            {
                ShowMessage(ret.msg);
            }
            this.DialogResult = DialogResult.OK;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FrmConsumeTransferAccount_Load(object sender, EventArgs e)
        {
            this.InitLargeFont();

            InitCoBoxGrid(
                new ArgInitCoBoxGrid
                {
                    Txt = txtAccountOut,
                    GetDataSource = GetAccountHandel
                },
                new ArgInitCoBoxGrid
                {
                    Txt = txtAccountIn,
                    GetDataSource = GetAccountHandel
                }
            );
            CoBoxGrid.OnSelectedRowItem += CoBoxGrid_OnSelectedRowItem;
        }

        private void CoBoxGrid_OnSelectedRowItem(TextBox txt, object data)
        {
            TAccount account;
            if (data is DataRow dr)
            {
                account = ReflectionHelper.SetPropertyValue<TAccount>(dr);
            }
            else
            {
                account = data as TAccount;
            }

            if (txt == txtAccountOut)
            {
                txtBalanceOut.Value = account.Balance;
                //txtAccountIn.Focus();
            }

            if (txt == txtAccountIn)
            {
                txtBalanceIn.Value = account.Balance;
                //txtAmount.Focus();
            }
        }
    }
}