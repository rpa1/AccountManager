﻿/**************************************************************************
创建时间:	2020/5/19 15:53:34    
作	  者:	张存
邮 	  箱:	zhangcunliang@126.com

Copyright (c) zhcun.cn

描	述：
记	录：
***************************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ZhCun.AccountManager.BLL;
using ZhCun.AccountManager.DBModel;
using ZhCun.Win;

namespace ZhCun.AccountManager
{
    public partial class FrmConsumeType : FrmBaseCrud<TConsumeType, TConsumeType>
    {
        public FrmConsumeType()
        {
            InitializeComponent();
        }

        readonly ConsumeTypeBLL _BLLObj = new ConsumeTypeBLL();

        protected override BLLBaseCrud<TConsumeType, TConsumeType> BLLObj => _BLLObj;

        /// <summary>
        /// 创建新增窗体的方法
        /// </summary>
        protected override FrmBaseEdit CreateAddForm()
        {
            var frm = new FrmConsumeTypeEdit();
            return frm;
        }
    }
}
