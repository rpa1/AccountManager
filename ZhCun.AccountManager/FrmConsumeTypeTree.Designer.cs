﻿namespace ZhCun.AccountManager
{
    partial class FrmConsumeTypeTree
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmConsumeTypeTree));
            this.toolStripTop = new System.Windows.Forms.ToolStrip();
            this.tsBtnAddTop = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbtnExit = new System.Windows.Forms.ToolStripButton();
            this.tsBtnRefresh = new System.Windows.Forms.ToolStripButton();
            this.tv = new ZhCun.Win.Controls.TreeViewEx();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.cMenuNode = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmiAddNode = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiAddItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiDel = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiSetTop = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiMoveUp = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiMoveDown = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripTop.SuspendLayout();
            this.cMenuNode.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripTop
            // 
            this.toolStripTop.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.toolStripTop.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsBtnAddTop,
            this.toolStripSeparator1,
            this.tsbtnExit,
            this.tsBtnRefresh});
            this.toolStripTop.Location = new System.Drawing.Point(0, 0);
            this.toolStripTop.Name = "toolStripTop";
            this.toolStripTop.Size = new System.Drawing.Size(430, 39);
            this.toolStripTop.TabIndex = 34;
            this.toolStripTop.Text = "工具栏";
            // 
            // tsBtnAddTop
            // 
            this.tsBtnAddTop.BackColor = System.Drawing.Color.Transparent;
            this.tsBtnAddTop.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tsBtnAddTop.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.tsBtnAddTop.Image = global::ZhCun.AccountManager.Properties.Resources.add;
            this.tsBtnAddTop.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsBtnAddTop.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnAddTop.Name = "tsBtnAddTop";
            this.tsBtnAddTop.Size = new System.Drawing.Size(121, 36);
            this.tsBtnAddTop.Text = "增加顶级(&A)";
            this.tsBtnAddTop.Click += new System.EventHandler(this.tsBtnAddTop_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 39);
            // 
            // tsbtnExit
            // 
            this.tsbtnExit.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tsbtnExit.BackColor = System.Drawing.Color.Transparent;
            this.tsbtnExit.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tsbtnExit.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.tsbtnExit.Image = global::ZhCun.AccountManager.Properties.Resources.exit;
            this.tsbtnExit.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbtnExit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnExit.Name = "tsbtnExit";
            this.tsbtnExit.Size = new System.Drawing.Size(73, 36);
            this.tsbtnExit.Text = "退出";
            this.tsbtnExit.Click += new System.EventHandler(this.tsbtnExit_Click);
            // 
            // tsBtnRefresh
            // 
            this.tsBtnRefresh.BackColor = System.Drawing.Color.Transparent;
            this.tsBtnRefresh.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tsBtnRefresh.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.tsBtnRefresh.Image = global::ZhCun.AccountManager.Properties.Resources.refresh;
            this.tsBtnRefresh.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsBtnRefresh.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnRefresh.Name = "tsBtnRefresh";
            this.tsBtnRefresh.Size = new System.Drawing.Size(73, 36);
            this.tsBtnRefresh.Text = "刷新";
            this.tsBtnRefresh.Click += new System.EventHandler(this.tsBtnRefresh_Click);
            // 
            // tv
            // 
            this.tv.AllowDropNode = false;
            this.tv.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tv.DrawMode = System.Windows.Forms.TreeViewDrawMode.OwnerDrawText;
            this.tv.HideSelection = false;
            this.tv.ImageIndex = 0;
            this.tv.ImageList = this.imageList1;
            this.tv.IsSingleChecked = false;
            this.tv.Location = new System.Drawing.Point(0, 39);
            this.tv.Name = "tv";
            this.tv.SelectedImageIndex = 0;
            this.tv.Size = new System.Drawing.Size(430, 556);
            this.tv.TabIndex = 36;
            this.tv.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.Tv_AfterSelect);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "org.png");
            this.imageList1.Images.SetKeyName(1, "tree-node0.png");
            // 
            // cMenuNode
            // 
            this.cMenuNode.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiAddNode,
            this.tsmiAddItem,
            this.tsmiEdit,
            this.tsmiDel,
            this.tsmiSetTop,
            this.tsmiMoveUp,
            this.tsmiMoveDown});
            this.cMenuNode.Name = "cMenuTree";
            this.cMenuNode.Size = new System.Drawing.Size(137, 158);
            this.cMenuNode.Opening += new System.ComponentModel.CancelEventHandler(this.cMenuNode_Opening);
            // 
            // tsmiAddNode
            // 
            this.tsmiAddNode.Name = "tsmiAddNode";
            this.tsmiAddNode.Size = new System.Drawing.Size(180, 22);
            this.tsmiAddNode.Text = "新增节点";
            this.tsmiAddNode.Click += new System.EventHandler(this.tsmiAddNode_Click);
            // 
            // tsmiAddItem
            // 
            this.tsmiAddItem.Name = "tsmiAddItem";
            this.tsmiAddItem.Size = new System.Drawing.Size(180, 22);
            this.tsmiAddItem.Text = "增加分类";
            this.tsmiAddItem.Click += new System.EventHandler(this.tsmiAddItem_Click);
            // 
            // tsmiEdit
            // 
            this.tsmiEdit.Name = "tsmiEdit";
            this.tsmiEdit.Size = new System.Drawing.Size(180, 22);
            this.tsmiEdit.Text = "编辑";
            this.tsmiEdit.Click += new System.EventHandler(this.tsmiEdit_Click);
            // 
            // tsmiDel
            // 
            this.tsmiDel.Name = "tsmiDel";
            this.tsmiDel.Size = new System.Drawing.Size(180, 22);
            this.tsmiDel.Text = "删除";
            this.tsmiDel.Click += new System.EventHandler(this.tsmiDel_Click);
            // 
            // tsmiSetTop
            // 
            this.tsmiSetTop.Name = "tsmiSetTop";
            this.tsmiSetTop.Size = new System.Drawing.Size(180, 22);
            this.tsmiSetTop.Text = "设置为顶级";
            this.tsmiSetTop.Click += new System.EventHandler(this.tsmiSetTop_Click);
            // 
            // tsmiMoveUp
            // 
            this.tsmiMoveUp.Name = "tsmiMoveUp";
            this.tsmiMoveUp.Size = new System.Drawing.Size(180, 22);
            this.tsmiMoveUp.Text = "向上移动";
            this.tsmiMoveUp.Click += new System.EventHandler(this.tsmiMoveUp_Click);
            // 
            // tsmiMoveDown
            // 
            this.tsmiMoveDown.Name = "tsmiMoveDown";
            this.tsmiMoveDown.Size = new System.Drawing.Size(180, 22);
            this.tsmiMoveDown.Text = "向下移动";
            this.tsmiMoveDown.Click += new System.EventHandler(this.tsmiMoveDown_Click);
            // 
            // FrmConsumeTypeTree
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(430, 595);
            this.Controls.Add(this.tv);
            this.Controls.Add(this.toolStripTop);
            this.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "FrmConsumeTypeTree";
            this.Text = "账目分类";
            this.Load += new System.EventHandler(this.FrmConsumeTypeTree_Load);
            this.toolStripTop.ResumeLayout(false);
            this.toolStripTop.PerformLayout();
            this.cMenuNode.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.ToolStrip toolStripTop;
        public System.Windows.Forms.ToolStripButton tsBtnAddTop;
        protected System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        public System.Windows.Forms.ToolStripButton tsbtnExit;
        private Win.Controls.TreeViewEx tv;
        private System.Windows.Forms.ContextMenuStrip cMenuNode;
        private System.Windows.Forms.ToolStripMenuItem tsmiAddNode;
        private System.Windows.Forms.ToolStripMenuItem tsmiEdit;
        private System.Windows.Forms.ToolStripMenuItem tsmiDel;
        public System.Windows.Forms.ToolStripMenuItem tsmiSetTop;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.ToolStripMenuItem tsmiAddItem;
        public System.Windows.Forms.ToolStripButton tsBtnRefresh;
        private System.Windows.Forms.ToolStripMenuItem tsmiMoveUp;
        private System.Windows.Forms.ToolStripMenuItem tsmiMoveDown;
    }
}