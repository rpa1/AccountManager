﻿/**************************************************************************
创建时间:	2020/5/19 15:54:27    
作	  者:	张存
邮 	  箱:	zhangcunliang@126.com

Copyright (c) zhcun.cn

描	述：
记	录：
***************************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ZhCun.AccountManager.BLL;
using ZhCun.AccountManager.DBModel;
using ZhCun.Utils;
using ZhCun.Utils.Helpers;
using ZhCun.Win;
using ZhCun.Win.Extend;

namespace ZhCun.AccountManager
{
    public partial class FrmConsumeTypeTree : FrmBase
    {
        public FrmConsumeTypeTree()
        {
            InitializeComponent();
        }

        readonly ConsumeTypeTreeBLL BLLObj = new ConsumeTypeTreeBLL();

        void NodeAddHandle(TreeNode node, TConsumeType m)
        {
            if (m.IsType)
            {
                node.ImageIndex = 1;
                node.SelectedImageIndex = 1;
            }
            else
            {
                node.ImageIndex = 0;
            }
            node.ContextMenuStrip = cMenuNode;
        }

        void InitTree(string selNodeName = null)
        {
            var treeData = BLLObj.GetTreeData();

            tv.InitTreeViewData(treeData, NodeAddHandle);

            tv.ExpandAll();

            if (!selNodeName.IsEmpty())
            {
                tv.SetSelectedNode(selNodeName);
            }
        }

        private void FrmConsumeTypeTree_Load(object sender, EventArgs e)
        {
            InitTree();
            tv.AllowDropNode = true;
            tv.DropNodeBefore += Tv_DropNodeBefore;
        }

        private bool Tv_DropNodeBefore(TreeNode moveNode, TreeNode targetNode)
        {
            var selMenu = targetNode.Tag as TConsumeType;
            if (selMenu.IsType)
            {
                ShowMessage("不能拖动到该项");
                return false;
            }

            var m = moveNode.Tag as TConsumeType;
            m.OrderNo = targetNode.Nodes.Count;
            m.PId = targetNode.Name;
            var r = BLLObj.EditNode(m);
            ShowMessage(r.msg);
            return !!r;
        }

        private void Tv_AfterSelect(object sender, TreeViewEventArgs e)
        {

        }

        private void tsBtnAddTop_Click(object sender, EventArgs e)
        {
            ShowInput(Save, "增加顶级");
            ApiResult Save(string input)
            {
                var saveModel = new TConsumeType
                {
                    TypeName = input,
                    Id = BLLObj.NewId(),
                    OrderNo = tv.Nodes.Count
                };
                var r = BLLObj.AddNode(saveModel);
                if (r)
                {
                    ShowMessage(r.msg);
                    InitTree(saveModel.Id);
                }
                return r;
            }
        }

        private void tsmiAddNode_Click(object sender, EventArgs e)
        {
            var selNode = tv.SelectedNode;

            ShowInput(Save, "增加节点");
            ApiResult Save(string input)
            {
                var saveModel = new TConsumeType
                {
                    PId = selNode.Name,
                    TypeName = input,
                    Id = BLLObj.NewId(),
                    OrderNo = selNode.Nodes.Count
                };
                var r = BLLObj.AddNode(saveModel);
                if (r)
                {
                    ShowMessage(r.msg);
                    InitTree(saveModel.Id);
                }
                return r;
            }
        }

        private void tsmiAddItem_Click(object sender, EventArgs e)
        {
            var selNode = tv.SelectedNode;
            FrmConsumeTypeEdit frm = new FrmConsumeTypeEdit();

            var saveModel = new TConsumeType
            {
                Id = BLLObj.NewId(),
                PId = selNode.Name,
                OrderNo = selNode.Nodes.Count
            };
            var r =
            frm.ShowDialog(new EditFormArgs
            {
                SaveHandle = (m) => BLLObj.Add(m as TConsumeType),
                SaveModel = saveModel,
                SetModel = saveModel
            });
            if (r)
            {
                ShowMessage(r.msg);
                InitTree(saveModel.Id);
            }
        }

        private void tsmiEdit_Click(object sender, EventArgs e)
        {
            var selModel = tv.SelectedNode.Tag as TConsumeType;
            ApiResult r = null;

            if (selModel.IsType)
            {
                FrmConsumeTypeEdit frm = new FrmConsumeTypeEdit();
                var arg = new EditFormArgs
                {
                    SaveHandle = (m) => BLLObj.Edit(m as TConsumeType),
                    SaveModel = selModel,
                    SetModel = selModel
                };
                r =
                frm.ShowDialog(new EditFormArgs
                {
                    SaveHandle = (m) => BLLObj.Edit(m as TConsumeType),
                    SaveModel = selModel,
                    SetModel = selModel
                });
            }
            else
            {
                ShowInput(Save, "编辑节点", selModel.TypeName);

                ApiResult Save(string input)
                {
                    selModel.TypeName = input;
                    r = BLLObj.EditNode(selModel);
                    return r;
                }
            }
            if (r)
            {
                ShowMessage(r.msg);
                InitTree(selModel.Id);
            }
        }

        private void tsmiDel_Click(object sender, EventArgs e)
        {
            var selModel = tv.SelectedNode.Tag as TConsumeType;
            var r = BLLObj.DelNode(selModel);
            if (r)
            {
                InitTree(selModel.Id);
            }
            ShowMessage(r.msg);
        }

        private void tsmiSetTop_Click(object sender, EventArgs e)
        {
            var selModel = tv.SelectedNode.Tag as TConsumeType;
            selModel.PId = string.Empty;
            selModel.OrderNo = tv.Nodes.Count;
            var r = BLLObj.EditNode(selModel);
            if (r)
            {
                InitTree();
            }
            ShowMessage(r.msg);
        }

        private void cMenuNode_Opening(object sender, CancelEventArgs e)
        {
            var selModel = tv.SelectedNode.Tag as TConsumeType;
            tsmiAddNode.Visible = !selModel.IsType;
            tsmiAddItem.Visible = !selModel.IsType;
            //tsmiAddItem.Visible = selModel.IsType;
        }

        private void tsbtnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        void MoveNode(bool isUp)
        {
            var srcNode = tv.SelectedNode;
            var tarNode = isUp ? srcNode.PrevNode : srcNode.NextNode;
            if (tarNode != null)
            {
                BLLObj.ChangeOrder(srcNode.Name, srcNode.Index, tarNode.Name, tarNode.Index);
                InitTree(srcNode.Name);
            }
        }

        private void tsmiMoveUp_Click(object sender, EventArgs e)
        {
            MoveNode(true);
        }

        private void tsmiMoveDown_Click(object sender, EventArgs e)
        {
            MoveNode(false);
        }

        private void tsBtnRefresh_Click(object sender, EventArgs e)
        {
            InitTree();
        }
    }
}