﻿/**************************************************************************
创建时间:	2020/5/15
作	  者:	张存
邮 	  箱:	zhangcunliang@126.com

Copyright (c) zhcun.cn

描	述：
记	录：
***************************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ZhCun.AccountManager.BLL;
using ZhCun.Utils;
using ZhCun.Win;
using ZhCun.Win.Extend;

namespace ZhCun.AccountManager
{
    public partial class FrmLogin : FrmBaseModal
    {
        public FrmLogin()
        {
            InitializeComponent();
        }
    
        LoginBLL BLLObj { get; } = new LoginBLL();

        ApiResult LoginHandle(string loginName,string loginPwd)
        {
            var ret = BLLObj.LoginVerify(loginName, loginPwd);
            if (ret)
            {
                CurrentUserId = BLLObj.CurrUser.Id;
                IsManager = BLLObj.CurrUser.IsManager;
            }
            return ret;
        }

        public DialogResult ShowDialog(string loginName, string loginPwd)
        {
            var ret = LoginHandle(loginName, loginPwd);
            if (!ret)
            {
                ShowMessageBox(ret.msg);
                return DialogResult.No;
            }
            else
            {
                return DialogResult.OK;
            }
        }

        private void FrmLogin_Load(object sender, EventArgs e)
        {
            
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            var ret = LoginHandle(txtLoginName.Text, txtLoginPwd.Text);
            if (!ret)
            {
                ShowMessage(ret.msg);
                return;
            }
            this.DialogResult = DialogResult.OK;            
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtLoginName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtLoginPwd.Focus();
                txtLoginPwd.SelectAll();
            }
        }

        private void txtLoginPwd_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnOK.Focus();
            }
        }
    }
}