﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using ZhCun.AccountManager.BLL;
using ZhCun.Utils;
using ZhCun.Win;

namespace ZhCun.AccountManager
{
    public partial class FrmMain : FrmMDIMain
    {
        public FrmMain()
        {
            InitializeComponent();
        }

        readonly MainBLL BLLObj = new MainBLL();

        protected override DockPanel DockPanelControl { get { return dockPanel1; } }

        protected override ToolStrip ToolStripControl { get { return toolStrip1; } }

        protected override MenuStrip MenuStripControl { get { return menuStrip1; } }

        protected override string RoleId => BLLObj.CurrUser.RoleId;

        protected override FrmBase CreateChangeUserForm()
        {
            return new FrmLogin();
        }

        protected override FrmBase CreateChangePwdForm()
        {
            return new FrmChangePwd(BLLObj.ChangePwd);
        }

        protected override FrmBase CreateVerifyPwdForm()
        {
            return new FrmVerifyPwd(BLLObj.VerifyPwd);
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
           
        }

        protected override void Init()
        {
            base.Init();
            tslbUserName.Text = BLLObj.CurrUser.UserName;
            tslbLoginTime.Text = DateTime.Now.ToString();
        }
    }
}