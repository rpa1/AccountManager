﻿namespace ZhCun.AccountManager
{
    partial class FrmUserEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.editText1 = new ZhCun.Win.Controls.EditText();
            this.editText2 = new ZhCun.Win.Controls.EditText();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 145);
            this.panel1.Size = new System.Drawing.Size(358, 42);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(182, 0);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(268, 0);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(45, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 20);
            this.label1.TabIndex = 3;
            this.label1.Text = "用户名：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(45, 82);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 20);
            this.label2.TabIndex = 3;
            this.label2.Text = "登陆名：";
            // 
            // editText1
            // 
            this.editText1.DisplayItem = "";
            this.dcm.SetDisplayMember(this.editText1, "UserName");
            this.editText1.EmptyTextTip = null;
            this.editText1.EmptyTextTipColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.dcm.SetEnterIsTab(this.editText1, true);
            this.dcm.SetIsUse(this.editText1, true);
            this.editText1.Location = new System.Drawing.Point(117, 35);
            this.editText1.MaxLength = 10;
            this.editText1.Name = "editText1";
            this.editText1.RealValue = "";
            this.editText1.Size = new System.Drawing.Size(177, 26);
            this.editText1.TabIndex = 1;
            this.editText1.ValueItem = null;
            this.dcm.SetValueMember(this.editText1, "Id");
            // 
            // editText2
            // 
            this.editText2.DisplayItem = "";
            this.dcm.SetDisplayMember(this.editText2, "LoginName");
            this.editText2.EmptyTextTip = null;
            this.editText2.EmptyTextTipColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.dcm.SetEnterIsTab(this.editText2, true);
            this.dcm.SetIsUse(this.editText2, true);
            this.editText2.Location = new System.Drawing.Point(116, 79);
            this.editText2.MaxLength = 10;
            this.editText2.Name = "editText2";
            this.editText2.RealValue = "";
            this.editText2.Size = new System.Drawing.Size(177, 26);
            this.editText2.TabIndex = 2;
            this.editText2.ValueItem = null;
            this.dcm.SetValueMember(this.editText2, "");
            // 
            // FrmUserEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(358, 187);
            this.Controls.Add(this.editText2);
            this.Controls.Add(this.editText1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "FrmUserEdit";
            this.Text = "用户";
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.editText1, 0);
            this.Controls.SetChildIndex(this.editText2, 0);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private Win.Controls.EditText editText1;
        private Win.Controls.EditText editText2;
    }
}