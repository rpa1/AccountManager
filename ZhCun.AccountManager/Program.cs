﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using ZhCun.AccountManager.BLL;
using ZhCun.Utils;

namespace ZhCun.AccountManager
{
    static class Program
    {
        static readonly string MutexName = "{CCE9C164-50F8-4D0B-A59D-FA921292834E}";

        static Mutex mutex;

        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main(params string[] arg)
        {
            LogHelper.LogObj.Info("程序启动");
            bool onlyOneRun = Config.Settings.RunOnleOne;
            if (onlyOneRun)
            {
                mutex = new Mutex(true, MutexName);
                if (!mutex.WaitOne(TimeSpan.Zero, true))
                {
                    MessageBox.Show("只能运行一个软件,请检查是否在任务栏隐藏!");
                    return;
                }
            }
            try
            {
                //处理未捕获的异常
                Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);
                //处理UI线程异常  
                Application.ThreadException += new System.Threading.ThreadExceptionEventHandler(Application_ThreadException);
                //处理非UI线程异常  
                AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);

                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);

                FrmLogin loginForm = new FrmLogin();
                DialogResult loginResult;
                if (arg != null && arg.Length > 1)
                {
                    loginResult = loginForm.ShowDialog(arg[0], arg[1]);
                }
                else
                {
                    loginResult = loginForm.ShowDialog();
                }

                if (loginResult == DialogResult.OK)
                {
                    Application.Run(new FrmMain());
                }
                else
                {
                    Application.Exit();
                }
            }
            catch (Exception ex)
            {
                LogHelper.LogObj.Fatal("① Main函数 出现应用程序未处理的异常", ex);
                MessageBox.Show("出现应用程序未处理的异常，详情查看日志！\r\n" + ex.Message, "系统错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// 做法很多，可以是把出错详细信息记录到文本、数据库，发送出错邮件到作者信箱或出错后重新初始化等等         
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        static void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {
            string msg = "② UI线程发生未处理异常!";
            string msg2 = string.Empty;
            if (e.Exception != null)
            {
                msg2 = "\r\n" + e.Exception.Message;
            }
            LogHelper.LogObj.Fatal(msg, e.Exception);
            MessageBox.Show(msg + msg2, "系统错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            string msg = "③ 非UI线程发生未处理异常!";
            string msg2 = string.Empty;
            Exception error = e.ExceptionObject as Exception;
            if (error != null)
            {
                msg2 = "\r\n" + error.Message;
            }
            LogHelper.LogObj.Fatal(msg, error);
            MessageBox.Show(msg + msg2, "系统错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }
}