﻿namespace ZhCun.AccountManager.ViewReport
{
    partial class FrmAccountSum
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.tsStartTime = new ZhCun.Win.Controls.ToolStripDateTimePicker();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.tsEndTime = new ZhCun.Win.Controls.ToolStripDateTimePicker();
            this.tsDateType = new System.Windows.Forms.ToolStripComboBox();
            this.tsBtnRefresh = new System.Windows.Forms.ToolStripButton();
            this.tsbtnExit = new System.Windows.Forms.ToolStripButton();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tslbAmountOut = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel4 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tslbAmountIn = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel5 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel6 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tslbAmount = new System.Windows.Forms.ToolStripStatusLabel();
            this.dgv = new ZhCun.Win.Controls.GridView();
            this.toolStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel2,
            this.tsStartTime,
            this.toolStripLabel1,
            this.tsEndTime,
            this.tsDateType,
            this.tsBtnRefresh,
            this.tsbtnExit});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(800, 39);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(51, 36);
            this.toolStripLabel2.Text = "日期：";
            // 
            // tsStartTime
            // 
            this.tsStartTime.AutoSize = false;
            this.tsStartTime.BackColor = System.Drawing.Color.Transparent;
            this.tsStartTime.Checked = true;
            this.tsStartTime.CustomFormat = null;
            this.tsStartTime.Format = System.Windows.Forms.DateTimePickerFormat.Long;
            this.tsStartTime.MaxDate = new System.DateTime(9998, 12, 31, 0, 0, 0, 0);
            this.tsStartTime.Name = "tsStartTime";
            this.tsStartTime.ShowCheckBox = false;
            this.tsStartTime.Size = new System.Drawing.Size(150, 32);
            this.tsStartTime.Text = "toolStripDateTimePicker1";
            this.tsStartTime.Value = new System.DateTime(2020, 5, 23, 22, 11, 25, 523);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(15, 36);
            this.toolStripLabel1.Text = "-";
            // 
            // tsEndTime
            // 
            this.tsEndTime.AutoSize = false;
            this.tsEndTime.BackColor = System.Drawing.Color.Transparent;
            this.tsEndTime.Checked = true;
            this.tsEndTime.CustomFormat = null;
            this.tsEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Long;
            this.tsEndTime.MaxDate = new System.DateTime(9998, 12, 31, 0, 0, 0, 0);
            this.tsEndTime.Name = "tsEndTime";
            this.tsEndTime.ShowCheckBox = false;
            this.tsEndTime.Size = new System.Drawing.Size(150, 32);
            this.tsEndTime.Text = "toolStripDateTimePicker1";
            this.tsEndTime.Value = new System.DateTime(2020, 5, 23, 22, 11, 25, 523);
            // 
            // tsDateType
            // 
            this.tsDateType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.tsDateType.Items.AddRange(new object[] {
            "记录时间",
            "发生时间"});
            this.tsDateType.Name = "tsDateType";
            this.tsDateType.Size = new System.Drawing.Size(100, 39);
            this.tsDateType.ToolTipText = "选择是按记录时间筛选记录,还是发生时间筛选";
            // 
            // tsBtnRefresh
            // 
            this.tsBtnRefresh.BackColor = System.Drawing.Color.Transparent;
            this.tsBtnRefresh.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tsBtnRefresh.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.tsBtnRefresh.Image = global::ZhCun.AccountManager.Properties.Resources.refresh;
            this.tsBtnRefresh.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsBtnRefresh.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnRefresh.Name = "tsBtnRefresh";
            this.tsBtnRefresh.Size = new System.Drawing.Size(73, 36);
            this.tsBtnRefresh.Text = "刷新";
            this.tsBtnRefresh.Click += new System.EventHandler(this.tsBtnRefresh_Click);
            // 
            // tsbtnExit
            // 
            this.tsbtnExit.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tsbtnExit.BackColor = System.Drawing.Color.Transparent;
            this.tsbtnExit.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tsbtnExit.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.tsbtnExit.Image = global::ZhCun.AccountManager.Properties.Resources.exit;
            this.tsbtnExit.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbtnExit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnExit.Name = "tsbtnExit";
            this.tsbtnExit.Size = new System.Drawing.Size(73, 36);
            this.tsbtnExit.Text = "退出";
            this.tsbtnExit.Click += new System.EventHandler(this.tsbtnExit_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.tslbAmountOut,
            this.toolStripStatusLabel2,
            this.toolStripStatusLabel4,
            this.tslbAmountIn,
            this.toolStripStatusLabel5,
            this.toolStripStatusLabel6,
            this.tslbAmount});
            this.statusStrip1.Location = new System.Drawing.Point(0, 425);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(800, 25);
            this.statusStrip1.TabIndex = 5;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.BackColor = System.Drawing.Color.Transparent;
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(65, 20);
            this.toolStripStatusLabel1.Text = "总支出：";
            // 
            // tslbAmountOut
            // 
            this.tslbAmountOut.BackColor = System.Drawing.Color.Transparent;
            this.tslbAmountOut.ForeColor = System.Drawing.Color.Red;
            this.tslbAmountOut.Name = "tslbAmountOut";
            this.tslbAmountOut.Size = new System.Drawing.Size(36, 20);
            this.tslbAmountOut.Text = "0.00";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.BackColor = System.Drawing.Color.Transparent;
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(13, 20);
            this.toolStripStatusLabel2.Text = "|";
            // 
            // toolStripStatusLabel4
            // 
            this.toolStripStatusLabel4.BackColor = System.Drawing.Color.Transparent;
            this.toolStripStatusLabel4.Name = "toolStripStatusLabel4";
            this.toolStripStatusLabel4.Size = new System.Drawing.Size(65, 20);
            this.toolStripStatusLabel4.Text = "总收入：";
            // 
            // tslbAmountIn
            // 
            this.tslbAmountIn.BackColor = System.Drawing.Color.Transparent;
            this.tslbAmountIn.ForeColor = System.Drawing.Color.Red;
            this.tslbAmountIn.Name = "tslbAmountIn";
            this.tslbAmountIn.Size = new System.Drawing.Size(36, 20);
            this.tslbAmountIn.Text = "0.00";
            // 
            // toolStripStatusLabel5
            // 
            this.toolStripStatusLabel5.BackColor = System.Drawing.Color.Transparent;
            this.toolStripStatusLabel5.Name = "toolStripStatusLabel5";
            this.toolStripStatusLabel5.Size = new System.Drawing.Size(13, 20);
            this.toolStripStatusLabel5.Text = "|";
            // 
            // toolStripStatusLabel6
            // 
            this.toolStripStatusLabel6.BackColor = System.Drawing.Color.Transparent;
            this.toolStripStatusLabel6.Name = "toolStripStatusLabel6";
            this.toolStripStatusLabel6.Size = new System.Drawing.Size(65, 20);
            this.toolStripStatusLabel6.Text = "净收入：";
            // 
            // tslbAmount
            // 
            this.tslbAmount.BackColor = System.Drawing.Color.Transparent;
            this.tslbAmount.ForeColor = System.Drawing.Color.Red;
            this.tslbAmount.Name = "tslbAmount";
            this.tslbAmount.Size = new System.Drawing.Size(36, 20);
            this.tslbAmount.Text = "0.00";
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgv.BackgroundColor = System.Drawing.Color.Snow;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.DisplayRowNo = false;
            this.dgv.DisplayRowNoStyle = ZhCun.Win.Controls.GridView.RowNoStyle.CustomColumn;
            this.dgv.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv.Location = new System.Drawing.Point(0, 39);
            this.dgv.MultiSelect = false;
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dgv.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dgv.RowTemplate.Height = 23;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.Size = new System.Drawing.Size(800, 386);
            this.dgv.TabIndex = 7;
            this.dgv.UseControlStyle = true;
            // 
            // FrmAccountSum
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.dgv);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.toolStrip1);
            this.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "FrmAccountSum";
            this.Text = "账户统计查询";
            this.Load += new System.EventHandler(this.FrmAccountView_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripComboBox tsDateType;
        public System.Windows.Forms.ToolStripButton tsBtnRefresh;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel tslbAmountOut;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel4;
        private System.Windows.Forms.ToolStripStatusLabel tslbAmountIn;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel5;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel6;
        private System.Windows.Forms.ToolStripStatusLabel tslbAmount;
        public System.Windows.Forms.ToolStripButton tsbtnExit;
        private Win.Controls.ToolStripDateTimePicker tsStartTime;
        private Win.Controls.ToolStripDateTimePicker tsEndTime;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private Win.Controls.GridView dgv;
    }
}