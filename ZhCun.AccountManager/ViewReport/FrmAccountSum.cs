﻿/**************************************************************************
创建时间:	2020/5/19 17:04:00    
作	  者:	张存
邮 	  箱:	zhangcunliang@126.com

Copyright (c) zhcun.cn

描	述： 查找指定年月的账户信息，统计出每天的金额
记	录：
***************************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ZhCun.AccountManager.BLL;
using ZhCun.AccountManager.BLL.VM;
using ZhCun.Win;

namespace ZhCun.AccountManager.ViewReport
{
    public partial class FrmAccountSum : FrmBase
    {
        public FrmAccountSum()
        {
            InitializeComponent();
        }

        readonly ViewAccountSumBLL BLLObj = new ViewAccountSumBLL();

        readonly ArgSumSearch _Arg = new ArgSumSearch();

        private void FrmAccountView_Load(object sender, EventArgs e)
        {
            tsDateType.SelectedIndex = 0;
            tsStartTime.Value = DateTime.Now.AddMonths(-1);
            tsEndTime.Value = DateTime.Now;
        }
        
        private void tsbtnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void tsBtnRefresh_Click(object sender, EventArgs e)
        {
            _Arg.StartTime = tsStartTime.Value;
            _Arg.EndTime = tsEndTime.Value;
            _Arg.DateType = tsDateType.SelectedIndex;
            var data = BLLObj.GetViewData(_Arg);
            dgv.DataSource = data;
            tslbAmountOut.Text = Math.Abs(_Arg.SumAmountOut).ToString();
            tslbAmountIn.Text = _Arg.SumAmountIn.ToString();
            tslbAmount.Text = (_Arg.SumAmountIn + _Arg.SumAmountOut).ToString();
        }
    }
}
