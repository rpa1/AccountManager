﻿/**************************************************************************
创建时间:	2020/5/19 16:57:36    
作	  者:	张存
邮 	  箱:	zhangcunliang@126.com

Copyright (c) zhcun.cn

描	述：
记	录：
***************************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ZhCun.AccountManager.BLL;
using ZhCun.AccountManager.BLL.VM;
using ZhCun.Win;

namespace ZhCun.AccountManager.ViewReport
{
    public partial class FrmDetailView : FrmBase
    {
        public FrmDetailView()
        {
            InitializeComponent();
        }

        private readonly ViewDetailBLL BLLObj = new ViewDetailBLL();

        private readonly ArgDetailViewSearch _SearchArg = new ArgDetailViewSearch();

        void RefreshData()
        {
            RefreshData(1, ucPage.OnePageCount);

            tslbAmount.Text = _SearchArg.AmountSum.ToString();
            tslbAmountIn.Text = _SearchArg.AmountSumIn.ToString();
            tslbAmountOut.Text = _SearchArg.AmountSumOut.ToString();
        }

        int RefreshData(int pageNo, int pageSize)
        {
            _SearchArg.PageNo = pageNo;
            _SearchArg.PageSize = pageSize;
            _SearchArg.SearchVal = tsTxtSearch.Text;
            _SearchArg.StartTime = tsiStartTime.Value;
            _SearchArg.EndTime = tsiEndTime.Value;
            _SearchArg.DateType = tsDateType.SelectedIndex;
            var rData = BLLObj.GetData(_SearchArg);
            dgv.DataSource = rData;
            ucPage.InitiControl(_SearchArg.OutRowCount);
            
            return _SearchArg.OutRowCount;
        }

        private void FrmDetailView_Load(object sender, EventArgs e)
        {
            ucPage.InitiOnePageList(new int[] { 50, 100, 200 });
            tsiEndTime.Value = tsiEndTime.MaxDate = DateTime.Now;
            tsiStartTime.Value = DateTime.Now.AddDays(-7);
            tsDateType.SelectedIndex = 0;
            ucPage.PageChangedEvent += UcPage_PageChangedEvent;
            dgv.SetDoubleBuffered(true);
        }

        private void UcPage_PageChangedEvent(int pageNo, int pageSize, out int recordCount)
        {
            recordCount = RefreshData(pageNo, pageSize);
        }

        private void tsConsumeDate_SelectedIndexChanged(object sender, EventArgs e)
        {
            RefreshData();
        }

        private void tsAdSearch_Click(object sender, EventArgs e)
        {
            ADSearch(BLLObj.ADJoinSql, RefreshData);
        }

        private void tsBtnSearch_Click(object sender, EventArgs e)
        {
            BLLObj.ClearADSql();
            RefreshData();
        }

        private void tsTxtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                RefreshData();
            }
        }

        private void tsbtnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
