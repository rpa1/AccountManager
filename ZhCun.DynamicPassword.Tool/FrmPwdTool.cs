﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ZhCun.DynamicPassword.Tool
{
    public partial class FrmPwdTool : Form
    {
        public FrmPwdTool()
        {
            InitializeComponent();
        }

        StringEncrypty PwdHelper;

        private void Form1_Load(object sender, EventArgs e)
        {
            PwdHelper = new StringEncrypty();
        }

        private void btnToCiphertext_Click(object sender, EventArgs e)
        {
            txtCiphertext.Text = PwdHelper.GetCiphertext(txtKey.Text, txtPlaintext.Text);
        }

        private void btnToPlaintext_Click(object sender, EventArgs e)
        {
            txtPlaintext.Text = PwdHelper.GetPlaintext(txtKey.Text, txtCiphertext.Text);
        }
    }
}